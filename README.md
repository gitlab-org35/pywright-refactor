# PyWright
Python game engine for running visual novel games similar to the Phoenix Wright series

This program, a 'case maker', was created to enable fans to produce their own fan cases based on the Phoenix Wright series of games. It consists of an engine to run the games. Games themselves are scripted in "wrightscript", and several (old versions) of them can be downloaded from within the program itself.

# PyWright Refactor
A project built upon CRxTRDude's maintenance update in 2021-2022 by ceets. 
Aims to clean up the codebase and remove unused and broken features.

# Features
* Fully functional scripting language, "WrightScript", based on a mishmash of BASIC and Python
* Animated sprites and text with sophisticated dialog engine
* Music, sound effects, other special effects
* Customizable
* Multiple saved games
* Works identically on Windows, Mac and Linux
* Multiple display options

# History
This project was started in 2007 as a personal project by Saluk. After showing it to other Phoenix Wright fans on the court-records forums, it has grown and evolved through their feedback. It is the dedicated members there who have actually made the games that are available.

# Minimum requirements for building/running from source
You will need the following:
- Python 3.10
- Pygame 2.1.2
- NumPy 1.23.1
- simplejson 3.17.6

(Also check the requirements.txt file)

# New build system

The program is now built from source using PyInstaller:

```
pyinstaller PyWright.spec -y
```

To learn more about using pyinstaller to use this build, read PyWright.spec and the
[spec files documentation]([https://pyinstaller.org/en/stable/spec-files.html#using-spec-files).

# Discontinued features
- Android support has been removed due to lack of maintenance and releases.
- Mac builds are broken.
- No longer supports automatic updates.
- The engine is now more prone to error, as a way to prevent silent failures and bugs. 
Avoid lines like 'sfx none' or 'sfx 0%' which relied on failed file searches.

# License
Under the New BSD License. 

# Assets
This engine uses some assets by Capcom. 
I might remove/change them from this repo to avoid some problems.

# Credits
- saluk - original author
- CRxTRDude - maintainance
- Scoopa - helped find many bugs

# New Library (PyWright-gpu)
This version has added gpu acceleration with pygame 2.

# Known bugs
- HTML color text is broken.
- Text in evidence menus and nametags is slightly misaligned.
- Failure to read a sound effect for typing text results in an error raised for every character.

# To-do list
- Changed cross exam left and right to wait for text - but no way to skip through cross exam text. It also shows misleading arrows.
- Court record over menu = control menu not court record
- psyche lock macro covers ugly arrow
- characters show up in menu
- custom back button w/sound

