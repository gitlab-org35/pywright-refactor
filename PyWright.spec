# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
include_artfiles = False
datas = [
   ('docs','docs'),
   ('examples', 'examples'),
   ('README.md', '.'),
   ('LICENSE.md', '.'),
   ('changelog.txt', '.'),
   ('program_settings/*.txt','program_settings'),
   ('program_settings/*.ini', 'program_settings'), 
   ('fonts', 'fonts'),
   ('core/macros','core/macros')]

a = Analysis(['PyWright.py'],
             pathex=[],
             binaries=[],
             datas=datas,
             hiddenimports=['pygame', 'simplejson', 'numpy', '[numpy.core]', 'requests', 'certifi'],
             hookspath=[],
             runtime_hooks=[],
             excludes=['loghistory.txt', 'lastlog.txt', 'lastgame'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='PyWright',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False , icon='data/art/general/bb.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='PyWright')
