extension_map = {"image":["png","jpg"],
"script":["txt"],
"music":["wav","mid","mod","ogg","s3m","it","xm"],
"sound":["wav","ogg"],
"movie":["mpeg","mpg"]}
def extensions_for_types(types=["image","script","music","sound"]):
    for et in types:
        for e in extension_map[et]:
            yield "."+e
def noext(p,types=["image","script","music","sound"]):
    """Path without the extension, if extension is in known types"""
    for ext in extensions_for_types(types):
        if p.endswith(ext):
            return p.rsplit(".",1)[0]
    return p
def onlyext(p,types=["image","script","music","sound"]):
    """Returns the extension of the path"""
    for ext in extensions_for_types(types):
        if p.endswith(ext):
            return ext
    return ""
assert noext("something.png",["image"])=="something"
assert noext("something.png",["music"])=="something.png"
assert onlyext("something.png")==".png"