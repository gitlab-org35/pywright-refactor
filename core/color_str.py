def color_str(variables, rgbstring):
    cv = variables.get("color_"+rgbstring.strip(), None)
    if cv is not None:
        rgbstring = cv
    # 6-digit rgb (green is #00ff00)
    if len(rgbstring) == 3:
        import warnings
        warnings.warn("The 3 digit color format is deprecated: " + rgbstring)
        return [int((int(colchar)/9.0)*255) for colchar in rgbstring]
    elif len(rgbstring) != 6:
        raise ValueError("Bad RGB format: " + rgbstring)
    # int(xy, 16) interprets xy as hex
    try:
        v = [
            int(rgbstring[:2], 16),
            int(rgbstring[2:4], 16),
            int(rgbstring[4:6], 16)
        ]
        return v
    except:
        raise ValueError("Bad RGB format: " + rgbstring)
