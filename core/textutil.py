import re
from core.errors import *
import pygame
WHITE = [255,255,255]
class markup:
    def addcharsto(self,list):
        list.append(self)
class markup_color(markup):
    def __init__(self,color=None,revert=False):
        self.color = "999"
        if color:
            self.color = color.strip()
        self.revert = revert
        self._color = None
    def __repr__(self):
        return "color: %s %s"%(self.color,self.revert)
    def __str__(self):
        if self.revert:
            return "{c}"
        else:
            return "{c "+self.color+"}"
    def getcolor(self):
        from .assets_init import assets
        from .color_str import color_str
        if not self._color:
            self._color = color_str(assets.variables, self.color)
        return self._color

class markup_command(markup):
    def __init__(self,command,args):
        self.command = command
        self.args = args
    def __repr__(self):
        return "command: %s %s"%(self.command,self.args)
    def __str__(self):
        if self.args:
            return "{%s %s}"%(self.command,self.args)
        else:
            return "{%s}"%(self.command,)
            
class markup_variable(markup):
    def __init__(self,var):
        self.variable = var
    def __str__(self):
        return "{$%s}"%(self.variable,)
    def __repr__(self):
        return "variable: %s"%self.variable
        
class plain_text:
    def __init__(self,text):
        self.text = text
    def addcharsto(self,list):
        list.extend(self.text)
        
def to_markup(text):
    if not text.startswith("{") or not text.endswith("}"):
        return plain_text(text)
    if not text.startswith("{"):
        raise "{ mismatch"
    if not text.endswith("}"):
        raise "} mismatch"
    text = text[1:-1]
    if not text:
        return markup_command("","")
    if text[0]=="c" and (len(text)==1 or text[1].isdigit() or text[1]==" "):
        if len(text)==1:
            return markup_color(revert=True)
        return markup_color(text[1:])
    if text.startswith("$"):
        return markup_variable(text[1:])
    macro_args = text.split(" ",1)+[""]
    return markup_command(macro_args[0],macro_args[1])

class markup_text:
    """Some text that has annotations"""
    def __init__(self,text="",commands=True):
        self.commands = commands
        if isinstance(text,markup_text):
            return text
        self._text = []
        if commands and text:
            markupre = re.compile("{.*?}")
            text_segments = markupre.split(text)
            markup_segments = markupre.findall(text)
            l = text_segments
            while 1:
                if not l:
                    break
                to_markup(l.pop(0)).addcharsto(self._text)
                if l == text_segments:
                    l = markup_segments
                else:
                    l = text_segments
        else:
            self._text = [c for c in text]
    def chars(self):
        return self._text
    def text(self):
        """Returns full text without markup"""
        return "".join([x for x in self._text if not hasattr(x,"addcharsto")])
    def fulltext(self):
        """Return full text markup included"""
        return "".join([str(x) for x in self._text])
    def strip(self):
        t = markup_text("")
        l = self._text[:]
        if not l:
            return t
        while l and l[0]==" ":
            l = l[1:]
        while l and l[-1]==" ":
            l = l[:-1]
        t._text = l
        return t
    def __getitem__(self,i):
        return self._text[i]
    def __repr__(self):
        return self.fulltext()
    def __len__(self):
        return len(self._text)
    def replace(self,*args):
        self.__init__(self.fulltext().replace(*args),self.commands)
    def m_replace(self,pattern,func):
        """pattern is a function run on each character, func is how to
        replace that character. Generally used to replace macros with text
        in some way"""
        nt = []
        for c in self._text:
            if pattern(c):
                nt.append(func(c))
            else:
                nt.append(c)
        self._text = nt

line = markup_text("This is a variable: {$varX}")
line.m_replace(lambda c:hasattr(c,"variable"),lambda c:"VAR:"+c.variable)
assert str(line)=="This is a variable: VAR:varX"

def _markup_text_list(list):
    t = markup_text("")
    for l in list:
        t._text.extend(l._text)
    return t
    
assert str(_markup_text_list([markup_text("Some text."),markup_text("Some more text.")])) == "Some text.Some more text."
            
class TextRenderer(object):
    def __init__(self,pwfont=None):
        self.lastcolor = WHITE
        self.colors = {}
        self.chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ "+\
         "abcdefghijklmnopqrstuvwxyz"+\
         "!?.;[](){}\"\"@#:+,/*'_\t\r%\b~<>&`^-"
        self.width = {"":0}
        self.start = {}
        self.starty = {}
        self.quote = 0
        self.fnt = pwfont

    def _set_metrics(self, metrics, character):
        min_x = metrics[0]
        max_y = metrics[3]
        horizontal_advance_x = metrics[4]
        starty = 10-max_y

        # Cause of the previous line printing bug.
        # Defines width of characters including empty space
        self.width[character] = horizontal_advance_x
        #FIXME: hack for shorter spaces with pwinternational font, 
        # #better is to fix the actual font
        if character==" ":
            self.width[character] = 3

        self.start[character] = min_x
        self.starty[character] = starty

    def _render_surf(self, character, color):
        """
        pygame.freetype.Font.render
            render(text, fgcolor=None, bgcolor=None, style=STYLE_DEFAULT, rotation=0, size=0) 
                -> (Surface, Rect)

                The return value is a tuple: 
                the new surface and the bounding rectangle 
                giving the size and origin of the rendered text.
        """
        if (character,tuple(color)) in self.colors:
            return self.colors[(character,tuple(color))]
            
        surf = self.fnt.render(character,color)[0]

        self.colors[character,tuple(color)] = surf
        return surf

    def _cwidth(self, c):
        if isinstance(c,markup):
            cwidth = 0
        else:
            if c not in self.width:
                metrics = self.fnt.get_metrics(c)[0]
                if(metrics): self._set_metrics(metrics, c)
            cwidth = self.width.get(c, 0)
        return cwidth
    
    def split_line(self,text,max_width):
        """Returns the line split at the point to equal a desired width"""
        if not isinstance(text,markup_text):
            text = markup_text(text)
        left = [markup_text("")]
        right = [markup_text("")]
        markup_text_list = left
        width = 0
        wb = 0
        for i,c in enumerate(text._text):
            cwidth = self._cwidth(c)
            if markup_text_list == left and width+cwidth>max_width:
                r = markup_text_list.pop(-1)
                markup_text_list = right
                right.insert(0,markup_text(r.fulltext()[1:]))
            elif c == " ":
                if not markup_text_list[-1] or markup_text_list[-1][-1]!=" ":
                    markup_text_list.append(markup_text(""))
            width+=cwidth
            markup_text_list[-1]._text.append(c)
        return _markup_text_list(left),_markup_text_list(right)

    def _get_width(self, text):
        width = 0
        for c in text.chars():
            if not isinstance(c, markup):
                width+=self.width.get(c,8)
        return width

    def _cache_surfaces_and_metrics(self, text, base_color):
        '''
        Takes in text and a default color value.
        If markup color is found, the base_color will be replaced by 
        the markup color and passed on to render_surf.
        '''
        if not isinstance(text,markup_text):
            text = markup_text(text)
        self.quote = 0
        chars = []
        used_color = base_color
        for c in text.chars():
            if isinstance(c,markup_color):
                used_color = c.getcolor()
            elif isinstance(c,markup):
                chars.append([c,None])
            else:
                char = self._render_surf(c, used_color)
                metrics = self.fnt.get_metrics(c)[0]
                if(metrics): self._set_metrics(metrics, c)
                chars.append([c,char])
        width = self._get_width(text)
        return used_color, chars, width

    def _make_surf(self, chars, width, starty : dict, width_dict : dict, color):
        '''
        Likely source of the black pixel bug in textboxes.
        Caused by rendering text onto a black surface, then using colorkey
        to set black to transparent. Stray black pixels would remain on non-black text.
        To fix, use render_on_surf and pass a surface along with a location to blit on.
        '''
        surf = pygame.Surface([width,20])

        # TODO ugly hack: instead of using 0,0,0 as transparency colorkey,
        # use 250 so it wont be noticeable in textboxes
        surf.fill((0,0,0))
        surf.set_colorkey((0,0,0))
        x = 0
        for c,img in chars:
            if img:
                surf.blit(img,[x,starty.get(c, 0)])
                x += width_dict.get(c, 0)
        return surf

    def _blit_on_surf(self, surf, position, chars, width, starty : dict, width_dict : dict, color, center):
        '''
        Special method that takes a surf to blit onto.
        Prevents blackline bug from _make_surf.
        '''
        x = 0
        if center:
            x = (surf.get_width()-width)//2
        if x + width > surf.get_width():
            if not getattr(self, "OVERAGE", None):
                self.OVERAGE = x + width - surf.get_width()
                raise offscreen_text(
                    'Text Overflow:"%s" over by %s' % (line, self.OVERAGE))
        
            
        for c,img in chars:
            if not img:
                if isinstance(c,markup_color):
                    self.color = c.getcolor()
            else:
                surf.blit(img,[position[0] + x, position[1] + starty.get(c, 0)])
                x += width_dict.get(c, 0)
        return surf

    def render(self,text,base_color=WHITE,return_size=False):
        """return a surface with rendered text
        color = the starting color"""
        if return_size:
            return self._get_width(text)
        used_color, chars, width = self._cache_surfaces_and_metrics(text, base_color)
        surf = self._make_surf(chars, width, self.starty, self.width, used_color)
        return surf

    def render_on_surf(self,surf, position, text, base_color, center):
        """return a surface with rendered text
        color = the starting color"""
        used_color, chars, width = self._cache_surfaces_and_metrics(text, base_color)
        self._blit_on_surf(surf, position, chars, width, self.starty, self.width, used_color, center=center)
        return used_color

    def size(self,text):
        """return the size of the text if it were rendered"""
        return self._get_width(text)[0]
    def get_linesize(self):
        """return hieght in pixels for a line of text"""
    def get_height(self):
        """return height in pixels of rendered text - average for each glyph"""
    def get_ascent(self):
        """return hieght in pixels from font baseline to top"""
    def get_descent(self):
        """return number of pixels from font baseline to bottom"""

def wrap_text(lines,font,width,wrap=True):
    lines = [markup_text(l) for l in lines]
    page = []
    while lines:
        line = lines.pop(0)
        if wrap:
            left,right = font.split_line(line,width)
        else:
            left,right = line,markup_text("")
        page.append(left)
        if right.strip():
            if not lines: lines.append(markup_text(""))
            lines[0] = _markup_text_list([right,markup_text(" "),lines[0]])
    return page

