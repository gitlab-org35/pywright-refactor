
STANDARD_SW, STANDARD_SH = 256, 192
FRAME_DELAY = 6

#portrait
KEY_PORT = "port"
ART_FOLDER = "art/"
PATH_PORT = ART_FOLDER+KEY_PORT+"/"
LIPSYNC = "lipsync"
TALK = "talk"
BLINK = "blink"
COMBINED = "combined"
BLIPSOUNDS_FILE = "program_settings/blipsounds.txt"

#settings

SETTINGS_FILE = "program_settings/settings.ini"

#gui

TEXTBOX_FLAG = "tb"
NT_FLAG = "nt"
WHITE = "FFFFFF"
WHITE_LIST = [255, 255, 255]

#sorting
SORTING = "program_settings/newsorting.ini"

#macros

INIT_DEFAULTS = "init_defaults"
FONT_DEFAULTS = "font_defaults"
LOAD_DEFAULTS = "load_defaults"
DEFAULTS = "defaults"
TEXTBOX_ON = "tbon"
TEXTBOX_OFF = "tboff"
CR_SETTINGS = "init_court_record_settings"
CORE_MACROS = "core/macros"
MACROS_TXT = "macros.txt"
NOEXT = NOSTART = ""

DATA_FILE = "program_settings/data.txt"
PWV_EXT = ".pwv"
VERSION = "version"