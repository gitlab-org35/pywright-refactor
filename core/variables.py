from .pwvlib import *
from .new_sorting import Sorting
from .vtrue import vtrue
sorting = Sorting()

data_from_folder = get_data_from_folder(".")
VERSION = "Version "+cver_s(data_from_folder["version"])

class Variables(dict):
    """
    Variables is a modded dictionary.
    Codebase and users use it to store state about pywright and the current game.
    This constructor allows Variables to take initial values from a dictionary.
    """
    def __init__(self, assets, map={}):
        self.assets = assets
        super(Variables, self).__init__(map)

    v = cver_s(data_from_folder["version"])
    _SPEAKING_KEY = "_speaking"

    def __getitem__(self, key):
        """Trying to access a nonexistent key results in an empty string"""
        return self.get(key, "")

    def get(self, key, *args):
        """
        Get a variable according to a key.
        If the key is _debug, return "on" or "off" depending on the debug mode
        """
        if key.startswith("_layer_"):
            layer = sorting.get_z_layer(key[7:])
            if layer is not None:
                return str(layer)
        if key == "_version":
            return self.v
        if key == "_debug":
            return "on" if self.assets.debug_mode else "off"
        if key == "_num_screens":
            return str(self.assets.screen_mode.num_screens)
        if key == "_buildmode":
            return str(self.assets.get_stack_top().buildmode)
        return dict.get(self, key, *args)

    def set_speaking(self, value):
        dict.__setitem__(self, self._SPEAKING_KEY, value)
        #Allow failure
        portrait = self.assets.get_portrait()
        self["_speaking_name"] = portrait.nametag.split("\n")


    """
    speaking, music_fade, debug, screen width and screen height 
    are special cases of key with additional behavior.
    Dependency on assets methods get music vol and set music vol
    """
    def __setitem__(self, key, value, *args):
        if key == "_music_fade":
            dict.__setitem__(self, key, value, *args)
            self.assets.update_volume_fade(value)
            return
        if key == "_sw":
            self.assets.sw = int(value)
            self.assets.make_screen()
            return
        if key == "_sh":
            self.assets.sh = int(value)
            self.assets.make_screen()
            return
        if key == "_debug":
            self.assets.debug_mode = vtrue(value)
        return dict.__setitem__(self, key, value, *args)

    def set(self, key, value):
        return self.__setitem__(key, value)