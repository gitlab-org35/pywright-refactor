
class ScreenFormat():
    def __init__(self, mode, number_screens, compress):
        self.mode = mode
        self.num_screens = number_screens
        self.compress = compress

    @staticmethod
    def from_name(name):
        format = next(filter(lambda f: f.mode == name, SCREEN_FORMATS))
        if not format:
            raise ValueError("bad screen format")
        return format

TWO_SCREENS = ScreenFormat("two_screens", 2, False)
SCALED_SINGLE = ScreenFormat("scaled_single", 1, False)
SHOW_ONE = ScreenFormat("show_one", 2, True)
HORIZONTAL = ScreenFormat("horizontal", 2, False)
SMALL_BOTTOM = ScreenFormat("small_bottom_screen", 2, False)

SCREEN_FORMATS = [TWO_SCREENS,SCALED_SINGLE,SHOW_ONE, HORIZONTAL, SMALL_BOTTOM]
