from . import extensions
import os 
from chardet import detect
from .errors import *
from .sprite_metadata import Sprite_Metadata

def get_sound_path(game_folder, track, type, folder):
    try:
        p = get_path(game_folder, track, type, folder)
        p = os.path.normpath(p)
    except:
        try:
            p = os.path.normpath(get_path(game_folder, 
                track.capitalize(), type, folder))
        except:
            raise art_error("File named "+track+
                " could not be read. Make sure you spelled it properly and check case.")
    return p

def get_path(game_folder, track, type, folder=""):
        possible_names = [track]
        # Unknown extension, make sure to check all extension types
        if extensions.noext(track) == track:
            for ext in extensions.extensions_for_types([type]):
                possible_names.insert(0, track+ext)
        # Get parent game folder, in case we are in a case folder
        game = parent_folder(cleanup(game_folder))
        if folder:
            folder = "/"+folder+"/"
        else:
            folder = "/"
        for name in possible_names:
            attempts = [game_folder+folder+name, game+folder+name, folder[1:]+name]
            for attempt in attempts:
                if os.path.exists(attempt):
                    return attempt
        raise file_error("not found: " + attempt)
    
def get_encoding_type(file):
    #TODO: fix attempt to access file:  'games/PW - The Contempt of Court/Turnabout Scapegoat/local://builtin.txt'
    f = open(file, 'rb')
    rawdata = f.read()
    return detect(rawdata)['encoding']

def macro_files_in_address(address, ext=".mcro"):
    return [f for f in os.listdir(address) if f.endswith(ext)]

def cleanup(path_string):
    return path_string.replace("\\","/")

def parent_folder(path_string):
    return path_string.rsplit("/",1)[0]

def raw_lines(game_folder, name, ext=".txt", start="game"):
    if start == "game":
        start = game_folder
    if start:
        start = start+"/"
    if name.endswith(".txt"):
        ext = ""
    
    try:
        # TODO: failed access to file: 'games/PW - The Contempt of Court/Turnabout Scapegoat/local://builtin.txt'
        file = open(os.path.normcase(start+name+ext), "r",
                    encoding=get_encoding_type(os.path.normcase(start+name+ext)))
    except IOError:
        try:
            name = name.capitalize()
            file = open(os.path.normcase(start+name+ext), "r",
                    encoding=get_encoding_type(os.path.normcase(start+name+ext)))
            open()
        except:
            raise file_error("File named "+start+name+ext +
                            " could not be read. Make sure you spelled it properly and check case.")

    text = file.read()
    #change code so unicode is always used, because its never called without it.
    text = text.replace('\ufeff', '')
    return text.split("\n")

def search_locations(game_folder, search_path, name):
    """
    If the searched file exists under the game folder,
    under search_path, or under the case folder,
    get the desired file. Otherwise, return None.

    NOTE: this method used to chnge the game_folder variable permanently
    to change the separators. Now this is done in a setter method.
    """
    case = game_folder
    game = parent_folder(game_folder)

    if os.path.exists(case+"/"+search_path+"/"+name):
        return case+"/"+search_path+"/"+name

    if os.path.exists(game+"/"+search_path+"/"+name):
        return game+"/"+search_path+"/"+name

    if os.path.exists(search_path+"/"+name):
        return search_path+"/"+name

def open_art_textfile(registry, directory, name):
    """
    looks up the .txt file attached to [name] in [directory].
    If found, it is returned as a meta object
    """
    dirname = directory+name+".txt"
    dirname_noext = directory+name.rsplit(".", 1)[0]+".txt"
    textpath = registry.lookup_with_extension(dirname)
    if not textpath:
        textpath = registry.lookup_with_extension(dirname_noext)

    if textpath:
        try:
            f = registry.open(textpath)
            meta = Sprite_Metadata(f.read())
            f.close()
        except:
            import traceback
            traceback.print_exc()
            raise art_error("Art textfile corrupt:"+textpath)
    else:
        meta = Sprite_Metadata()
    return meta
    
def open_art_file(registry, directory, name):
        '''
        Looks for an art file, trying a list of image file extensions
        '''
        file = registry.lookup((directory+name).replace(".zip/", "/"))
        if not file: raise art_error("No such art file exists: "+directory+name)
        return file
    
def set_up_texture(registry, pygame, artpath, key):
        file = registry.open(artpath)
        texture = pygame.image.load(file, artpath)
        if texture.get_flags() & pygame.SRCALPHA:
            texture = texture.convert_alpha()
        else:
            texture = texture.convert()
        if key:
            texture.set_colorkey(key)

        return texture
    
class ImgFrames(list):
    pass

def setup_image_frames(pygame, path, texture, local_meta):
    image_frames = []
    x = 0
    y = 0
    width, height = texture.get_size()
    incx = width//local_meta.horizontal
    incy = height//local_meta.vertical
    for frame in range(local_meta.length):
        image_frames.append(texture.subsurface(
            pygame.Rect((x, y), (incx, incy))))
        x += incx
        if x >= width:
            x = 0
            y += incy
    image_frames = ImgFrames(image_frames)
    image_frames._meta = local_meta
    image_frames.real_path = path
    return image_frames

def get_image_data(registry, directory, name, pygame, colorkey):
    local_meta = open_art_textfile(registry, directory, name)
    artpath = open_art_file(registry, directory, name)
    texture = set_up_texture(registry, pygame, artpath, colorkey)
    image_frames = setup_image_frames(pygame, artpath, texture, local_meta)
    return image_frames, artpath, local_meta

def tries(name, type):
    tries = [name]
    for ext in extensions.extensions_for_types(["image"]):
        tries.append(name+ext)
    return tries
