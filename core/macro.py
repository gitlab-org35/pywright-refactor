import os
from . import file_system
from core.constants import *

def parse_macros(lines):
    """Alters lines to not include macro definitions, and returns macro dictionary"""
    macros = {}
    mode = "normal"
    i = 0
    while i < len(lines):
        line = lines[i]
        if line.startswith("macro "):
            del lines[i]
            i -= 1
            mode = "macro"
            macroname = line[6:].strip()
            macrolines = []
        elif mode == "macro":
            del lines[i]
            i -= 1
            if line == "endmacro":
                mode = "normal"
                macros[macroname] = macrolines
            else:
                macrolines.append(line)
        i += 1
    return macros

def __is_curlybraces__(line):
    # TODO: please make a more thorough macro check
    return line.startswith("{")

def __isassignment__(line):
    return line.count("=") == 1

def __parse_macro__(line):
    # drop curly braces and split by spaces
    return line[1:-1].split(" ")

def expand_macro_from_args(macro_name, macro_args, lineno, macros):
    '''
    None if macro_name not in macros
    '''
    if macro_name not in macros:
        return None
    # construct the list of replacements
    ## assignments represented as (k, v) pairs
    named_arguments = list(map(
        lambda s: s.split("="),
        filter(__isassignment__, macro_args)))
    ## anonymous arguments represented as *0-indexed* (i, v) pairs
    numbered_arguments = list(enumerate(filter(
        lambda s: not __isassignment__(s),
        macro_args)))
    ## replacements represented as (searchstr, output) pairs
    ## `(1,2,*(3,4))` flattens to (1,2,3,4)
    replacements = ( ("$0", lineno),
        *map(lambda tuple: (f"${ tuple[0]+1 }", tuple[1]), numbered_arguments),
        *map(lambda tuple: (f"${ tuple[0] }", tuple[1]), named_arguments))

    # fill in a template line
    def fill(template):
        out = template
        for k, v in replacements:
            out = out.replace(k, str(v))
        return out
    return list(map(fill, macros[macro_name]))


def replace_macros(lines, macros):
    '''
    lines: List of strings that represent lines of code.
    outputs the lines, but if a line is a macro, 
    it is replaced with the value of that macro.
    '''
    out = lines[:]
    # we'll keep an explicit counter instead of enumerating
    # because macro expansion changes line numbers
    i = 0
    while i < len(out):
        line = out[i]
        if __is_curlybraces__(line):
            macro_name, *macro_args = __parse_macro__(line)
            expansion = expand_macro_from_args(macro_name, macro_args, i, macros)
            if expansion is not None:
                # splice-in the macro expansion
                out[i:i+1] = expansion
        else: i += 1
    return out

def fetch_macro_definitions(game_folder):
    '''
    Get macros from .macro files in core/macros, the game folder and case folder,
    as well as macros.txt
    '''
    
    lines_list = []

    for f in file_system.macro_files_in_address(CORE_MACROS):
        lines_list.append(file_system.raw_lines(game_folder,CORE_MACROS+"/"+f, NOEXT, NOSTART))

    for pth in [file_system.parent_folder(game_folder), game_folder]:
        if os.path.exists(pth+"/"+MACROS_TXT):
            # check macros.txt in game and case folders
            lines_list.append(file_system.raw_lines(game_folder, MACROS_TXT, NOEXT, start=pth))
        for f in file_system.macro_files_in_address(pth):
            # check .mcro files in game and case folders
            lines_list.append(file_system.raw_lines(game_folder, f, NOEXT, start=pth))

    combined_macros = {}
    for lines in lines_list:
        combined_macros.update(parse_macros(lines))
    return combined_macros

#TODO now in macro
def open_script_no_includes(name, ext, game_folder):
    lines = file_system.raw_lines(game_folder, name, ext)
    return [line.strip() for line in lines]

def _remove_include(line):
    return line[len("include "):].strip()

def resolve_includes(lines, ext, game_folder):
    reallines = []
    for line in lines:
        if line.startswith("include "):
            lines = open_script_no_includes(
                name=_remove_include(line), ext=ext, game_folder=game_folder)
            reallines.extend(lines)
        else:
            reallines.append(line)
    return reallines