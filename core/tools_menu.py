import pygame,sys
from . import settings
from . import textutil
from . import gui

class msg(gui.pane):
    def __init__(self,m,assets):
        gui.pane.__init__(self)
        #self.files = scrollpane([10,20])
        self.view = gui.scrollpane([10,20])
        self.view.width = 250
        self.view.height = 155
        self.children.append(self.view)
        self.width = 256
        self.height = 160
        self.pri = -1001
        self.z = 11001
        self.rpos = [0,100]
        self.align = "vert"
        
        for line in textutil.wrap_text([m],assets.get_image_font("block_arial"),300):
            text = gui.label(line.fulltext())
            self.view.add_child(text)#children.append(text)
    def click_down_over(self,*args):
        self.delete()
    def delete(self):
        self.kill = 1
        super(msg,self).delete()

class tools_menu(gui.pane):
    def __init__(self,*args,**kwargs):
        self.sw=kwargs["sw"]
        self.sh=kwargs["sh"]
        self.assets=assets=kwargs["assets"]
        gui.widget.__init__(self)
        self.width = 1000
        self.height = 1000
        self.pri = -1001
        self.z = 11001
        self.align = False
            
        self.total_height = assets.screen_data.total_height
        self.total_width = assets.screen_data.total_height
        self.base()
    def make_button(self,text,pos):
        b = gui.button(self,text,pos)
        self.children.append(b)
        return b
    def base(self):
        assets = self.assets
        sw,sh = self.sw,self.sh
        sh = assets.sh*assets.num_screens
        self.children[:] = []
        #self.make_button("gif2strip",[0,10])
        #self.make_button("aao2pywright experimental",[0,30])
        #all tools are currently deprecated
        self.make_button("close tools",[0,30])#50
    '''
    def gif2strip(self):
        assets = self.assets
        self.files_dir = gui.directory([0,0])
        if ",," in assets.tool_path:#this is just a patch, whatever is wrong needs to be actually fixed
            assets.tool_path = assets.tool_path.replace(",,", "")
        self.files_dir.populate(assets, assets.tool_path, self,"giffile",lambda x: x.endswith(".gif"),False)
        self.children.append(self.files_dir)
    def aao2pywright_experimental(self):
        assets = self.assets
        sw,sh = self.sw,self.sh
        #settings_menu.firstpane = "saves"
        self.firstpane = "saves"
        self.base()
        line = gui.pane([0,30],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        line.children.append(gui.label("Autosave?"))
        class myb(gui.checkbox):
            def click_down_over(self,*args):
                super(myb,self).click_down_over(*args)
                if self.checked:
                    assets.autosave = 1
                else:
                    assets.autosave = 0
                settings.write_init_file(assets)
        line.children.append(myb("autosave"))
        cb = line.children[-1]
        if assets.autosave: cb.checked = True
            
        line = gui.pane([0,50],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        line.children.append(gui.label("Minutes between autosave"))
        class mymin(gui.editbox):
            def insert(self,val):
                if val not in "0123456789":
                    return
                super(mymin,self).insert(val)
            def set(self,val):
                super(mymin,self).set(val)
                if not val:
                    val = 0
                assets.autosave_interval = int(val)
                settings.write_init_file(assets)
        self.autosave_interval = str(assets.autosave_interval)
        line.children.append(mymin(self,"autosave_interval"))
            
        line = gui.pane([0,70],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        line.children.append(gui.label("Autosave backups"))
        class mye(gui.editbox):
            def insert(self,val):
                if val not in "0123456789":
                    return
                super(mye,self).insert(val)
            def set(self,val):
                super(mye,self).set(val)
                if not val:
                    val = 0
                assets.autosave_keep = int(val)
                settings.write_init_file(assets)
        self.autosave_keep = str(assets.autosave_keep)
        line.children.append(mye(self,"autosave_keep"))
    '''
    def update(self,*args):
        assets = self.assets
        self.rpos = [0,0]
        self.pos = self.rpos
        self.children[:] = [x for x in self.children if not getattr(x,"kill",0)]
        for x in self.children:
            x.update()#gifsicle
        if getattr(self,"giffile",""):
            sys.path.append("tools")
            # RIP gif2strip
        return True
    def close_tools(self):
        self.delete()
    def delete(self):
        self.kill = 1
