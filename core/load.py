
from .gui_elements import *
from core.gui_elems.portrait import portrait
def load(script,olist):
    f = None
    cls,args,props = olist
    #print(olist)
    #print(props)
    from .assets_init import assets
    if cls == "bg":
        o = bg(assets)
    if cls == "testimony_blink":
        o = testimony_blink(*args)
    if cls == "fg":
        o = fg(assets)
    if cls in ["bg","fg","testimony_blink"]:
        for p in props:
            setattr(o,p,props[p])
        o.load(o.name)
    if cls == "evidence":
        o = evidence(assets, props["id"],page=props["page"])
    if cls == "char":
        o = portrait(assets, *args)
        if "blinkspeed" in props:
            o.blink_sprite.blinkspeed = props["blinkspeed"]
        if "blinkemo" in props:
            o.set_blink_emotion(props["blinkemo"])
        if "tsprite" in props:
            for k in props["tsprite"]:
                setattr(o.talk_sprite,k,props["tsprite"][k])
        if "bsprite" in props:
            for k in props["bsprite"]:
                setattr(o.blink_sprite,k,props["bsprite"][k])
    if cls == "mesh":
        o = mesh(assets, *args)
        print("mesh loaded",o)
        def f(o=o,props=props):
            o.load(script)
    if cls == "surf3d":
        print("load surf3d")
        o = surf3d(assets, *args)
    if cls == "ev_menu":
        items = []
        #TODO: reimplement
    if cls == "scroll":
        o = scroll(assets)
        def f(o=o,props=props):
            o.objects = []
            for o2 in script.objects:
                if getattr(o2,"id_name",None) in props["ob_ids"]:
                    o.objects.append(o2)
    if cls == "zoomanim":
        o = zoomanim(assets)
        def f(o=o,props=props):
            o.objects = []
            for o2 in script.objects:
                if getattr(o2,"id_name",None) in props["ob_ids"]:
                    o.objects.append(o2)
    if cls == "rotateanim":
        o = rotateanim(assets)
        def f(o=o,props=props):
            o.objects = []
            for o2 in script.objects:
                if getattr(o2,"id_name",None) in props["ob_ids"]:
                    o.objects.append(o2)
    if cls in ["fadeanim","tintanim"]:
        #print(cls)
        o = {"fadeanim":fadeanim,"tintanim":tintanim}[cls](assets)
        def f(o=o,props=props):
            o.objects = []
            for o2 in script.objects:
                if getattr(o2,"id_name",None) in props["ob_ids"]:
                    o.objects.append(o2)
    if cls == "textbox":
        o = textbox(assets, *args)
    if cls == "textblock":
        o = textblock(*args)
    if cls == "uglyarrow":
        o = uglyarrow(assets)
        if props.get("_tb",""):
            def f(o=o,props=props):
                for tb in script.world.all:
                    if isinstance(tb,textbox):
                        o.textbox = tb
                o.update()
    if cls == "penalty":
        o = penalty(*args)
    if cls == "menu":
        o = menu(assets)
        def f(o=o,props=props):
            if o.options and not getattr(o,"selected",""):
                o.selected = o.options[0]
    if cls == "listmenu":
        o = listmenu(assets)
    if cls == "examinemenu":
        o = examine_menu(assets, props["hide"])
        o.bg = []
        def f(o=o,props=props):
            for o2 in script.objects:
                if getattr(o2,"id_name",None) in props["bg_ids"]:
                    o.bg.append(o2)
    if cls == "guiWait":
        o = guiWait(assets)
        o.script = script
    if cls == "button":
        o = ws_button(assets.screen_data.screen_mode, None,props["s_text"])
        if props.get("s_graphic",""):
            graphic = props["s_graphic"]
            graphic = assets.open_art(graphic)[0]
            o.graphic = graphic
        def func(*args):
            script.goto_result(props["s_macroname"])
        setattr(o,props["s_text"].replace(" ","_"),func)
        if props["hold_func"]:
            setattr(o,"hold_down_over",func)
    if cls == "waitenter":
        o = waitenter(assets)
    if cls == "delay":
        o = delay(assets)
    if cls == "timer":
        o = timer(assets)
        o.script = script
    for p in props:
        setattr(o,p,props[p])
    o.cur_script = script
    return o,f
