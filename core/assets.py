
import pygame
import pygame.freetype
import pygame.mixer as mixer
from .pwvlib import *
from . import textutil
import time
import os
from . import file_system
from .errors import *
from . import fonts as fontslib
TextRenderer = textutil.TextRenderer
from . import registry
from .screen_format import *
from . import constants
from .vtrue import vtrue
from .variables import Variables
from .variables import VERSION
from . import loadsave
import warnings
from .gui_elements import *
from . import settings

pygame.freetype.init()
class empty_stack_error(Exception):
    pass

data_from_folder = get_data_from_folder(".")
VERSION = "Version "+cver_s(data_from_folder["version"])
from . import gui
TextRenderer = textutil.TextRenderer

def make_button(name, func, position, z,pri):
    item = gui.main_menu_button(func, name)
    item.bordercolor = [255,255,255]
    item.rpos = position
    item.z = z
    item.pri = pri
    return item

class Music_Assets(object):
    NO_SOUND_FLAGS = ["0%", "/null", "no"]
    def __init__(self):
        self._loop = False
        self._track = None
        self.snds = {}
        self.sound_format = 44100
        self.sound_bits = 16
        self.sound_sign = -1
        self.sound_buffer = 4096
        self.sound_init = 0
        self.sound_volume = 100
        self.sound_paused = False
        self._music_vol = 100
        self.mute_sound = 0

    def set_volume(self, music_fade, volume):
        mixer.music.set_volume(
                volume/100.0*music_fade/100.0)

    
    def set_music_vol(self, v):
        """
        Setter for music volume.
        depends on assets initialization in core.py
        """
        self._music_vol = v
        
    def get_music_vol(self):
        """
        Getter for music volume.
        """
        return self._music_vol
    music_volume = property(get_music_vol, set_music_vol)

    def set_mute_sound(self, value):
        self.mute_sound = value

    def _initialize(self):
        mixer.pre_init(self.sound_format, self.sound_sign *
                        self.sound_bits, 2, self.sound_buffer)
        mixer.init()
        self.sound_init = 1
        self.music_volume = self._music_vol
        return True

    def init_sound(self, reset=False):
        """If not initialized, initialize mixer with data like sound format, 
        sound sign, sound bits and buffer.
        failure to initialize results in an exception
        """
        self.sound_repeat_timer = {}
        self.min_sound_time = 0.01

        # we are initialized and caller does not want reset
        if self.sound_init and not reset:
            return True
        self.snds = {}
        try:
            self._initialize()
        except:
            raise Exception("Sound issue")

    def open_music(self, game_folder, track, pre="music"):  # data/
        from . import file_system
        p = file_system.get_sound_path(game_folder, track, "music", pre)
        try:
            mixer.music.load(p)
        except: 
            raise art_error("Couldnt load music from path" + p)
        return True

    def open_movie(self, movie):
        # FIXME: open movies some other way
        raise script_error("pygame.movie is deprecated :(")

    def _sound_is_muted(self):
        return not self.sound_volume or self.mute_sound

    def _await_repeater(self, name):
        '''
        handle gap between repeated sound effects using dictionary
        '''
        if (name in self.sound_repeat_timer):
            delta_time = time.time()-self.sound_repeat_timer[name]
            return (delta_time < self.min_sound_time)
        else:
            return False

    def _set_repeater(self, name):
        self.sound_repeat_timer[name] = time.time()

    def play_sound(self, game_folder, name, wait=False, volume=1.0, offset=0, frequency=1, layer=0):
        self.init_sound()
        if self.sound_init == -1 :
            raise art_error("Sound could not be initialized.")

        if self._sound_is_muted(): return
        if self._await_repeater(name): return
        self._set_repeater(name)
        
        from . import file_system
        try:
            path = file_system.get_sound_path(game_folder, name, "sound", "sfx")
        except:
            if name in self.NO_SOUND_FLAGS:
                return None
            else:
                raise art_error("Soundfile could not be found: " + name)

        if path in self.snds:  # self.snds.get(path,None):
            snd = self.snds[path]
        else:
            snd = mixer.Sound(path)
            self.snds[path] = snd
            
        if not layer: snd.stop()
        snd.set_volume(float(self.sound_volume/100.0)*volume)
        channel = snd.play()
        return channel

    def play_music(self, assets, game_folder, track=None, loop=False, pre="music", reset_track=True):
        if reset_track:
            loop = True
            assets.variables["_music_loop"] = track
        self.init_sound()
        if self.sound_init == -1 or self.mute_sound:
            return
        self._track = track
        self._loop = loop
        if track:
            track = self.open_music(game_folder, track, pre)
            try:
                mixer.music.play()
            except:
                raise art_error("Could not play() mixer")
        else:
            self.stop_music()

    def stop_music(self):
        if self.sound_init == -1:
            return
        self._track = None
        self._loop = False
        try:
            mixer.music.stop()
            # sd.stop()
            # sa.stop()
        except:
            import traceback
            traceback.print_exc()

    def pause_sound(self):
        self.sound_paused = True
        mixer.pause()
        mixer.music.pause()

    def resume_sound(self):
        self.sound_paused = False
        mixer.unpause()
        mixer.music.unpause()
        
    def reset_state(self):
        mixer.stop()
        self.sound_paused = False


TEXT_FONT = "pwinternational.ttf"
TEXT_SIZE = "10"
FONT_KEY = "_font_%s"
FONT_SIZE = "_font_%s_size"
FONT_FOLDER = "fonts"

class Assets(object):
    '''
    Assets is treated as a singleton, but a variable may be passed
    to allow for multiple instances to allow testing. 
    '''
    INSTANCE_COUNT = 0

    def get_sw(self):
        return self.screen_data.sw
    def set_sw(self,v):
        self.screen_data.sw = v
    sw = property(get_sw, set_sw)

    def get_sh(self):
        return self.screen_data.sh
    def set_sh(self,v):
        self.screen_data.sh = v
    sh = property(get_sh, set_sh)

    def get_gba(self):
        return self.screen_data.gbamode
    def set_gba(self,v):
        self.screen_data.gbamode = v
    gbamode = property(get_gba, set_gba)

    def get_scref(self):
        return self.screen_data.screen_refresh
    def set_scref(self,v):
        self.screen_data.screen_refresh = v
    screen_refresh = property(get_scref, set_scref)

    def get_screenmode(self):
        return self.screen_data.screen_mode
    def set_screenmode(self,v):
        self.screen_data.screen_mode = v
    screen_mode = property(get_screenmode, set_screenmode)
        
    def __init__(self, singleton=True):
        if(singleton):
            Assets.INSTANCE_COUNT += 1
            if(Assets.INSTANCE_COUNT != 1):
                raise ValueError("Multiple instances of Assets singleton detected. Quitting...")

        self.framerate = 60
        self.lists = {}
        self.snds = {}
        from core.engine import screen
        self.screen_data = screen.PyGameScreen()
        from core.engine.scriptinterpreter import ScriptInterpreter
        self.interpreter = ScriptInterpreter()
        self.last_autosave = 0
        self.autosave_interval = 0
        self.path = ""
        self.tool_path = ""
        self.game_folder = None
        self.debug_mode = False
        self.playing_speed = 1
        self.px = 0
        self.py = 0
        self.pz = None
        self.fonts = {}
        self.deffonts = None
        self.variables = Variables(self)
        self.music_assets = Music_Assets()
        self.init_sound()
        pygame.init()
        settings.load(self)
        self.game_path = "menu"
        self.items = []

    def queue_effects(assets, script):
        '''
        append effects according to attributes (assets.flash and asset. shakeargs)
        '''
        if assets.flash:
            from .effects import flash
            try:
                fl = flash(assets)
                assets.get_stack_top().add_object(fl, False)
                fl.ttl = assets.flash
                if hasattr(assets,"flashcolor"):
                    fl.color = assets.flashcolor
            except (art_error,script_error) as e:
                script.handle_error(e)
            assets.flash = 0
            assets.flashcolor = [255,255,255]
            
        if assets.shakeargs != 0:
            try:
                assets.get_stack_top()._shake("shake",*assets.shakeargs)
            except (art_error,script_error) as e:
                script.handle_error(e)
            assets.shakeargs = 0

    def top_draw_screen(assets, script):
        assets.screen_data.screen.blit(assets.screen_data.blank,[0,0])
        try:
            assets.get_stack_top().draw(assets.screen_data.screen)
        except (art_error,script_error) as e:
            script.handle_error(e, script)

        assets.queue_effects(script)

        if assets.variables.get("render",1):
            assets.draw_screen(assets.screen_data.show_fps)

    def set_game_folder(self, value):
        self.game_folder = file_system.cleanup(value)

    def get_stack(self):
        '''
        Create a new stack based on assets.stack.
        '''
        stack = []
        for s in self.stack:
            def gp(s, i):
                si = s.si+i
                if si < 0:
                    return "PRE"
                if si >= len(s.scriptlines):
                    return "END"
                if not s.scriptlines:
                    return "NOSCRIPT"
                return s.scriptlines[si]
            p0 = gp(s, -1)
            p1 = gp(s, 0)
            p2 = gp(s, 1)
            stack.append([p0, p1, p2])
        return stack

    def update_volume_fade(self, fade):
        self.music_assets.set_volume(int(fade), self.get_music_volume())

    def set_music_volume(self, v):
        self.music_assets.set_music_vol(v)
        self.music_assets.set_volume(int(self.variables.get("_music_fade", "100")), v)

    def get_music_volume(self):
        return self.music_assets.get_music_vol()
    music_volume = property(get_music_volume, set_music_volume)
    
    def set_sound_volume(self, v):
        self.music_assets.sound_volume = v
        # self.music_assets.set_volume(int(self.variables.get("_music_fade", "100")), v)

    def get_sound_volume(self):
        return self.music_assets.sound_volume
    sound_volume = property(get_sound_volume, set_sound_volume)

    def set_mute_sound(self, value):
        return self.music_assets.set_mute_sound(value)

    def _appendgba(self):
        if not self.gbamode:
            return ""
        return "_gba"
    appendgba = property(_appendgba)
        
    def open_script(self, name, handle_macros=True, ext=".txt", game_folder = None):
        """Takes the raw lines, strips them, and processes macros if macros=True.

        Args:
            name ([type]): [description]
            macros (bool, optional): [description]. Defaults to True.
            ext (str, optional): [description]. Defaults to ".txt".

        Returns:
            lines: the processed lines.
        """
        from . import macro
        game_folder = game_folder if game_folder else self.game_folder
        if handle_macros:
            lines = macro.open_script_no_includes(name, ext, game_folder)
            lines = macro.resolve_includes(lines, ext, game_folder)
            macros = macro.fetch_macro_definitions(game_folder)
            macros.update(macro.parse_macros(lines))
        else:
            lines = macro.open_script_no_includes(name, ext, game_folder)
            macros = macro.fetch_macro_definitions(game_folder)
        self.macros = macros
        return lines

    def casemenu(self, temp_casemenu):
        return case_menu(self, self.game_folder, temp_casemenu)

# Assets: font handling
    def open_font(self, name, size):
        # cache handling
        CACHE_KEY = name+"."+str(size)
        if CACHE_KEY in self.fonts:
            return self.fonts[CACHE_KEY]

        # file open
        pth = file_system.search_locations(self.game_folder, FONT_FOLDER, name)
        font = pygame.freetype.Font(pth, size)  # font

        self.fonts[CACHE_KEY] = font
        return font

    def _get_fonts(self):
        if not self.deffonts:
            self.deffonts = fontslib.get_fonts()
        return self.deffonts

    def _resolve_fn_size(self, name):
        defs_lib = self._get_fonts()
        defs_user = self.variables

        fn = size = None
        for _dict in [defs_user, defs_lib]:
            if not fn and FONT_KEY%name in _dict: fn = _dict[FONT_KEY%name] 
            if not size and FONT_SIZE%name in _dict: size = _dict[FONT_SIZE%name]
        return fn, size

    def get_font(self, name):
        fn, size = self._resolve_fn_size(name)
        return self.open_font(fn, int(size))

    def get_text_renderer(self, name):
        fn, size = self._resolve_fn_size(name)
        # get cache
        RENDERER_KEY = fn+"."+size+".i"
        if RENDERER_KEY in self.fonts:
            return self.fonts[RENDERER_KEY]

        font = self.open_font(fn, int(size))
        text_renderer = TextRenderer(font)

        # set cache
        self.fonts[RENDERER_KEY] = text_renderer
        return text_renderer

    # pygame surface class wrapper.
    def Surface(self, size, flags=0):
        return pygame.Surface(size, flags)

    def _open_art_nocache_(self, name, registry, colorkey=None):
        directory = "art/"
        image_frames, path, meta = \
            file_system.get_image_data(registry, directory, name, pygame, colorkey)
        return image_frames, path, meta
    
    def _open_art_(self, name, registry, key=None, cache=None):
        """Returns list of frame images. 
        If the [name] image is cached, it uses the cache.
        Otherwise, the image is loaded from the directory.
        Each frame image may come with an attached .txt metadata file.
        If the file exists,
        failing to read it raises an art error.ss
        """
        if name in cache:
            img = cache[name]
            return img, img.real_path, img._meta, cache

        image_frames, path, meta = self._open_art_nocache_(name, registry, key)
        cache[name] = image_frames
        return image_frames, path, meta, cache

    def open_art(self, name, key=None):
        """Try to open an art file.  Name has no extension.
        Will open gif, then png, then jpg.  Returns list of 
        frame images"""
        import traceback
        self.real_path = None
        for t in file_system.tries(name, "image"):
            try:
                if not self.stack_empty():
                    frames, path, meta, cache = self._open_art_(t, self.registry, key, self.get_stack_top().imgcache)
                    self.real_path = path
                    self.meta = meta
                    self.get_stack_top().imgcache = cache
                else: 
                    frames, path, meta = self._open_art_nocache_(t, self.registry, key)
                return frames
            except art_error as e:
                pass
            except (IOError, ImportError, pygame.error, TypeError) as e:
                print(str(e))
                print("TYPE ERROR WHILE OPENING "+t)
                traceback.print_tb(e.__traceback__)
                pass
        traceback.print_exc()
        raise art_error("Art file corrupt or missing:"+name +"\n-------------------------")

    def get_frame_delay(self, type=None):
        if type == "port":
            key = "_default_port_frame_delay"
        elif type == "fg":
            key = "_default_fg_frame_delay"
        else:
            key = "_default_frame_delay"
        return int(self.variables[key]) if key in self.variables else constants.FRAME_DELAY
    
    def init_sound(self, reset=False):
        return self.music_assets.init_sound(reset)

    def open_movie(self, movie):
        # FIXME: open movies some other way
        raise script_error("pygame.movie is deprecated :(")

    def list_casedir(self):
        return os.listdir(self.game_folder)

    def handle_error(self, e):
        self.get_stack_top().handle_error(e)

    def play_sound(self, name, wait=False, volume=1.0, offset=0, frequency=1, layer=0):
        return self.music_assets.play_sound(self.game_folder, name, wait, volume, offset, frequency, layer)
    
    # used by settings and script.py
    def play_music(self, track=None, loop=0, pre="music", reset_track=True):
        return self.music_assets.play_music(self, self.game_folder, track, loop, pre, reset_track)
    
    def stop_music(self):
        return self.music_assets.stop_music()

    def music_update(self):
        if self.music_assets._track and not mixer.music.get_busy():
            if self.variables.get("_music_loop", None) and self.music_assets.sound_paused == False:
                try:
                    self.music_assets.play_music(self, self.game_folder, self.variables["_music_loop"], self.music_assets._loop)
                except art_error:
                    old_track = self.music_assets._track
                    self.music_assets._track = None
                    raise art_error("Problem playing track: " + old_track)


    def pause_sound(self):
        return self.music_assets.pause_sound()

    def resume_sound(self):
        return self.music_assets.resume_sound()

    def set_emotion(self, e):
        """Sets the emotion of the current portrait"""
        if not self.portrait:
            self.add_portrait(self.character+"/"+e+"(blink)")
        if self.portrait:
            self.portrait.set_emotion(e)
    flash = 0  # Tells main to add a flash object
    flashcolor = [255, 255, 255]
    shakeargs = 0  # Tell main to add a shake object
    
    """
    ------ DEFINE cur_script ---------
    """
    def get_stack_top(self):
        try:
            return self.stack[-1]
        except:
            raise empty_stack_error("Script stack is empty.")
        
    cur_script = property(get_stack_top)

    """
    ------ PORTRAIT property ---------
    """
    def get_portrait(self):
        from core.gui_elems.portrait import portrait
        id_name = self.variables.get("_speaking", "")
        if isinstance(id_name, portrait):
                return id_name
        ports = []
        for an_object in self.get_stack_top().objects:
            if isinstance(an_object, portrait) and not getattr(an_object, "kill", 0):
                ports.append(an_object)
                if getattr(an_object, "id_name", None) == id_name:
                    return an_object
        for an_object in self.get_stack_top().objects:
            if isinstance(an_object, portrait) and not getattr(an_object, "kill", 0):
                ports.append(an_object)
                if getattr(an_object, "charname", None) == id_name:
                    return an_object
        if len(ports) > 0:
            return ports[0]
        return None

    portrait = property(get_portrait)

    def add_portrait(self, name, fade=False, stack=False, hide=False):
        from core.gui_elems.portrait import portrait
        if hide:
            stack = True
        assets = self
        self = self.get_stack_top()
        if not stack:
            [(lambda o:setattr(o, "kill", 1))(o)
             for o in self.objects if isinstance(o, portrait)]
        #assets.variables.set_speaking(None)
        p = portrait(assets, name, hide)
        p.pos[0] += assets.px
        p.pos[1] += assets.py
        if fade:
            p.fade = 0
        if assets.pz is not None:
            p.z = assets.pz
            assets.pz = None
        self.objects.append(p)
        assets.variables["_speaking"] = p.id_name
        if stack:
            p.was_stacked = True
        return p

    def clear(self):
        self.variables.clear()
        while self.items:
            self.items.pop(0)
        self.stop_music()
        self.lists = {}
        # no longer clear the font cache
        # self.fonts = {}

    """
    Core saving funtionality. Save attributes of Assets class (and Music_Assets),
    then save the entire inventory under key "items"
    then save all items from 'variables' class under key 'variables', 
    handling _speaking variable as a special case.
    Return list ["Assets", [], props, None] (Why????)

    """
    def load(self, properties):
        return loadsave.load(self, properties)

    def save(self):
        #serialize assets and the 'variables' dictionary
        assets_save = ["Assets", [], loadsave.save(self), None]
        script_saves = []

        #serialize stack
        for script in self.get_stack():
            if script.save_me:
                script_saves.append(script.save())
        return [assets_save] + script_saves
    
    def combine_registries(self):
        from . import registry
        return registry.combine_registries("./"+self.game_folder, self.show_load)

    def after_load(self):
        self.registry = self.combine_registries()
        self.last_autosave = time.time()
        itemobs = []
        for x in self.items:
            if isinstance(x, dict):
                itemobs.append(evidence(self, x["id"], page=x["page"]))
            else:
                itemobs.append(evidence(self, x))
        self.items = itemobs
        # TODO: not use variables as temporary storage of dict
        v = self.variables.copy()
        self.variables = Variables(self, v)
        if self.music_assets._track:
            self.music_assets.play_music(self, self.game_folder, self.music_assets._track, self.music_assets._loop, reset_track=False)

    def show_load(self):
        self.make_screen()
        txt = "LOADING..."
        txt = self.get_font("loading").render(txt, [200, 100, 100])[0]  # 1,
        self.screen_data.screen.blit(txt, [50, 50])
        self.draw_screen(0)
        time.sleep(0.05)

    def save_game(self, filename="save", hide=False):
        return loadsave.save_game(self, filename, hide)

    def load_game(self, path=None, filename="save", hide=False):
        return loadsave.load_game(self, path, filename, hide)

    def vdefault(self, var):
        """Return default value for a variable"""
        if var == "_debug":
            return "off"
        return None

    def variable_value(self, var, default="_NOT GIVEN_"):
        """Return a variable value, or it's default value"""
        if default == "_NOT_GIVEN_":
            default = self.vdefault(var)
        v = self.variables.get(var, default)
        return v

    def v(self, var, default="_NOT GIVEN_"):
        return self.variable_value(var,default)

    def vtrue(self, var, default="_NOT_GIVEN_"):
        return vtrue(self.variable_value(var, default))

    def reset_state(self):
        self.variables.clear()
        self.music_assets.reset_state()
        self.stop_music()
        self.clear_stack()

    def draw_loading_screen(assets):
        from core.engine import scriptfactory
        bottomscript = scriptfactory.create_menu_bottomscript()
        assets.set_stack([bottomscript])
        assets.get_stack_top().update()
        assets.get_stack_top().draw(assets.screen_data.screen)
        assets.draw_screen(False)

    
        
       
    def make_start_script(assets,logo=True):
        from core.gui_elements import choose_game
        from core.engine import scriptfactory
        from core import settings
        from core import tools_menu
        
        assets.real_path = None
        assets.registry = registry.Registry(".")
        assets.set_game_folder("games")

        assets.dt = 1
        assets.variables["_allow_saveload"] = "false"
        assets.draw_loading_screen()
        bottomscript = scriptfactory.create_start_bottomscript()
        assets.set_stack([bottomscript])  #So that the root object gets tagged as in bottomscript
        
        def games_func(*args):
            [x.delete() for x in bottomscript.objects if isinstance(x,choose_game)]
            cg = choose_game(assets)
            cg.list_games("games")
            cg.close_button()
            bottomscript.objects.append(cg)
        def examples_func(*args):
            gamedir = "examples"#!this won't work and needs fixed!!!
            directory = "./examples/"+gamedir+"/data"
            assets.start_game(gamedir)
        def settings_func(*args):
            [x.close() for x in assets.get_stack_top().objects if isinstance(x,settings.settings_menu)]
            assets.get_stack_top().objects.append(settings.settings_menu(sw=assets.sw,sh=assets.sh,assets=assets))
        def tools_func(*args):
            [x.close() for x in assets.get_stack_top().objects if isinstance(x,tools_menu.tools_menu)]
            assets.get_stack_top().objects.append(tools_menu.tools_menu(sw=assets.sw,sh=assets.sh,assets=assets))

        item = make_button("GAMES", games_func, [190,30], 999, -1001)
        bottomscript.objects.append(item)
        item = make_button("EXAMPLES", examples_func, [190,50], 999, -1001)
        bottomscript.objects.append(item)
        item = make_button("SETTINGS", settings_func, [190,70], 999, -1001)
        bottomscript.objects.append(item)
        # All tools are deprecated
        item = make_button("TOOLS", tools_func, [190,90], 999, -1001)
        if os.path.isdir("tools"):
            bottomscript.objects.append(item)

    def quit_game(self):
        self.reset_state()
        self.make_start_script(False)

    def reset_game(self):
        game = self.game_folder
        self.reset_state()
        self.start_game(game)

    def start_game(self, game, script=None, mode="casemenu"):
        self.show_load()
        gamename = game
        if "/" in game:
            gamename = game.rsplit("/", 1)[1]
        if not script:
            if os.path.exists(game+"/"+gamename+".txt"):
                script = gamename
            else:
                script = "intro"
        game = os.path.normpath(game).replace("\\", "/")
        self.last_autosave = time.time()
        self.clear()
        self.set_game_folder(game)
        self.registry = self.combine_registries()  # games/ #"./"

        from .engine import script as scr
        _script = scr.Script()
        self.append_to_stack(_script)
        if mode == "casemenu" and not os.path.exists(game+"/"+script+".txt"):
            _script.objects = [bg(self, "main"), bg(self, "main"), case_menu(self,game)]
            _script.objects[1].pos = [0, 192]
        else:
            _script.init_from_scene(script)
            from core import macro
            _script.execute_macro(macro.INIT_DEFAULTS)
            _script.execute_macro(macro.FONT_DEFAULTS)
            _script.execute_macro(macro.LOAD_DEFAULTS)
            _script.execute_macro(macro.CR_SETTINGS)


    def addevmenu(self):
        try:
            em = evidence_menu(self, self.items)
            self.get_stack_top().add_object(em, True)
            em.update()
        except art_error as e:
            self.get_stack_top().objects.append(
                error_msg(self, e.value, "", 0, self.get_stack_top()))
            import traceback
            traceback.print_exc()
            return
        return em

    def addscene(self, scene):
        from .engine import script as scr
        s = scr.Script()
        s.init_from_scene(scene)
        self.append_to_stack(s)

    def make_screen(self):
        self.screen_data.make_screen(self)
    
    def draw_screen(self, showfps):
        from core.engine import screen
        screen.draw_screen(self, showfps)

    def _load_game_menu(self, gui, ws_button, choose_game):
        if [1 for o in self.get_stack_top().objects if isinstance(o,choose_game)]:
            return
        root = choose_game(self)
        root.pri = -1000
        root.z = 5000
        root.width,root.height = [self.sw,self.sh]
        list = gui.scrollpane([0,0])
        list.width,list.height = [self.sw,self.sh]
        root.add_child(list)
        title = gui.editbox(None,"Choose save to load")
        title.draw_back = False
        list.add_child(title)
        list.add_child(ws_button(self.screen_data.screen_mode, root,"cancel",pos=[200,0]))
        cb = list.children[-1]
        def cancel(*args):
            root.delete()
        setattr(root,"cancel",cancel)
        cb.bgcolor = [0, 0, 0]
        cb.textcolor = [255,255,255]
        cb.highlightcolor = [50,75,50]
        self.get_stack_top().objects.append(root)
        saves = _get_saves(self.game_folder)
        saves.sort(key=lambda a: -a[1])
        i = len(saves)
        for s in saves:
            lt = time.localtime(s[1])
            fn = s[0].rsplit("/",1)[1].split(".",1)[0]
            t = str(i)+") "+fn+" %s/%s/%s %s:%s"%(lt.tm_mon,lt.tm_mday,lt.tm_year,lt.tm_hour,lt.tm_min)
            i -= 1
            item = ws_button(self.screen_data.screen_mode,root,t)
            list.add_child(item)
            fullpath=s[0]
            def do_load(fullpath=fullpath):
                root.delete()
                self.clear()
                self.show_load()
                loadsave.load_game_from_string(self, open(fullpath).read())
            setattr(root,t.replace(" ","_"),do_load)

    def load_game_menu(self):
        return self._load_game_menu(gui, ws_button, choose_game)

    def choose_game_menu(self):
        return choose_game(self)

    def press_enter(assets):
        if "enter" not in assets.get_stack_top().held: 
            assets.get_stack_top().held.append("enter")
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"enter_down") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.enter_down()
                if isinstance(o,evidence_menu):
                    #print (dir(evidence_menu))
                    if "enter" in assets.get_stack_top().held:
                        assets.get_stack_top().held.remove("enter")
                if isinstance(o,examine_menu):
                    if "enter" in assets.get_stack_top().held:
                        assets.get_stack_top().held.remove("enter")
                break

    def append_script_code(assets):
        from .gui import script_code
        if assets.debug_mode:
            assets.get_stack_top().objects.append(script_code(assets.get_stack_top()))

    def screen_format(self):
        from core.engine import screen
        try:
            dim = screen.get_screen_dim(
                self.screen_data.total_width, self.screen_data.total_height, 
                self.screen_data.sw, self.screen_data.sh, 
                self.screen_data.cur_screen, 
                self.screen_data.screen_mode,
                self.get_stack_top())
        except:
            dim = screen.get_screen_dim(
                self.screen_data.total_width, self.screen_data.total_height, 
                self.screen_data.sw, self.screen_data.sh, 
                self.screen_data.cur_screen, 
                self.screen_data.screen_mode)
        return dim

    def set_stack(self, new_stack):
        warnings.warn("Avoid using this")
        self.stack = new_stack

    def clear_stack(self):
        self.stack.clear()

    def delete_stack_top(self):
        del self.stack[-1]

    def stack_empty(self):
        return len(self.stack) == 0

    def get_stack(self):
        warnings.warn("Avoid using this")
        return self.stack

    def append_to_stack(self, script):
        self.stack.append(script)

    def remove_from_stack(self, script):
        self.stack.remove(script)

def _get_saves(game_folder):
    saves = []
    for p in os.listdir(game_folder+"/"):
        if not p.endswith(".ns"):
            continue
        fp = game_folder+"/"+p
        if os.path.exists(fp):
            saves.append((fp,os.path.getmtime(fp)))
    if os.path.isdir(game_folder+"/save_backup"):
        for f in os.listdir(game_folder+"/save_backup"):
            p = f
            fp = game_folder+"/save_backup/"+p
            saves.append((fp,float(fp.rsplit("_",1)[1])))
    return saves

    