import configparser as cp
from core.constants import *
def getopts(path):
    parser = cp.ConfigParser(delimiters=(' '))
    parser.read_file(open(path, "r"))
    return parser

def getlayers(parser):
    zlayers = {}
    for level in parser["z"]:
        layernames = parser["z"][level].split()
        for ln in layernames:
            zlayers[ln] = int(level)

    ulayers = {}
    for level in parser["u"]:
        layernames = parser["u"][level].split()
        for ln in layernames:
            ulayers[ln] = int(level)

    return (zlayers, ulayers)

class Sorting:
    def __init__(self):
        self.zlayers, self.ulayers = getlayers(getopts(SORTING))

    def get_z_layer(self,classname):
        return self.zlayers.get(classname, None)

    def get_u_layer(self,classname):
        return self.ulayers.get(classname, None)

sorting = Sorting()
    

