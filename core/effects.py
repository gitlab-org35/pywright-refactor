from .new_sorting import sorting
from core.vtrue import vtrue
import random
import pygame
from . import constants
from .errors import *

class effect(object):
    id_name = "_effect_"

    def __init__(self,assets):
        self.z = sorting.get_z_layer(self.__class__.__name__)
        self.assets = assets

    def delete(self):
        self.kill = 1

class scroll(effect):
    def __init__(self, assets, amtx=1, amty=1, amtz=1, speed=1, wait=1, filter="top", ramp=-.005, a_mesh = None):
        super(scroll, self).__init__(assets)
        self.a_mesh = a_mesh
        self.aamtx, self.aamty, self.aamtz = amtx, amty, amtz
        self.amtx = abs(amtx)
        self.amty = abs(amty)
        self.amtz = abs(amtz)
        self.ramp = ramp
        self.dx = self.dy = self.dz = 0
        if amtx == 0 and amty:
            self.dy = amty/abs(amty)
        elif amty == 0 and amtx:
            self.dx = amtx/abs(amtx)
        elif amty == amtx == 0:
            pass
        else:
            slope = amty/float(amtx)
            if abs(amtx) > abs(amty):
                self.dx = amtx/abs(amtx)
                self.dy = amty/float(abs(amtx))
            elif abs(amty) > abs(amtx):
                self.dx = amtx/float(abs(amty))
                self.dy = amty/abs(amty)
            else:
                self.dx = amtx/abs(amtx)
                self.dy = amty/abs(amty)
        if amtz:
            self.dz = (amtz)/abs(amtz)*speed
        self.dx *= speed
        self.dy *= speed
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.speed = speed  # !!!... so the speed is kept between runs
        # TODO:this is VERY bad and needs fixed
        self.objects = self.assets.get_stack_top().objects
        self.filter = filter
        self.wait = wait
        self.initialized = False
        if not self.amtx and not self.amty and not self.amtz:
            self.wait = False

    def draw(self, dest): pass

    def init_scroll(self, a_mesh):
        if self.initialized:
            return
        self.mesh = a_mesh
        self.initialized = True
        self.stuff = []
        for o in self.objects:
            if not hasattr(o, "pos"):
                continue
            if self.filter == "top" and o.pos[1] >= constants.STANDARD_SH:
                continue
            if self.filter == "bottom" and o.pos[1] < constants.STANDARD_SH:
                continue
            d = {"ob": o, "start": None, "end": None}
            if hasattr(o, "pos"):
                d["start"] = o.pos[:]
                d["end"] = [int(o.pos[0]+self.aamtx), int(o.pos[1]+self.aamty)]
            self.stuff.append(d)

    def update(self):
        self.init_scroll(self.a_mesh)  # !!why is this called on every update
        ndx, ndy, ndz = self.dx*self.assets.dt, self.dy*self.assets.dt, self.dz*self.assets.dt
        there_yet = False

        for d in self.stuff:
            if d["end"]:
                if d["ob"].pos[:] == d["end"]:
                    there_yet = True
                    return False
        # may need adjusted if object w/ multiple parts is moving and stuff

        if there_yet == False:
            self.amtx -= abs(ndx)
            # print "after self.amtx:",self.amtx
            if self.amtx < 0:
                ndx += self.amtx*(self.dx/abs(self.dx))
                #print("self.amtx<0 ndx:",ndx)
                self.amtx = 0
            self.amty -= abs(ndy)
            if self.amty < 0:
                ndy += self.amty*(self.dy/abs(self.dy))
                self.amty = 0
            self.amtz -= abs(ndz)
            if self.amtz < 0:
                ndz += self.amtz*(self.dz/abs(self.dz))
                self.amtz = 0
            for d in self.stuff:
                if getattr(d["ob"], "kill", 0):
                    continue
                # print(d["ob"].pos[:])

                do_it_x = True
                do_it_y = True
                if self.speed > 25:
                    if d["end"]:
                        if d["ob"].pos[:][0] + ndx >= d["end"][0]:
                            d["ob"].pos[:][0] = d["end"][0]
                            do_it_x = False
                        if d["ob"].pos[:][1] + ndx >= d["end"][1]:
                            d["ob"].pos[:][1] = d["end"][1]
                            do_it_y = False
                            #print("don't DO IT")

                if d["start"]:
                    if do_it_x == True:
                        d["ob"].pos[0] += ndx
                    if do_it_y == True:
                        d["ob"].pos[1] += ndy
                
                from core.assets import mesh
                if isinstance(d["ob"], mesh):
                    if do_it_x == True and do_it_y == True:
                        d["ob"].trans(z=ndz)

        if (self.amtx <= 0 and self.amty <= 0 and self.amtz <= 0):  # or there_yet == True
            for d in self.stuff:
                if d["end"]:
                    d["ob"].pos[:] = d["end"]
            self.delete()
            return False
        if self.wait:
            return True

    def control_last(self):
        for o in reversed(self.assets.get_stack_top().objects):
            if hasattr(o, "pos") and not getattr(o, "kill", 0):
                self.objects = [o]
                return
        if self.assets.debug_mode:
            raise missing_object("Scroll: no objects found to scroll")

    def control(self, name):
        self.filter = None
        for o in reversed(self.assets.get_stack_top().objects):
            if getattr(o, "id_name", None) == name:
                self.objects = [o]
                return
        if self.assets.debug_mode:
            raise missing_object("Scroll: no object named "+str(name)+" found")

class zoomanim(effect):
    def __init__(self,assets, mag=1, frames=1, wait=1, name=None):
        super(zoomanim, self).__init__(assets)
        self.mag = mag
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.frames = frames
        self.objects = self.assets.get_stack_top().objects
        self.mag_per_frame = float(self.mag)/float(self.frames)
        self.wait = wait
        self.kill = 0

    def draw(self, dest): pass

    def update(self):
        if self.kill:
            return False
        self.frames -= self.assets.dt
        if self.frames <= 0:
            self.delete()
        for o in self.objects:
            if getattr(o, "kill", 0):
                continue
            if hasattr(o, "dim"):
                o.dim += self.mag_per_frame*self.assets.dt
        if self.wait:
            return True

    def control_last(self):
        for o in reversed(self.assets.get_stack_top().objects):
            if hasattr(o, "pos") and not getattr(o, "kill", 0):
                self.objects = [o]
                return
        if self.assets.debug_mode:
            raise missing_object("zoom: no objects found to zoom")

    def control(self, name):
        self.filter = None
        for o in reversed(self.assets.get_stack_top().objects):
            if getattr(o, "id_name", None) == name:
                self.objects = [o]
                return
        if self.assets.debug_mode:
            raise missing_object("zoom: no object named "+str(name)+" found")

class rotateanim(effect):
    def __init__(self, assets, axis="z", degrees=90, speed=1, wait=1, name=None, obs=[]):
        super(rotateanim, self).__init__(assets)
        self.axis = {"x": 0, "y": 1, "z": 2, 0: 0, 1: 1, 2: 2}[axis]
        self.degrees = degrees
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.speed = speed
        self.objects = obs
        self.wait = wait
        self.kill = 0
        if name:
            self.objects = [o for o in self.objects if getattr(
                o, "id_name", None) == name]
            if not self.objects and self.assets.debug_mode:
                raise missing_object(
                    "rotate: no object named "+str(name)+" found")

    def draw(self, dest): pass

    def update(self):
        if self.kill:
            return False
        amt = self.speed*self.assets.dt
        if self.degrees > 0:
            self.degrees -= amt
            if self.degrees <= 0:
                self.delete()
                amt += self.degrees
            amt = -amt
        elif self.degrees < 0:
            self.degrees += amt
            if self.degrees >= 0:
                self.delete()
                amt -= self.degrees
        else:
            self.delete()
        for o in self.objects:
            if getattr(o, "kill", 0):
                continue
            if hasattr(o, "rot"):
                o.rot[self.axis] += amt
            if hasattr(o, "rotate"):
                o.rotate(self.axis, amt)
        if self.wait:
            return True

class fadeanim(effect):
    def __init__(self, assets, start=0, end=100, speed=1, wait=1, name=None, obs=[]):
        super(fadeanim, self).__init__(assets)
        self.start = start
        self.end = end
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.speed = speed
        self.objects = obs
        self.wait = wait
        self.kill = 0
        if name:
            self.objects = [o for o in self.objects if getattr(
                o, "id_name", None) == name]
            if not self.objects and self.assets.debug_mode:
                raise missing_object(
                    "fade: no object named "+str(name)+" found")
        self.update()

    def draw(self, dest): pass

    def update(self):
        if self.kill:
            return False
        amt = self.speed*self.assets.dt
        if self.start < self.end:
            self.start += amt
            if self.start > self.end:
                amt -= (self.start-self.end)
                self.delete()
        elif self.start > self.end:
            self.start -= amt
            if self.start < self.end:
                amt -= (self.end-self.start)
                self.delete()
        else:
            self.delete()
        for o in self.objects:
            if getattr(o, "kill", 0):
                continue
            if hasattr(o, "setfade"):
                o.setfade(int((self.start/100.0)*255.0))
        if self.wait:
            return True

class tintanim(effect):
    def __init__(self, assets,start="ffffff", end="000000", speed=1, wait=1, name=None, obs=[]):
        from core.gui_elements import color_str
        super(tintanim, self).__init__(assets)
        self.start = color_str(assets.variables, start)
        self.end = color_str(assets.variables, end)
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.speed = speed
        self.objects = obs
        self.wait = wait
        self.kill = 0
        if name:
            self.objects = [o for o in self.objects if getattr(
                o, "id_name", None) == name]
            if not self.objects and self.assets.debug_mode:
                raise missing_object(
                    "tint: no object named "+str(name)+" found")
        self.update()

    def draw(self, dest): pass

    def update(self):
        if self.kill:
            return False
        amt = self.speed*self.assets.dt
        col = self.start
        done = 0
        for r in range(3):
            if col[r] < self.end[r]:
                col[r] += amt
                if col[r] > self.end[r]:
                    amt -= (col[r]-self.end[r])
                    done += 1
            elif col[r] > self.end[r]:
                col[r] -= amt
                if col[r] < self.end[r]:
                    amt -= (self.end[r]-col[r])
                    done += 1
            else:
                done += 1
        if done == 3:
            self.delete()
        for o in self.objects:
            if getattr(o, "kill", 0):
                continue
            if hasattr(o, "setfade"):
                o.tint = [x/255.0 for x in col]
        if self.wait:
            return True

class invertanim(effect):
    def __init__(self, assets, start=0, end=1, speed=1, wait=0, name=None, obs=[], **kw):
        super(invertanim, self).__init__(assets)
        self.start = start
        self.end = end
        self.pri = sorting.get_u_layer("fadeanim")
        self.speed = speed
        self.objects = obs
        self.wait = wait
        self.kill = 0
        if name:
            self.objects = [o for o in self.objects if getattr(
                o, "id_name", None) == name]
            if not self.objects and self.assets.debug_mode:
                raise missing_object(
                    "invert: no object named "+str(name)+" found")
        self.update()

    def draw(self, dest): pass

    def update(self):
        if self.kill:
            return False
        amt = self.speed
        if self.start < self.end:
            self.start += amt
            if self.start > self.end:
                amt -= (self.start-self.end)
                self.delete()
        elif self.start > self.end:
            self.start -= amt
            if self.start < self.end:
                amt -= (self.end-self.start)
                self.delete()
        else:
            self.delete()
        for o in self.objects:
            if getattr(o, "kill", 0):
                continue
            if hasattr(o, "setfade"):
                o.invert = self.start
        if self.wait:
            return True

class greyscaleanim(effect):
    def __init__(self, assets, start=0, end=1, speed=1, wait=0, name=None, obs=[], **kw):
        super(greyscaleanim, self).__init__(assets)
        self.start = start
        self.end = end
        self.pri = sorting.get_u_layer("fadeanim")
        self.speed = speed
        self.objects = obs
        self.wait = wait
        self.kill = 0
        if name:
            self.objects = [o for o in self.objects if getattr(
                o, "id_name", None) == name]
            if not self.objects and self.assets.debug_mode:
                raise missing_object(
                    "greyscale: no object named "+str(name)+" found")
        self.update()

    def draw(self, dest): pass

    def update(self):
        if self.kill:
            return False
        amt = self.speed
        if self.start < self.end:
            self.start += amt
            if self.start > self.end:
                amt -= (self.start-self.end)
                self.delete()
        elif self.start > self.end:
            self.start -= amt
            if self.start < self.end:
                amt -= (self.end-self.start)
                self.delete()
        else:
            self.delete()
        for o in self.objects:
            if getattr(o, "kill", 0):
                continue
            if hasattr(o, "setfade"):
                o.greyscale = self.start
        if self.wait:
            return True

class flash(effect):
    def __init__(self, assets):
        from core.engine import screen
        super(flash, self).__init__(assets)
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.ttl = 5
        self.color = [255, 255, 255]
        self.surf = pygame.Surface(assets.screen_data.screen.get_size())
        if vtrue(self.assets.variables.get("_flash_sound", "false")):
            self.assets.play_sound("Slash.ogg")

    def draw(self, dest):
        self.surf.fill(self.color)
        dest.blit(self.surf, [0, 0])

    def update(self):
        self.ttl -= self.assets.dt
        if self.ttl <= 0:
            self.delete()
        return True

class shake(effect):
    def __init__(self, assets, screen_setting="top"):
        super(shake, self).__init__(assets)
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.ttl = 15
        self.offset = 15
        if vtrue(self.assets.variables.get("_shake_sound", "false")):
            self.assets.play_sound("Shock.ogg")
        self.wait = True
        self.screen_setting = screen_setting

    def draw(self, dest):
        o = int(self.offset)
        if self.screen_setting == "top":
            dest.subsurface([[0, 0], [constants.STANDARD_SW, constants.STANDARD_SH]]).blit(dest.subsurface(
                [[0, 0], [constants.STANDARD_SW, constants.STANDARD_SH]]).copy(), [random.randint(-o, o), random.randint(-o, o)])
        elif self.screen_setting == "both":
            dest.blit(dest.copy(), [
                      random.randint(-o, o), random.randint(-o, o)])

    def update(self):
        self.offset -= abs(self.offset / self.ttl)
        if self.offset < 1:
            self.offset = 1
        self.ttl -= self.assets.dt
        if self.ttl <= 0:
            self.delete()
        return self.wait
