import pygame,sys,os
from .engine.screen import PyGameScreen
from .screen_format import *
from . import gui
from core.constants import *
#TODO: hard to read.
#write_init_file means write_ini_file
def write_init_file(assets):
    """creates the settings.ini file with assets' default settings."""
    f = open(SETTINGS_FILE,"w")
    f.write(""";standard width is 256
;standard height is 192
width=%s
height=%s
scale2x=%s
smoothscale=%s
fullscreen=%s
show_fps=%s
framerate=%s
sound_format=%s
sound_bits=%s
sound_buffer=%s
sound_volume=%s
music_volume=%s
mute_sound=%s
screen_refresh=%s
autosave=%s
autosave_interval=%s
autosave_keep=%s
tool_path=%s,
screen_mode=%s"""%(
    assets.screen_data.total_width,
    assets.screen_data.total_height,
    assets.filter,
    assets.screen_data.smoothscale,
    assets.screen_data.fullscreen,
    int(assets.screen_data.show_fps),
    float(assets.framerate),
    assets.music_assets.sound_format,
    assets.music_assets.sound_bits,
    assets.music_assets.sound_buffer,
    int(assets.music_assets.sound_volume),
    int(assets.music_assets.music_volume),
    int(assets.music_assets.mute_sound),
    int(assets.screen_data.screen_refresh),
    int(assets.autosave),
    int(assets.autosave_interval),
    int(assets.autosave_keep),
    assets.tool_path,assets.screen_data.screen_mode.mode))
    f.close()
    
def load(assets):
    assets.screen_data = PyGameScreen()
    assets.screen_data.total_width,assets.screen_data.total_height = pygame.display.list_modes()[0]
    assets.filter = 0
    assets.autosave = 1
    assets.autosave_interval = 5 #minutes between autosaves
    assets.autosave_keep = 2 #how many saves to keep

    if os.path.exists(SETTINGS_FILE):
        f = open(SETTINGS_FILE,"r")
        lines = f.readlines()
        f.close()
        set_assets_attributes(assets, lines)

def set_assets_attributes(assets, lines):
    screen_values = {
        "screen_compress":"screen_compress",
        "screen_mode":"screen_mode",
        "screens":"num_screens",
        "width":"total_width",
        "height":"total_height",
        "smoothscale":"smoothscale",
        "screen_refresh":"screen_refresh",
        "framerate":"framerate"
    }
    music_values = {
        "sound_format":"sound_format",
        "sound_bits":"sound_bits",
        "sound_buffer":"sound_buffer",
        "mute_sound":"mute_sound",
        "sound_volume":"sound_volume",
        "music_volume":"music_volume",
    }
    i_fl_val = {
            "scale2x":"filter",
            "fullscreen":"fullscreen",
            "autosave":"autosave",
            "autosave_keep":"autosave_keep",
            "show_fps":"show_fps",}
    s_val = {
            "tool_path":"tool_path",
            }

    """TODO: REFLECTION"""
    for line in lines:
        spl = line.split("=")
        if len(spl)!=2: continue
        elif spl[0] in i_fl_val:
            setattr(assets,i_fl_val[spl[0]],int(float(spl[1])))
        elif spl[0] in s_val:
            setattr(assets,s_val[spl[0]],spl[1].strip())
        elif(spl[0] == "show_fps"):
            assets.show_fps = float(spl[1])
        elif spl[0] in screen_values:
            if(spl[0] == "screen_mode"):
                assets.screen_data.screen_mode = ScreenFormat.from_name(spl[1])
            else:
                setattr(assets.screen_data,screen_values[spl[0]],float(spl[1]))
        

class settings_menu(gui.pane):
    firstpane = "display"
    def __init__(self,*args,**kwargs):
        self.sw=kwargs["sw"]
        self.sh=kwargs["sh"]
        self.assets=assets=kwargs["assets"]
        self.assets.pause_sound()
        gui.widget.__init__(self)
        self.width = 1000
        self.height = 1000
        self.pri = -1001
        self.z = 11001
        self.align = False
        if self.firstpane == "debug" and not assets.vtrue("_debug"):
            settings_menu.firstpane = "display"
        self.reses = []
            
        self.total_height = assets.screen_data.total_height
        self.total_width = assets.screen_data.total_width
        getattr(self,self.firstpane)()
    def make_button(self,text,pos):
        b = gui.button(self,text,pos)
        if settings_menu.firstpane == text:
            b.bgcolor = [50,50,50]
            b.highlightcolor = [50,50,50]
            b.textcolor = [255,255,255]
            print("changed settings for",text)
        self.children.append(b)
        return b
    def base(self):
        assets = self.assets
        sw,sh = self.sw,self.sh
        sh = assets.sh*assets.screen_mode.num_screens
        self.children[:] = []
        self.make_button("close",[225,0])
        self.make_button("quit game",[0,sh-17])
        self.make_button("reset game",[74,sh-17])
        self.make_button("quit pywright",[sw-82,sh-17])
        self.make_button("saves",[0,0])
        self.make_button("display",[35,0])
        self.make_button("sound",[94,0])
        self.make_button("debug",[132,0])
    def debug(self):
        assets = self.assets
        sw,sh = self.sw,self.sh
        settings_menu.firstpane = "debug"
        self.base()

        #Create debug mode option if we aren't running a game
        if assets.game_path == "games":
            line = gui.pane([0,90],[sw,20])
            line.align = "horiz"
            self.children.append(line)
            line.children.append(gui.label("Debug mode?"))
            class myb(gui.checkbox):
                def click_down_over(self,*args):
                    super(myb,self).click_down_over(*args)
                    if self.checked:
                        assets.debug_mode = True
                    else:
                        assets.debug_mode = False
            line.children.append(myb("debug_mode"))
            cb = line.children[-1]
            if assets.debug_mode: cb.checked = True
            return
        line = gui.pane([0,110],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        class newr(gui.radiobutton):
            def click_down_over(s,*args):
                gui.radiobutton.click_down_over(s,*args)
                assets.playing_speed = int(s.text)
        line.children.append(gui.label("Game speed"))
        line.children.append(newr("1","gamespeed"))
        line.children.append(newr("2","gamespeed"))
        line.children.append(newr("6","gamespeed"))
        for t in line.children:
            if t.text==str(assets.playing_speed):
                t.checked = True
    def saves(self):
        assets = self.assets
        sw,sh = self.sw,self.sh
        settings_menu.firstpane = "saves"
        self.base()
        line = gui.pane([0,30],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        line.children.append(gui.label("Autosave?"))
        
        def set_autosave(val):
            assets.autosave = 1 if val else 0
            write_init_file(assets)
        button = gui.checkbox("autosave")
        button.add_listener(set_autosave)
        if assets.autosave: button.checked = True
        line.children.append(button)
            
        line = gui.pane([0,50],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        line.children.append(gui.label("Minutes between autosave"))
        class mymin(gui.editbox):
            def insert(self,val):
                if val not in "0123456789":
                    return
                super(mymin,self).insert(val)
            def set(self,val):
                super(mymin,self).set(val)
                if not val:
                    val = 0
                assets.autosave_interval = int(val)
                write_init_file(assets)
        self.autosave_interval = str(assets.autosave_interval)
        line.children.append(mymin(self,"autosave_interval"))
            
        line = gui.pane([0,70],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        line.children.append(gui.label("Autosave backups"))
        class mye(gui.editbox):
            def insert(self,val):
                if val not in "0123456789":
                    return
                super(mye,self).insert(val)
            def set(self,val):
                super(mye,self).set(val)
                if not val:
                    val = 0
                assets.autosave_keep = int(val)
                write_init_file(assets)
        self.autosave_keep = str(assets.autosave_keep)
        line.children.append(mye(self,"autosave_keep"))
        
        line = gui.pane([0,90],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        if not assets.variables.get("_allow_saveload","true"):
            line.children.append(gui.label("Save/Load currently disabled by game"))
        else:
            line.children.append(gui.label("Save/Load"))
            line.children.append(gui.button(self,"save_game"))
            line.children.append(gui.button(self,"load_game"))
    def load_game(self):
        self.assets.load_game_menu()
        self.delete()
    def save_game(self):
        self.assets.save_game()
        self.delete()
    def sound(self):
        assets = self.assets
        sw,sh = self.sw,self.sh
        settings_menu.firstpane = "sound"
        self.base()
        ermsg = gui.label("")
        ermsg.rpos = [0,140]
        ermsg.textcol = [255,0,0]
        
        line = gui.pane([0,130],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        class myb(gui.checkbox):
            def click_down_over(self,*args):
                super(myb,self).click_down_over(*args)
                if self.checked:
                    assets.set_mute_sound(True)
                else:
                    assets.set_mute_sound(False)
        line.children.append(myb("mute sound"))
        if assets.music_assets.mute_sound:
            line.children[-1].checked = True
        
        line = gui.pane([0,30],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        class newr(gui.radiobutton):
            def click_down_over(s,*args):
                ermsg.text = ""
                gui.radiobutton.click_down_over(s,*args)
                assets.sound_format = int(s.text)
                if not assets.init_sound(True): 
                    ermsg.text = "Sound not initialized"
                else:
                    assets.play_sound("phoenix/objection.ogg")
                    write_init_file(assets)
        line.children.append(gui.label("Format:"))
        line.children.append(newr("11025","formchoice"))
        line.children.append(newr("22050","formchoice"))
        line.children.append(newr("44100","formchoice"))
        for t in line.children:
            if t.text==str(assets.music_assets.sound_format):
                t.checked = True
        
        line = gui.pane([0,50],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        class newr(gui.radiobutton):
            def click_down_over(s,*args):
                ermsg.text = ""
                gui.radiobutton.click_down_over(s,*args)
                assets.music_assets.sound_bits = int(s.text)
                if not assets.init_sound(True): 
                    ermsg.text = "Sound not initialized"
                else:
                    assets.play_sound("phoenix/objection.ogg")
                    write_init_file(assets)
        line.children.append(gui.label("Bits:"))
        line.children.append(newr("8","bitschoice"))
        line.children.append(newr("16","bitschoice"))
        for t in line.children:
            if t.text==str(assets.music_assets.sound_bits):
                t.checked = True
                
        line = gui.pane([0,70],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        class newr(gui.radiobutton):
            def click_down_over(s,*args):
                ermsg.text = ""
                gui.radiobutton.click_down_over(s,*args)
                assets.sound_buffer = int(s.text)
                if not assets.init_sound(True): 
                    ermsg.text = "Sound not initialized"
                else:
                    assets.play_sound("phoenix/objection.ogg")
                    write_init_file(assets)
        line.children.append(gui.label("Buffer:"))
        line.children.append(newr("512","bufchoice"))
        line.children.append(newr("1024","bufchoice"))
        line.children.append(newr("2048","bufchoice"))
        line.children.append(newr("4096","bufchoice"))
        for t in line.children:
            if t.text==str(assets.music_assets.sound_buffer):
                t.checked = True
                
        line = gui.pane([0,90],[sw,20])
        line.align = "horiz"
        self.children.append(line)

        self.snd_line = gui.label("SoundVolume: %d"%assets.music_assets.sound_volume)
        def mod(amt,min,max,var,play):
            def modit():
                ermsg.text = ""
                assets.init_sound()
                
                #calculate new volume
                n = getattr(assets,var) + amt
                if n>max:
                    n = max
                if n<min:
                    n=min
                    
                setattr(assets,var,n)
                self.snd_line.text = "SoundVolume: %d"%assets.music_assets.sound_volume
                self.mv_line.text = "MusicVolume: %d"%assets.music_assets.music_volume
                play()
                write_init_file(assets)
            return modit
        line.children.append(self.snd_line)
        line.children.append(gui.button(None,"less"))
        line.children[-1].less = mod(-10,0,100,"sound_volume",lambda:assets.play_sound("phoenix/objection.ogg"))
        line.children.append(gui.button(None,"more"))
        line.children[-1].more = mod(10,0,100,"sound_volume",lambda:assets.play_sound("phoenix/objection.ogg"))
                
        line = gui.pane([0,110],[sw,20])
        line.align = "horiz"
        self.children.append(line)
        
        self.mv_line = gui.label("MusicVolume: %d"%assets.music_assets.music_volume)
        line.children.append(self.mv_line)
        line.children.append(gui.button(None,"less"))
        line.children[-1].less = mod(-10,0,100,"music_volume",lambda:assets.play_music("Ding.ogg",loop=1,pre="sfx/",reset_track=False))
        line.children.append(gui.button(None,"more"))
        line.children[-1].more = mod(10,0,100,"music_volume",lambda:assets.play_music("Ding.ogg",loop=1,pre="sfx/",reset_track=False))
        if ermsg.text != "":
            self.children.append(ermsg)
            
    def display(self):
        assets = self.assets
        settings_menu.firstpane = "display"
        self.base()
        self.screens = []
        res_box = gui.scrollpane([10,20])
        res_box.width = 200
        res_box.height = 120
        self.res_box = res_box
        self.children.append(res_box)
        
        button = gui.button(self,"Change resolution (%sx%s)"%(assets.screen_data.total_width,assets.screen_data.total_height))
        button.checked = True
        button.click_down_over = self.popup_resolution
        res_box.add_child(button)
        
        button = gui.button(self,"dualscreen mode(%s)"%(assets.screen_mode.mode))
        button.checked = True
        button.click_down_over = self.popup_screenmode
        res_box.add_child(button)
        
        button = gui.checkbox("smoothscale")
        self.smoothscale = button
        res_box.add_child(button)
        
        button = gui.checkbox("fullscreen")
        self.fs = button
        res_box.add_child(button)
        
        button = gui.checkbox("show fps")
        self.show_fps = button
        def set_fps(val):
            assets.screen_data.show_fps = val
            write_init_file(assets)
        self.show_fps.add_listener(set_fps)
        res_box.add_child(button)
        
        self.frameskip = gui.checkbox("frameskip")
        def set_frameskip(val):
            if val:
                assets.set_scref(3)
            else:
                assets.set_scref(1)
            write_init_file(assets)
        self.frameskip.add_listener(set_frameskip)
        res_box.add_child(self.frameskip)

        if assets.screen_data.fullscreen:
            self.fs.set_checked(True)
        if assets.show_fps:
            self.show_fps.set_checked(True)
        if assets.screen_data.smoothscale:
            self.smoothscale.set_checked(True)
        if assets.screen_data.screen_refresh >1:
            self.frameskip.set_checked(True)
        self.children.append(gui.button(self,"apply",[10,140]))
    
    def popup_resolution(self,mp):
        assets = self.assets
        sw,sh = self.sw,self.sh
        self.res_box.pane.children[:] = []
        #print(self.res_box)
        h = 192
        if assets.screen_data.screen_mode == TWO_SCREENS:
            h*=2
        h2 = h*2
        self.res_box.add_child(gui.radiobutton("(%sx%s)"%(assets.screen_data.total_width,assets.screen_data.total_height),"resopt"))
        self.res_box.pane.children[-1].checked = True
        self.res_box.add_child(gui.radiobutton("DS Res (256x%s)"%h,"resopt"))
        self.res_box.add_child(gui.radiobutton("Double scale (512x%s)"%h2,"resopt"))
        for mode in sorted(pygame.display.list_modes()):
            self.res_box.add_child(gui.radiobutton("(%sx%s)"%mode,"resopt"))
        self.reses = gui.radiobutton.groups["resopt"]
        for r in self.reses:
            if str(assets.screen_data.total_width)+"x" in r.text and "x"+str(assets.screen_data.total_height) in r.text:
                r.checked = True
        self.res_box.updatescroll()
    def popup_screenmode(self,mp):
        assets = self.assets
        sw,sh = self.sw,self.sh
        self.res_box.pane.children[:] = []
        for mode in SCREEN_FORMATS:
            r = gui.radiobutton("%s"%mode.mode,"scropt")
            self.res_box.add_child(r)
            def _set_checked_radio(val, text = r.text):
                assets.screen_mode = ScreenFormat.from_name(text)
                write_init_file(assets)
                assets.make_screen()
            r.add_listener(_set_checked_radio)
        self.screens = gui.radiobutton.groups["scropt"]
            
        for r in self.screens:
            if r.text==assets.screen_mode.mode:
                r.checked = True
        self.res_box.updatescroll()
    def setdl(self,v):
        self.dislis.checked = 1-self.dislis.checked
        pygame.DISPLAY_LIST = self.dislis.checked
        write_init_file(self.assets)
    def apply(self):
        assets = self.assets
        sw,sh = self.sw,self.sh
        for r in self.screens: 
            if r.checked:
                assets.screen_mode = ScreenFormat.from_name(r.text)
        for r in self.reses: 
            if r.checked:
                self.oldwidth,self.oldheight = assets.screen_data.total_width,assets.screen_data.total_height
                self.timer = 5.0
                self.really_applyb = gui.pane()
                self.really_applyb.is_applyb = True
                self.really_applyb.width = 1000
                self.really_applyb.height = 1000
                self.really_applyb.pri = -1002
                self.really_applyb.z = 11002
                #self.really_applyb.align = False
                e = gui.editbox(None,"")
                e.draw_back = False
                self.really_applyb.children.append(e)
                self.really_applyb.timer = e
                b = gui.button(self,"save resolution",[0,0])
                self.really_applyb.children.append(b)
                assets.get_stack_top().objects.append(self.really_applyb)
                assets.screen_data.total_width,assets.screen_data.total_height = [int(x) for x in (r.text[r.text.find("(")+1:r.text.find(")")]).split("x")]
        self.old_fullscreen = assets.fullscreen
        assets.fullscreen = 0
        if self.fs.checked:
            assets.fullscreen = 1
        assets.smoothscale = 0
        if self.smoothscale.checked:
            assets.smoothscale = 1
        assets.make_screen()
        self.display()
        write_init_file(assets)
    def save_resolution(self):
        assets = self.assets
        #self.sw,self.sh = sw,sh 
        for o in assets.get_stack_top().objects:
            if hasattr(o,"is_applyb"):
                assets.get_stack_top().get_world().remove(o)
        self.really_applyb = None
        self.timer = 0
        write_init_file(assets)
        self.display()
    def reset_res(self):
        assets = self.assets
        #sw,sh = self.sw,self.sh
        assets.screen_data.total_width,assets.screen_data.total_height = self.oldwidth,self.oldheight
        assets.fullscreen = self.old_fullscreen
        assets.make_screen()
        write_init_file(assets)
        self.display()
    def update(self,*args):
        assets = self.assets
        self.rpos = [0,0]
        self.pos = self.rpos
        if getattr(self,"timer",0)>0:
            self.timer -= .02
            self.really_applyb.timer.text = "Resetting view in: %.02f seconds"%self.timer
        else:
            if getattr(self,"really_applyb",None):
                assets.get_stack_top().get_world().remove(self.really_applyb)
                self.really_applyb = None
                self.reset_res()
        for x in self.children:
            x.update()
        return True
    def quit_game(self):
        self.assets.quit_game()
    def reset_game(self):
        self.assets.reset_game()
    def quit_pywright(self):
        sys.exit()
    def close(self):
        self.delete()
    def k_space(self):
        self.close()
    def delete(self):
        self.kill = 1
        self.assets.resume_sound()
