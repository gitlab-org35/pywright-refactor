#No this has nothing to do with pwlib
#It's a way to get version information from archives and folders
import os
from core.constants import *
def cver(verstr):
    """Converts a version string into a number"""
    if verstr.startswith("b"):
        return float(verstr[1:])-100000
    return float(verstr)
    
def cver_t(verstr):
    """Converts a version string into a tuple"""
    if verstr.startswith("b"):
        return tuple([0,0,0,0]+list(cver_t(verstr[1:])))
    return tuple([int(x) for x in verstr.split(".")])
        
def cver_s(tup):
    """Convert tuple version back to string"""
    """tup = list(tup)
    while tup and not tup[-1]:
        del tup[-1]
    if not tup:
        return "0.0"
    if len(tup)==1:
        tup.append(0)
    return ".".join([str(x) for x in tup])"""
    vers = ""#str(tup[0])#+"."+str(tup[1])+"."+str(tup[2])
    count = 0
    for x in tup:
        if count == 0:
            vers = str(tup[0])
        else:
            vers = vers + "." + str(x)
        
        count += 1
    #print(vers)
    return vers
    #return ''.join(map(str, tup))

def compare_versions(v1,v2):
    v1 = list(v1)
    v2 = list(v2)
    while len(v1)<len(v2):
        v1.insert(0, 0)#.append(0)
    while len(v2)<len(v1):
        v2.insert(0, 0)#.append(0)
    def cmp(a, b):
        if a is None:
            a = 0
        if b is None:
            b = 0
        return (a > b) - (a < b) 
    #print(tuple(v1))
    #print(tuple(v2))
    return cmp(tuple(v1),tuple(v2))

def read_pwv(txt):
    d = {}
    if txt[0]=="b" or txt[0].isdigit() and "\n" not in txt:
        d[VERSION] = cver_t(txt.strip())
        return d
    for line in txt.split("\n"):
        if not line:
            continue
        key,val = line.strip().split(" ",1)
        if key in [VERSION,"min_pywright_version"]:
            val = cver_t(val)
        d[key] = val
    return d

def shortest_pwv_path(zip):
    pwvpaths = []
    for path in zip.namelist():
        if path.endswith("/"+DATA_FILE) or path==DATA_FILE:
            pwvpaths.append(path)
        elif path.endswith("/.pwv") or path==".pwv":
            pwvpaths.append(path)
    pwvpaths.sort(key=lambda o: len(o))
    return pwvpaths[0]

def extract_pwv(zip,name):
    pth = shortest_pwv_path(zip)
    txt = zip.read(pth)
    f = open(name+PWV_EXT,"w")
    f.write(txt)
    f.close()
    return txt

'''
TODO: gets called for many game folders 
'''
def get_data_from_folder(folder):
    "data.txt file is optional"
    from os.path import exists
    for vfile in [DATA_FILE,".pwv"]:
        if exists(folder+"/"+vfile):
            f = open(folder+"/"+vfile)
            txt = f.read()
            f.close()
            return read_pwv(txt)
        else:
            pass
    return {"version":(0,)}
