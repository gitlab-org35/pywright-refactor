def vtrue(variable):
    """
    True if variable.lower() is 'on', '1' or 'true'.
    Otherwise false.
    """
    if variable.lower() in ["on", "1", "true"]:
        return True
    return False