import os,zipfile,io
from . import file_system
ASSET_SUBFOLDERS = ["art", "art.zip", "sfx", "music","movies"]
IGNORE = ".hg"
FILETYPE_PRIORITY = ["png","jpg","gif","bmp","mp3","ogg"]

class File:
    def __init__(self,path):
        self.path = path
        if "/" in path: 
            self.filename = self.path.rsplit("/",1)[1]  
        else: 
            self.path
        if "." in self.filename:
            self.pathtag = self.path.rsplit(".",1)[0]
            self.filetag,self.ext = self.filename.rsplit(".",1)
        else:
            self.pathtag = self.path
            self.filetag = self.filename
            self.ext = ""
        if self.ext in FILETYPE_PRIORITY:
            self.priority = FILETYPE_PRIORITY.index(self.ext)
        else:
            self.priority = 12
        
        self.filetag = self.filetag.lower()
        self.pathtag = self.pathtag.lower()
        self.pathtagext = self.pathtag+"."+self.ext
    
    def search_in_map(self, map, use_extension=False):
        key = self.pathtagext if use_extension else self.pathtag
        return map[key].path if key in map else None

    def get_tags(self, root):
        lower_root = root.lower()+"/"
        tag = self.pathtag.split(lower_root,1)[1]
        tagext = self.pathtagext.split(lower_root,1)[1]
        return tag, tagext

    def get_tags_zip(self, root):
        tag, tagext = self.get_tags(root)
        spl = tag.split("/")
        spl[-2] = spl[-2].rsplit(".",1)[0]
        tag = "/".join(spl)
        return tag, tagext

    def __repr__(self):
        return self.path

global_registry_cache = {}
class Registry:
    def __init__(self,root=None,use_cache=True):
        self.map = {}
        self.listdir_map = {}
        self.root = None
        if root:
            self.build(root, use_cache)

    def open(self,path,mode="rb"):
        if ".zip/" in path:
            return self.open_zip(path)
        return open(path,mode)

    def open_zip(self, path):
        normal,zip = path.split(".zip/",1)
        zf = zipfile.ZipFile(normal+".zip")
        return io.StringIO(zf.open(zip,"r").read())

    def clear_cache(self):
        global_registry_cache.clear()

    def __build__(self, root, after_load_subfolder):
        for sub in ASSET_SUBFOLDERS:
            path =  root+"/"+sub
            if os.path.isdir(path) or zipfile.is_zipfile(path):
                self.index(root, path)
            after_load_subfolder()
        self.root = root

    def build(self,root,use_cache = False, after_load_subfolder=lambda:1):
        if use_cache and root in global_registry_cache:
            self.map = global_registry_cache[root]
            return

        self.__build__(root, after_load_subfolder)

        global_registry_cache[root] = self.map

    def list_files(self,path):
        if os.path.isdir(path):
            return os.listdir(path)
        if zipfile.is_zipfile(path):
            return zipfile.ZipFile(path,"r").namelist()

    def index(self,root,path):
        in_zip = zipfile.is_zipfile(path)
        subdirs = []
        self.mapfile(root, path+"/",in_zip)
        for sub in self.list_files(path):
            new_path = path+"/"+sub
            if sub==IGNORE:
                continue
            elif os.path.isdir(new_path) or zipfile.is_zipfile(new_path):
                subdirs.append(new_path)
            else:
                self.mapfile(root, new_path,in_zip)
        for sub in subdirs:
            self.index(root, sub)
    
    def _add_file_to_map(self, map, tag, file):
        if tag in map:
            if file.priority<=map[tag].priority:
                map[tag] = file
        else:
            map[tag] = file

    def mapfile(self,root, path,in_zip=False):
        '''
        Creates a File object that creates new arguments based on path
        '''
        file = File(path)
        tag, tagext = file.get_tags_zip(root) if in_zip else file.get_tags(root)
        self._add_file_to_map(self.map, tag, file)
        self._add_file_to_map(self.map, tagext, file)

    def cleanpath(self,path):
        return file_system.cleanup(os.path.normpath(path)).replace(".zip/","/")

    def lookup(self,thingie,ext=False):
        """
        Look up files in the file map we built.
        Ignores the file extension.
        """
        thingie = self.cleanpath(thingie)
        f = File(thingie)
        return f.search_in_map(self.map, use_extension=False)
    def lookup_with_extension(self, thingie):
        """
        Look up files in the file map we built.
        does not ignore file extension.
        """
        thingie = self.cleanpath(thingie)
        f = File(thingie)
        return f.search_in_map(self.map, use_extension=True)

    def merge(self,other_reg):
        self.map.update(other_reg.map)

def __parent_folders_in_path__(root):
    """
    ./games/Appeal to Truth.V3
    ->
    ['.', './games', './games/Appeal to Truth.V3']
    """
    split_root = root.split("/")
    last = ""
    order = []
    for x in split_root:
        if last:
            last+="/"+x
        else:
            last=x
        order.append(last)
    return order

def combine_registries(root,after_load_subfolder=lambda:1):
    order = __parent_folders_in_path__(root)
    cur_reg = Registry()
    for root in order:
        reg = Registry()
        reg.build(root,after_load_subfolder)
        cur_reg.merge(reg)
    return cur_reg

