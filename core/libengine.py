from .errors import script_error
from .gui_elements import *
from .keybinds import get_keybinds
from .pwvlib import *
import pygame
from . import settings

try:
    import numpy
    pygame.sndarray.use_arraytype("numpy")
    pygame.use_numpy = True
except:
    numpy = None
    pygame.use_numpy = False

data_from_folder = get_data_from_folder(".")
__version__ = data_from_folder["version"]
VERSION = "Version "+cver_s(data_from_folder["version"])
clock = pygame.time.Clock()

from .engine import script
    
# import engine.script and engine.debug_script
def set_version():
    data_from_folder = get_data_from_folder(".")
    __version__ = data_from_folder["version"]
    VERSION = "Version "+cver_s(data_from_folder["version"])

def remove_objects_with_kill_att(assets):
    for o in assets.get_stack_top().all_objects()[:]:
            if getattr(o,"kill",0):
                assets.get_stack_top().all_objects().remove(o)
    
def handle_screen_draw(assets):
    assets.top_draw_screen(script)
    
def run(assets, checkupdate=False):
    set_version()
    assets.make_screen()
    assets.clock = clock
    assets.make_start_script()
    next_screen = assets.screen_refresh
    running = True
    keybinds = get_keybinds()
    while running:
        dt = clock.tick(assets.framerate)
        assets.dt = min(dt*.001*60,10.0)*assets.playing_speed

        pygame.display.set_caption("PyWright "+VERSION)
        
        assets.get_stack_top().update()
        assets.interpreter.interpret_scripts(assets)
                #~ print [[x,x.pri] for x in assets.get_stack_top().objects]
        if assets.stack_empty(): break
        next_screen -= assets.dt
        [o.unadd() for o in assets.get_stack_top().objects if getattr(o,"kill",0) and hasattr(o,"unadd")]
        remove_objects_with_kill_att(assets)
        if next_screen < 0:
            handle_screen_draw(assets)
            next_screen = assets.screen_refresh

        pygame.event.pump()
        try:
            assets.music_update()
            assets.get_stack_top().handle_events(pygame.event.get([pygame.MOUSEMOTION,pygame.MOUSEBUTTONUP,pygame.MOUSEBUTTONDOWN]))
            # TODO: add back enter-holding functionality
            for e in pygame.event.get():
                if e.type==pygame.ACTIVEEVENT:
                    # bug regarding event with no attributes
                    if not hasattr(e, "gain"): continue
                    if e.gain==0 and (e.state==6 or e.state==2 or e.state==4):
                        print("minimize")
                        gw = guiWait(assets,mute=True)
                        gw.pri = -1000
                        gw.minimized = True
                        assets.get_stack_top().objects.append(gw)
                    if e.gain==1 and (e.state==6 or e.state==2 or e.state==4):
                        print("maximize")
                        for ob in assets.get_stack_top().objects:
                            if hasattr(ob,"minimized"):
                                ob.delete()
                if e.type==pygame.VIDEORESIZE:
                    w,h = e.w,e.h
                    assets.screen_data.total_width = w
                    assets.screen_data.total_height = h
                    assets.make_screen()
                    settings.write_init_file(assets)
                if e.type == pygame.QUIT:
                    running = False
                if e.type==pygame.KEYDOWN or e.type==pygame.KEYUP:
                    def keybind_has_key_and_flag(keybind):
                        return (keybind.code == e.key) and (e.type == keybind.type)
                    mylist = list(filter(keybind_has_key_and_flag, keybinds))
                    if(len(mylist)): 
                        bind = mylist[0]
                        bind.run(e, assets)
                assets.get_stack_top().handle_events([e])
        except (art_error,script_error,markup_error) as e:
            assets.get_stack_top().handle_error(e=e)
            import traceback
            traceback.print_exc()
    if hasattr(assets, "threads"):
        while [1 for thread in assets.threads if thread and thread.isAlive()]:
            print("waiting")
            pass
if __name__=="__main__":
    run()
