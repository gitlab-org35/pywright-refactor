import pygame
import sys
from . import settings
class Keybind():
    def __init__(self, callback, pygame_code, pygame_type):
        self.callback = callback
        self.code = pygame_code
        self.type = pygame_type
    
    def run(self, event, assets):
        self.callback(event, assets)

def get_keybinds():
    def toggle_settings(event, assets):
        ss = [x for x in assets.get_stack_top().objects if isinstance(x,settings.settings_menu)]
        if ss:
            ss[0].close()
        else:
            assets.get_stack_top().objects.append(settings.settings_menu(sw=assets.sw,sh=assets.sh,assets=assets))
    def enter_up(event, assets):
        if "enter" in assets.get_stack_top().held: assets.get_stack_top().held.remove("enter")
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"enter_up"):
                o.enter_up()
                break
    def enter_down(event, assets):
        if(pygame.key.get_mods() & pygame.KMOD_ALT):
            assets.fullscreen = 1-assets.fullscreen
            assets.make_screen()
            settings.write_init_file(assets)
        else:
            assets.press_enter()
    def k_right(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"statement") and not o.statement:
                continue
            if hasattr(o,"k_right") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.k_right()
                break
    def k_left(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"statement") and not o.statement:
                continue
            if hasattr(o,"k_left") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.k_left()
                break
    def k_up(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"k_up") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.k_up()
                break
    def k_down(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"k_down") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.k_down()
                break
    def k_cancel(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"k_space") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.k_space()
                return
        game = assets.game_folder
        assets.get_stack_top().quit()
        if game == 'games':
            sys.exit()
    def k_switch(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"k_tab") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                print("tab on",o)
                o.k_tab()
                break
    def press(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"k_z") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.k_z()
                break
    def present(event, assets):
        for o in assets.get_stack_top().up_objects:
            if hasattr(o,"k_x") and not getattr(o,"kill",0) and not getattr(o,"hidden",0):
                o.k_x()
                break
    def debug(event, assets):
        if event.mod&pygame.K_LCTRL:
            assets.debug_mode = True              
    def save_ev(event, assets):
        if assets.game_folder!="menu":
            assets.save_game()
    def load_ev(event, assets):
        if assets.game_folder!="menu":
            assets.load_game_menu()
    def script_code(event, assets):
        assets.append_script_code()
    def invert_cur(event, assets):
        assets.screen_data.cur_screen = 1 - assets.screen_data.cur_screen


    #TODO: restore joystick functions with new system
    keybinds = []
    keybinds.append(Keybind(toggle_settings, pygame.K_ESCAPE, pygame.KEYDOWN))
    keybinds.append(Keybind(enter_up, pygame.K_RETURN, pygame.KEYUP))
    keybinds.append(Keybind(enter_down, pygame.K_RETURN, pygame.KEYDOWN))
    keybinds.append(Keybind(k_right, pygame.K_RIGHT, pygame.KEYDOWN))
    keybinds.append(Keybind(k_left, pygame.K_LEFT, pygame.KEYDOWN))
    keybinds.append(Keybind(k_up, pygame.K_UP, pygame.KEYDOWN))
    keybinds.append(Keybind(k_down, pygame.K_DOWN, pygame.KEYDOWN))
    keybinds.append(Keybind(k_cancel, pygame.K_SPACE, pygame.KEYDOWN))
    keybinds.append(Keybind(press, pygame.K_z, pygame.KEYDOWN))
    keybinds.append(Keybind(present, pygame.K_x, pygame.KEYDOWN))
    keybinds.append(Keybind(debug, pygame.K_d, pygame.KEYDOWN))
    keybinds.append(Keybind(save_ev, pygame.K_F5, pygame.KEYDOWN))
    keybinds.append(Keybind(load_ev, pygame.K_F7, pygame.KEYDOWN))
    keybinds.append(Keybind(script_code, pygame.K_F3, pygame.KEYDOWN))
    keybinds.append(Keybind(invert_cur, pygame.K_F4, pygame.KEYDOWN))
    return keybinds
   