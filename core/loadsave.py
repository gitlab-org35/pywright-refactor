from .errors import *
import time
import os
import json
from .vtrue import vtrue
"""
Core saving funtionality. Save attributes of Assets class (and Music_Assets),
then save the entire inventory under key "items"
then save all items from 'variables' class under key 'variables', 
handling _speaking variable as a special case.
Return list ["Assets", [], props, None] (Why????)

"""

REGS = ["character", "_track", "_loop", "lists"]
MUSIC_REGS = ["_track", "_loop"]
def load(assets, properties):
    for prop in properties:
        if prop in MUSIC_REGS:
            setattr(assets.music_assets, prop, properties[prop])
        else:
            setattr(assets, prop, properties[prop])

    # items and variables get handled in the after_load method
        
            

def save(assets):
    assets.last_autosave = time.time()
    props = {}

    for reg in REGS:
        if hasattr(assets.music_assets, reg):
            props[reg] = getattr(assets.music_assets, reg)
        elif hasattr(assets, reg):
            props[reg] = getattr(assets, reg)
        
    # save items
    items = []
    for x in assets.items:
        items.append({"id": x.id, "page": x.page})
    props["items"] = items
    # save variables
    vars = {}
    for x in assets.variables:
        v = assets.variables[x]
        if x == "_speaking" and hasattr(v, "id_name"):
            vars[x] = v.id_name
        else:
            vars[x] = v
    props["variables"] = vars
    #print("props: ", props)
    return props

def load_game_new(assets, path=None, filename="save", hide=False):
    from .assets import saved
    if not vtrue(assets.variables.get("_allow_saveload", "true")):
        return
    if "\\" in filename or "/" in filename:
        raise script_error("Invalid save file path:'%s'" % (filename,))
    if not hide:
        assets.show_load()
    if path:
        assets.set_game_folder(path)
    try:
        f = open(assets.game_folder+"/"+filename+".ns")
    except:
        assets.get_stack_top().objects.append(
            saved(assets, text="No game to load.", ticks=240))
        return
    save_text = f.read()
    f.close()
    load_game_from_string(assets, save_text)

"""
Core functionality for loading saves.
Each file contains a list for elements, each one containing 4 parts:
A string name, an empty list, a dictionary of misc data and another list
(class, args, props, dest)

There are two save types: JSON and eval. First JSON is attempted, then eval

TODO: deprecate old save mode
"""
def convert_save_string_to_ob(text):
    def read_oldsave(s):
        return eval(s)

    def read_newsave(s):
        return json.loads(s)

    def read_save(s):
        try:
            return read_newsave(s)
        except:
            return read_oldsave(s)
    return read_save(text)

"""
Core functionality for loading saves.
Each file contains a list for elements, each one containing 4 parts:
A string name, an empty list, a dictionary of misc data and another list
(class, args, props, dest)

There are two save types: JSON and eval. First JSON is attempted, then eval

TODO: deprecate old save mode
"""

def load_game_from_string(assets, save_text):
    if not save_text: raise ValueError("no save text provided")
    from .assets import saved
    assets.loading_cache = {}
    from core.engine import script as scr
    # clear lists and fonts, clear Variables() class if it exists
    assets.clear()
    stack = {}
    loaded = []
    object_list = convert_save_string_to_ob(save_text)
    #TODO Reflection
    for class_name, args, properties, dest in object_list:
        if class_name == "Assets":
            object = assets
        elif (class_name == "Script" or class_name == "assets.Script"):
            object = scr.Script(*args)
        else:
            raise Exception("Unfamiliar class saved")
            # object = eval(class_name)(*args)

        object.load(properties)

        if dest:
            key, index = dest
            if key == "stack":
                # if the list is a ["stack", 123] pair, put the object on the stack at 123
                stack[index] = object
            else: 
                # TODO: does this ever happen??? 
                # get the script at [index] and append the object.
                # TODO: assets doesnt have a dest attribute, so all this code
                # is script specific
                stack[index].objects.append(object)
        loaded.append(object)

    keys = list(stack.keys())
    keys.sort()
    for prop in keys:
        assets.append_to_stack(stack[prop])
    for object in loaded:
        object.after_load()
    # Clear stack, keep only the last root script and its children
    d = 0
    for s in reversed(assets.get_stack()):
        if d:
            assets.remove_from_stack(s)
        if not s.parent:
            d = 1
    assets.get_stack_top().objects.append(saved(assets, text="Game restored", block=False))
    assets.get_stack_top().execute_macro("load_defaults")
    assets.get_stack_top().execute_macro("init_court_record_settings")

def check_autosave(path):
    if not os.path.exists(path+"/autosave.ns"):
        return "save"
    if not os.path.exists(path+"/save.ns"):
        return "autosave"
    mt1 = os.path.getmtime(path+"/autosave.ns")
    mt2 = os.path.getmtime(path+"/save.ns")
    if mt1 > mt2:
        return 'autosave'
    return "save"

def load_game(assets, path=None, filename="save", hide=False):
    assets.get_stack_top().imgcache.clear()
    chkpath = ""
    if path is not None:
        chkpath = path+"/"
    if filename == "save":
        filename = check_autosave(chkpath)
    load_game_new(assets, path, filename, hide)

def backup(assets, path, save):
    if not os.path.exists(path+"/"+save):
        return
    if not os.path.exists(path+"/save_backup"):
        os.mkdir(path+"/save_backup")
    f = open(path+"/"+save)
    t = f.read()
    f.close()
    f = open(path+"/save_backup/"+save+"_" +
                repr(os.path.getmtime(path+"/"+save)), "w")
    f.write(t)
    f.close()
    if save != "autosave.ns":
        return
    autosaves = []
    for f in os.listdir(path+"/save_backup"):
        if f.startswith(save):
            autosaves.append((f, float(f.split("_")[1])))
    autosaves.sort(key=lambda s: s[1])
    while len(autosaves)+1 > assets.autosave_keep:
        p, t = autosaves.pop(0)
        print("delete", p)
        os.remove(path+"/save_backup/"+p)

def save_game(assets, filename="save", hide=False):
    from .assets import saved
    if not vtrue(assets.variables.get("_allow_saveload", "true")) and not vtrue(assets.variables.get("_debug", "false")):
        return
    if "\\" in filename or "/" in filename:
        raise script_error("Invalid save file path:'%s'" % (filename,))
    filename = filename.replace("/", "_").replace("\\", "_")+".ns"
    # Collect *things* to save (here is where music is saved)
    backup(assets, assets.game_folder, filename)
    f = open(assets.game_folder+"/"+filename, "w")
    f.write(json.dumps(assets.save(), indent=4))
    f.close()
    # Send GUI notification
    if not hide:
        assets.get_stack_top().objects.append(saved(assets,block=False))
