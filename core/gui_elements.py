from .pwvlib import *
from .new_sorting import sorting
from . import textutil
from .errors import *
from . import gui
from core.vtrue import vtrue
from . import extensions
from .soft3d import context
from .engine.scriptinterpreter import subscript
from .color_str import color_str
from .sprite_metadata import Sprite_Metadata
from core.constants import *
TextRenderer = textutil.TextRenderer
class sprite(gui.button):
    """
    Base class that loads of other classes inherit from.
    Self.img is extremely important and should be more visible.
    """
    blinkspeed = [100, 200]
    autoclear = False
    pri = 0
    # widget stuff
    #FIXME: redo getters and setters, preferably remove

    def _g_rpos(self):
        if not hasattr(self, "pos"):
            return [0, 0]
        return self.getpos()
    def _set_rpos(self, value):
        self.pos = value
    rpos = property(_g_rpos, _set_rpos)
    children = []
    spd = 6
    
    def getpos(self):
        """
        Return a clone of the position.
        Code for screen handling.
        """
        pos = self.pos[:]
        if self.screen_setting == "try_bottom":
            pos[1] = trans_y(self.assets.screen_data.screen_mode.num_screens,pos[1])
        return pos

    def getprop(self, p):
        if p in "xy":
            return self.pos["xy".index(p)]
        if p == "frame":
            return self.x
        if p == "screen_setting":
            return self.screen_setting
            return getattr(self, p, "")

    def setprop(self, p, v):
        if p in "xy":
            self.pos["xy".index(p)] = float(v)
        if p in "z":
            self.z = int(v)
        if p == "frame":
            self.x = int(v)
        if p == "screen_setting":
            self.screen_setting = v

    def delete(self):
        self.kill = 1

    def click_down_over(self, mp):
        pass

    def _load_extra(self, vars, sprite_metadata):
        self.sounds = sprite_metadata.sounds
        self.loops = sprite_metadata.loops
        self.split = sprite_metadata.split
        self.blinkmode = sprite_metadata.blinkmode
        self.offsetx = sprite_metadata.offsetx
        self.offsety = sprite_metadata.offsety
        self.blipsound = sprite_metadata.blipsound
        self.delays = sprite_metadata.delays
        self.spd = sprite_metadata.speed
        self.blinkspeed = sprite_metadata.blinkspeed
        if vars.get("_blinkspeed_next", ""):
            self.blinkspeed = [
                int(x) for x in vars["_blinkspeed_next"].split(" ")]
            vars["_blinkspeed_next"] = ""
        elif vars.get("_blinkspeed_global", "default") != "default":
            self.blinkspeed = [
                int(x) for x in vars["_blinkspeed_global"].split(" ")]

    def load_string(self, name, key=[255, 0, 255]):
        assets = self.assets
        # if name is a string, use assets.meta
        path = ""
        self.base = assets.open_art(name, key)
        self._load_extra(assets.variables, assets.meta)
        self.real_path = assets.real_path
        self.width, self.height = self.base[0].get_size()
        self.img = self.base[0]
        self.name = name
        self.x = 0
        self.next = self.delays.get(0, self.spd)
        return self

    def load_base(self, sprite_list):
        '''
        Load separated sprite images into 'base'
        '''
        assets = self.assets
        # if base is a string, use assets.meta
        self._load_extra(assets.variables,Sprite_Metadata())
        self.real_path = assets.real_path
        self.width, self.height = sprite_list[0].get_size()
        self.img = sprite_list[0]
        self.x = 0
        self.next = self.delays.get(0, self.spd)
        self.base = sprite_list
        return self

    def load(self, name, key=[255, 0, 255]):
        assets = self.assets
        # if name is a string, use assets.meta
        if type(name) == str:
            return self.load_string(name, key)
        else:
            return self.load_base(name)

    def __init__(self, assets, x=0, y=0, flipx=0, **kwargs):
        self.assets = assets
        # TODO: get asset data more cleanly
        try:
            self.spd = int(assets.get_frame_delay())
        except:
            import traceback
            traceback.print_exc()
        self.loopmode = ""
        self.next = self.spd
        self.pos = [x, y]
        self.dim = 1
        self.z = sorting.get_z_layer(self.__class__.__name__)
        self.rot = [0, 0, 0]
        if kwargs.get("rotx", None):
            self.rot[0] = int(kwargs.get("rotx"))
        if kwargs.get("roty", None):
            self.rot[1] = int(kwargs.get("roty"))
        if kwargs.get("rotz", None):
            self.rot[2] = int(kwargs.get("rotz"))
        self.sounds = {}
        self.x = 0
        self.offsetx = 0
        self.offsety = 0
        self.loops = 0
        self.loopmode = 0
        self.flipx = flipx
        self.blinkmode = "blinknoset"
        from .gui_elements import other_screen
        if kwargs.get("screen", None) == 2:
            self.pos[1] = other_screen(self.pos[1], assets.screen_data)
        self.screen_setting = ""
        self.base = []
        self.delays = {}
        self.start = 0
        self.end = None

    def draw(self, dest):
        if not getattr(self, "img", None):
            return
        img = self.img
        if self.flipx:
            img = pygame.transform.flip(img, 1, 0)
        pos = self.getpos()
        if hasattr(self, "offsetx"):
            pos[0] += self.offsetx
        if hasattr(self, "offsety"):
            pos[1] += self.offsety
        if hasattr(self, "rot"):
            if hasattr(img, "ori"):
                img.ori = self.rot
            elif self.rot[2]:
                pos[0] += img.get_width()//2
                pos[1] += img.get_height()//2
                img = pygame.transform.rotate(img, self.rot[2]).convert_alpha()
                pos[0] -= img.get_width()//2
                pos[1] -= img.get_height()//2
        if self.dim != 1:
            os = img.get_size()
            img = pygame.transform.rotozoom(img, 0, self.dim)
            ns = img.get_size()
            pos[0] += os[0]//2-ns[0]//2
            pos[1] += os[1]//2-ns[1]//2
        dest.blit(img, pos)

    def update(self):
        # print(self.__next__)
        # print(self.next)
        #d = self.__dict__
        # print(d) __next__
        if self.next > 0:
            self.next -= self.assets.dt
        if self.next <= 0:
            if self.sounds.get(self.x, None):
                self.assets.play_sound(self.sounds[self.x])
            self.x += 1
            self.next += self.delays.get(self.x, self.spd)
            end = len(self.base)
            if self.end is not None:
                end = self.end
            if self.x >= end:
                if self.loops and (not self.loopmode or self.loopmode == "loop"):
                    self.x = 0
                    if self.loops > 1:
                        self.loops -= 1
                        if self.loops == 1:
                            self.loops = 0
                elif self.loopmode in ["blink", "blinknoset"]:
                    self.x = self.start
                    self.next = random.randint(
                        self.blinkspeed[0], self.blinkspeed[1])
                else:
                    self.next = -1
                    self.x -= 1
                    #self.x = 0
        if self.loopmode == "stop":
            self.loops = 0
        if self.base:
            if self.x < len(self.base):
                self.img = self.base[self.x]

class fadesprite(sprite):
    real_path = None
    invert = 0
    tint = None
    greyscale = 0

    def setfade(self, val=255):
        if val < 0:
            val = 0
        if val > 255:
            val = 255
        if getattr(self, "fade", None) is None:
            self.fade = 255
        self.lastfade = self.fade
        self.fade = val
        return self

    def draw(self, dest):
        if getattr(self, "fade", None) is None:
            self.fade = 255
        if self.fade == 0:
            return
        if self.fade == 255 and not self.invert and not self.tint and not self.greyscale:
            return sprite.draw(self, dest)
        if getattr(self, "img", None) and not getattr(self, "mockimg", None):
            if pygame.use_numpy:
                self.mockimg = self.img.convert_alpha()
                self.mockimg_base = [x.convert_alpha() for x in self.base]
                self.origa_base = [pygame.surfarray.array_alpha(
                    x) for x in self.mockimg_base]
                self.draw_func = self.numpydraw
            else:
                self.draw_func = self.mockdraw
                ximg = pygame.Surface(self.img.get_size())
                ximg.fill([255, 0, 255])
                ximg.blit(self.img, [0, 0])
                ximg = ximg.convert()
                ximg.set_colorkey([255, 0, 255])
                self.mockimg = ximg
        if (getattr(self, "tint", None) or getattr(self, "invert", None)) and not getattr(self, "origc_base", None) and pygame.use_numpy:
            self.origc_base = [pygame.surfarray.array3d(
                x) for x in self.mockimg_base]
            # print "set base foo",self.origc_base
        try:
            self.draw_func(dest)
        except Exception:
            import traceback
            traceback.print_exc()
            if pygame.use_numpy:
                pygame.use_numpy = False
                self.mockimg = None
                #import traceback
                # traceback.print_exc()
                raise art_error(
                    "Problem with fading code, switching to older fade technology. Did you install NumPy?")

    def numpydraw(self, dest):
        if not self.mockimg_base:
            return
        px = pygame.surfarray.pixels_alpha(self.mockimg_base[self.x])
        px[:] = self.origa_base[self.x][:]*(self.fade/255.0)
        del px
        px = pygame.surfarray.pixels3d(self.mockimg_base[self.x])
        if getattr(self, "origc_base", None):
            px[:] = self.origc_base[self.x]
            if self.invert:
                px[:] = 255-px[:]
            self.linvert = self.invert
            if self.tint:
                # NumPy 0.10 same_kind cast fix.
                px[:, :, 0] = px[:, :, 0] * self.tint[0]
                px[:, :, 1] = px[:, :, 1] * self.tint[1]
                px[:, :, 2] = px[:, :, 2] * self.tint[2]
            self.lt = self.tint
        del px
        img = self.img
        self.img = self.mockimg_base[self.x]
        if self.greyscale:
            # print(self.name)
            self.lgs = self.greyscale
            ximg = pygame.Surface(self.img.get_size())
            ximg.fill([255, 0, 255])
            ximg.blit(self.img, [0, 0])
            for y in range(ximg.get_height()):
                for x in range(ximg.get_width()):
                    # uses color theory but individually setting each pixel is slow, so an the original should be used once
                    orig_val = ximg.get_at((x, y))
                    # the pygame convert command doesn't set everything to grey when using 8 bit
                    if orig_val.r == 255 and orig_val.g == 0 and orig_val.b == 255:
                        continue
                    gray = int(0.299 * orig_val.r + 0.587 *
                               orig_val.g + 0.114 * orig_val.b)
                    ximg.set_at((x, y), pygame.Color(gray, gray, gray))
            """ximg = ximg.convert(8)
            pal = ximg.get_palette()
            gpal = []
            for col in pal:
                if col == (255,0,255):
                    gpal.append(col)
                    continue
                avg = (col[0]+col[1]+col[2])//3
                gpal.append([avg,avg,avg])
            ximg.set_palette(gpal)"""
            ximg = ximg.convert()
            ximg.set_colorkey([255, 0, 255])
            yimg = pygame.Surface(self.img.get_size()).convert_alpha()
            yimg.fill([0, 0, 0, 0])
            yimg.blit(ximg, [0, 0])
            self.img = yimg
        sprite.draw(self, dest)
        self.img = img

    def mockdraw(self, dest):
        self.mockimg.set_alpha(self.fade)
        img = self.img
        self.img = self.mockimg
        sprite.draw(self, dest)
        self.img = img

    def update(self):
        sprite.update(self)

class case_menu(fadesprite, gui.widget):
    children = []
    parent = None

    def click_down_over(self, mp):
        surf, pos = self.option_imgs[self.choice*3]
        new, npos = self.option_imgs[self.choice*3+1]
        save, spos = self.option_imgs[self.choice*3+2]
        pos = pos[:]
        pos[0] -= self.x
        if mp[0] < pos[0]:
            self.k_left()
        elif mp[0] > pos[0]+surf.get_width():
            self.k_right()
        else:
            if new and mp[1] >= npos[1] and mp[1] <= npos[1]+new.get_height():
                self.enter_down()
            elif save and mp[1] >= spos[1] and mp[1] <= spos[1]+save.get_height():
                self.assets.set_game_folder(self.path+"/"+self.options[self.choice])
                self.assets._load_game_menu(gui, ws_button, choose_game)
                # assets.load_game(self.path+"/"+self.options[self.choice])

    def get_script(self, fullpath):
        dname = os.path.split(fullpath)[1]
        for test in [[fullpath+"/intro.txt", "intro"], [fullpath+"/"+dname+".txt", dname]]:
            if os.path.exists(test[0]):
                return test[1]

    """
    Takes case priority, which is ignored only if value provided is None,
    in favor of the value listed in get_u_layer
    """
    def __init__(self, assets, path="games", priority=None):
        self.pri = sorting.get_u_layer(self.__class__.__name__) if (priority == None) else priority
        self.reload = False
        self.path = path
        fadesprite.__init__(self, assets, screen=2)
        self.base = self.img = assets.Surface([64, 64])
        self.max_fade = 150
        self.next = 0
        self.width = assets.sw
        self.height = assets.sh
        self.options = []
        order = self.assets.variables.get("_order_cases", "alphabetical")
        if order == "alphabetical":
            for d in os.listdir(path):
                full = os.path.join(path, d)
                if os.path.isdir(full):
                    if self.get_script(full):
                        self.options.append(d)
            self.options.sort()
        elif order == "variable":
            opts = {}
            for v in list(assets.variables.keys()):
                if v.startswith("_case_"):
                    try:
                        num = int(v[6:])
                        opts[num] = assets.variables[v]
                    except:
                        continue
            self.options = []
            keys = list(opts.keys())
            keys.sort()
            for k in keys:
                self.options.append(opts[k])
        else:
            raise script_error("_order_cases set to '%s'," % (order,) +
                               "only valid values are 'alphabetical' or " +
                               "'variable'")
        self.init_options()
        self.choice = 0
        self.x = 0
        if os.path.exists(path+"/last"):
            f = open(path+"/last")
            self.choice = int(f.readlines()[0].strip())
            f.close()
        if self.choice >= len(self.options):
            self.choice = 0
        self.scrolling = False
        self.arr = assets.open_art("general/arrow_right")[0]
        self.tried_case = False
        self.primary = True

    def delete(self):
        super(case_menu, self).delete()

    def init_options(self):
        self.option_imgs = []
        base = self.assets.open_art("general/selection_chapter")[0].convert()
        x = self.pos[0]+self.assets.sw/2-base.get_width()/2
        y = self.pos[1]+self.assets.sh/2-base.get_height()/2
        for o in self.options:
            spr = base.copy()

            title = o.replace("_", " ")
            lines = [[]]
            wd_sp = 2
            for word in title.split(" "):
                word = self.assets.get_font("gametitle").render(
                    word, [200, 100, 100])[0]  # 1,
                if sum([wd.get_width() for wd in lines[-1]])+wd_sp*len(lines[-1])+word.get_width() > 160:
                    lines.append([])
                lines[-1].append(word)
            wd_y = spr.get_height()//2-(len(lines)*16)//2
            for line in lines:
                w = sum([wd.get_width() for wd in line])+wd_sp*len(line)
                wd_x = (spr.get_width()-w)/2
                for word in line:
                    spr.blit(word, [wd_x, wd_y])
                    wd_x += word.get_width()+wd_sp
                wd_y += 16
            self.option_imgs.append([spr, [x, y]])

            fnt = self.assets.get_font("new_resume")
            txt = fnt.render("New game", [200, 100, 100])[0]  # 1,
            spr = pygame.transform.scale(
                base, [base.get_width(), base.get_height()//2])
            spr.blit(txt, [(spr.get_width()-txt.get_width()) /
                           2, (spr.get_height()-txt.get_height())/2])
            self.option_imgs.append([spr, [x, y+60]])
            if os.path.exists(self.path+"/"+o+"/save.ns"):
                txt = fnt.render("Resume Game", [200, 100, 100])[0]  # 1,
                spr = pygame.transform.scale(
                    base, [base.get_width(), base.get_height()//2])
                spr.blit(txt, [(spr.get_width()-txt.get_width()) /
                               2, (spr.get_height()-txt.get_height())/2])
                self.option_imgs.append([spr, [x, y+90]])
            elif os.path.exists(self.path+"/"+o+"/save"):
                txt = fnt.render("Resume Game", [200, 100, 100])[0]  # 1,
                spr = pygame.transform.scale(
                    base, [base.get_width(), base.get_height()//2])
                spr.blit(txt, [(spr.get_width()-txt.get_width()) /
                               2, (spr.get_height()-txt.get_height())/2])
                self.option_imgs.append([spr, [x, y+90]])
            elif os.path.exists(self.path+"/"+o+"/autosave.ns"):
                txt = fnt.render("Resume Game", [200, 100, 100])[0]  # 1,
                spr = pygame.transform.scale(
                    base, [base.get_width(), base.get_height()//2])
                spr.blit(txt, [(spr.get_width()-txt.get_width()) /
                               2, (spr.get_height()-txt.get_height())/2])
                self.option_imgs.append([spr, [x, y+90]])
            else:
                self.option_imgs.append([None, None])
            x += self.assets.sw
        self.children = self.option_imgs

    def update(self):
        if self.reload:
            self.option_imgs = []
            self.__init__(self.path)
        spd = (self.choice*constants.STANDARD_SW-self.x)/25.0
        if abs(spd) > 0 and abs(spd) < 10:
            spd = 10*abs(spd)/spd
        spd *= self.assets.dt
        if self.x < self.choice*self.assets.sw:
            self.x += spd
            if self.x > self.choice*self.assets.sw:
                self.x = self.choice*self.assets.sw
        if self.x > self.choice*self.assets.sw:
            self.x += spd
            if self.x < self.choice*self.assets.sw:
                self.x = self.choice*self.assets.sw
        return True

    def k_right(self):
        if self.choice < len(self.options)-1:
            self.choice += 1
            subscript("sound_case_menu_select")
        self.case_screen()

    def k_left(self):
        if self.choice > 0:
            self.choice -= 1
            subscript("sound_case_menu_select")
        self.case_screen()

    def case_screen(self):
        if not self.options:
            return
        if not hasattr(self, "curgame"):
            self.curgame = self.assets.game_folder
        #print(os.path.join(self.path,self.options[self.choice],"case_screen.txt")+"os path")
        if os.path.exists(os.path.join(self.path, self.options[self.choice], "case_screen.txt")):
            from .engine import script
            _scr = script.Script(self.assets.get_stack_top())
            self.assets.stack.append(_scr)
            self.assets.game_folder = self.curgame+"/"+self.options[self.choice]
            self.assets.registry = self.assets.combine_registries()
            self.assets.get_stack_top().init_from_scene("case_screen")
            self.assets.get_stack_top().set_world(_scr.parent.world)

    def enter_down(self):
        f = open(os.path.join(self.path, "last"), "w")
        f.write(str(self.choice))
        f.close()
        self.assets.start_game(
            self.path+"/"+self.options[self.choice], mode="nomenu")

    def draw(self, dest):
        if self.reload:
            return
        if not self.tried_case:
            self.case_screen()
            self.tried_case = 1
        for s, p in self.option_imgs:
            if not s:
                continue
            dest.blit(s, [p[0]-self.x, p[1]])
        if self.x == self.choice*self.assets.sw:
            if self.choice < len(self.options)-1:
                dest.blit(self.arr, [self.pos[0]+240, self.pos[1]+80])
            if self.choice > 0:
                dest.blit(pygame.transform.flip(self.arr, 1, 0),
                          [self.pos[0], self.pos[1]+80])

class graphic(fadesprite):
    def __init__(self, assets, name, *args, **kwargs):
        fadesprite.__init__(self, assets, *args, **kwargs)
        self.load(name)

class penalty(fadesprite):
    def __init__(self, assets, end=100, var="penalty", flash_amount=None):
        self.assets = assets
        self._width = 0 #TODO: get properly
        self._height = 0
        super(penalty, self).__init__(assets)
        self.id_name = "penalty"
        self.var = var
        self.gfx = self.assets.open_art("general/healthbar", key=[255, 0, 255])[0]
        self.left = self.gfx.subsurface([[0, 0], [2, 14]])
        self.right = self.gfx.subsurface([[82, 0], [2, 14]])
        self.good = self.gfx.subsurface([[2, 0], [1, 14]])
        self.bad = self.gfx.subsurface([[66, 0], [1, 14]])
        self.pos = [0, 0]
        if end < 0:
            end = 0
        self.end = end
        self.delay = 50
        self.flash_amount = min(flash_amount, self.gv()
                                ) if flash_amount else flash_amount
        self.flash_color = [255, 242, 129, 150]
        self.flash_dir = 1
        self.change = 0

    def gv(self):
        v = self.assets.variables.get(self.var, 100)
        try:
            v = int(v)
        except:
            v = 100
        return v

    def sv(self, val):
        self.assets.variables[self.var] = str(val)

    def draw(self, dest):
        v = self.gv()
        x = self.assets.sw-110
        dest.blit(self.left, [x, 2])
        x += 2
        if v < 0:
            v = 0
        for i in range(v):
            dest.blit(self.good, [x, 2])
            x += 1
        for i in range(100-v):
            dest.blit(self.bad, [x, 2])
            x += 1
        dest.blit(self.right, [x, 2])
        if self.flash_amount:
            fx = self.assets.sw-108+v-self.flash_amount
            fw = self.flash_amount
            fy = 4
            fh = 10
            surf = pygame.Surface([fw, fh]).convert_alpha()
            surf.fill(self.flash_color)
            dest.blit(surf, [fx, fy])
            self.flash_color[3] += self.flash_dir*8
            if self.flash_color[3] > 200 or self.flash_color[3] < 100:
                self.flash_dir = -self.flash_dir
        self.sv(v)

    def update(self):
        self.change += self.assets.dt
        while self.change > 1:
            self.change -= 1
            v = self.gv()
            if self.end < v:
                v -= 1
                if v < 0:
                    v = 0
            elif self.end > v:
                v += 1
                if v > 100:
                    v = 100
            elif self.delay:
                self.delay -= 1
                if self.delay == 0:
                    self.die()
                    self.delete()
            self.sv(v)
        if self.delay:
            return True

    def die(self):
        if self.gv() <= 0:
            ps = self.assets.variables.get("_penalty_script", "")
            if ps:
                args = []
                if " " in ps:
                    ps, label = ps.split(" ", 1)
                    args.append("label="+label)
                self.assets.get_stack_top()._script("script", ps, *args)

class bg(fadesprite):
    autoclear = True

    def __init__(self, assets, name="", **kwargs):
        super(bg, self).__init__(assets, **kwargs)
        if name:
            self.load("bg/"+name)

class fg(fadesprite):
    autoclear = True

    def __init__(self, assets, name="", **kwargs):
        super(fg, self).__init__(assets, **kwargs)
        if name:
            self.load("fg/"+name)
            self.pos = [(self.assets.sw-self.img.get_width())/2+self.pos[0],
                        (self.assets.sh-self.img.get_height())/2+self.pos[1]]
        self.wait = kwargs.get("wait", 1)
        self.spd = assets.get_frame_delay("fg")

    def update(self):
        super(fg, self).update()  # __next__
        if self.next >= 0 and self.wait:
            return True

# Keep this for saved games
class testimony_blink(fg):

    def __init__(self, assets, name="", **kwargs):
        super(testimony_blink, self).__init__(assets, name, **kwargs)

    id_name = "_tb_blinker_"

    def draw(self, dest):
        self.pos[0] = 0
        self.pos[1] = 22
        self.fade = 255
        if not hasattr(self, "time"):
            self.time = 80
        self.time -= 1
        if self.time == 0:
            self.time = 80
        from .core import vtrue
        if self.time > 20 and vtrue(self.assets.variables.get("_testimony_blinker", "true")):
            w, h = self.img.get_size()
            dest.blit(pygame.transform.scale(
                self.img, [int(w//1.5), int(h//1.5)]), self.pos)

class surf3d(sprite):
    def __init__(self, assets, pos, sw, sh, rw, rh):
        self.id_name = "surf3d"
        self.pos = pos
        self.sw, self.sh = sw, sh
        self.z = 2
        self.pri = -1000
        self.width, self.height = rw, rh
        self.context = context.SoftContext(sw, sh, rw, rh)
        self.surf = self.context.draw()
        self.next = 5
        self.screen_setting = ""
        self.primary = True
        super(surf3d, self).__init__(assets)

    def click_down_over(self, pos):
        if pos[0] >= self.pos[0] and pos[0] <= self.pos[0]+self.width and pos[1] >= self.pos[1] and pos[1] <= self.pos[1]+self.height:
            for o in self.assets.get_stack_top().objects:
                if isinstance(o, mesh):
                    o.click(pos)

    def draw(self, dest):
        # print("draw")
        dest.blit(self.surf, self.getpos())
        for y in range(self.surf.get_height()):
            for x in range(self.surf.get_width()):
                # print(self.surf.get_at((x,y)))
                pass

    def update(self):
        # print("update")
        self.next -= self.assets.dt
        if self.next > 0:  # __next__
            return
        if [x for x in self.context.objects if x.changed]:
            self.surf = self.context.draw().convert()
        [setattr(x, "changed", 0) for x in self.context.objects]
        self.next = 2

class mesh(sprite):
    def __init__(self, assets, meshfile, pos=[0, 0], rot=[0, 0, 0], name="surf3d"):
        self.pos = pos
        self.z = 0
        self.pri = 0
        self.id_name = "mesh"
        self.regions = []
        self.fail = "none"
        self.examine = False
        self.dz = 0
        self.rot = [0, 0, 0]
        self.maxz = 0
        self.minz = -150
        self.meshfile = meshfile
        self.surfname = name
        self.changed = 1
        self.screen_setting = ""
        super(mesh, self).__init__(assets)

    def load(self, script=None):
        if not script:
            script = self.assets.get_stack_top()
        con = None
        for o in self.assets.get_stack_top().all_objects():
            if getattr(o, "id_name", None) == self.surfname:
                con = o
                break
        if not con:
            return
        self.con = con
        path = self.assets.game_folder+"/art/models/"  # /data
        self.ob = ob = con.context.load_object(self.meshfile, path)
        ob.trans(z=-100)
        ob.rot(90, 0, 0)
        ob.changed = 1

    def trans(self, x=0, y=0, z=0):
        if self.dz+z > self.maxz:
            z = self.maxz-self.dz
        elif self.dz+z < self.minz:
            z = self.minz-self.dz
        self.dz += z
        self.ob.trans(x, y, z)
        self.ob.changed = 1

    def click(self, pos):
        if not self.examine:
            return
        x, y = pos
        x = int((x-self.con.pos[0]) *
                (self.con.context.s_w/float(self.con.context.r_w)))
        y = int((y-self.con.pos[1]) *
                (self.con.context.s_h/float(self.con.context.r_h)))
        i = y*self.con.context.s_w+x
        if i >= len(pygame.depth) or i < 0:
            return
        point = pygame.depth[i][1]
        if point:
            u, v = point
            for rect in self.regions:
                if u >= rect[0] and u <= rect[0]+rect[2] and v >= rect[1] and v <= rect[1]+rect[3]:
                    label = rect[4]
                    self.goto(u, v, label)
                    return
            return self.goto(u, v, self.fail)

    def goto(self, u, v, label):
        self.examine = False
        self.assets.variables["_examine_clickx3d"] = str(u)
        self.assets.variables["_examine_clicky3d"] = str(v)
        self.regions[:] = []
        self.assets.get_stack_top().goto_result(label, backup=self.fail)

    def rotate(self, axis, amount):
        r = [0, 0, 0]
        r[axis] = amount
        self.ob.rot(*r)
        self.ob.changed = 1

    def draw(self, dest):
        pass

    def update(self):
        pass

class evidence(fadesprite):
    autoclear = True

    def __init__(self, assets, name="ev", **kwargs):
        self.assets = assets
        if "x" not in kwargs:
            kwargs["x"] = 5
        if "y" not in kwargs:
            kwargs["y"] = 5
        if "pri" not in kwargs:
            kwargs["pri"] = 50
        if not kwargs.get("page", None):
            pages = assets.variables.get(
                "_ev_pages", "evidence profiles").split(" ")
            if len(pages) == 1:
                pages = pages + pages
            if name.endswith("$"):
                kwargs["page"] = pages[1]
            else:
                kwargs["page"] = pages[0]
        self.page = kwargs["page"]
        super(evidence, self).__init__(assets, **kwargs)
        self.id = name
        self.reload()

    def reload(self):
        artname = self.assets.variables.get(
            self.id+"_pic", self.id.replace("$", ""))
        try:
            self.load("ev/"+artname)
        except:
            import traceback
            traceback.print_exc()
            self.img = self.assets.Surface([16, 16])
            self.img.fill([255, 255, 255])
        try:
            self.small = pygame.transform.smoothscale(self.img, [40, 40])
            self.scaled = pygame.transform.smoothscale(self.img, [70, 70])
        except:
            self.small = pygame.transform.scale(self.img, [40, 40])
            self.scaled = pygame.transform.scale(self.img, [70, 70])
        self.setfade()
        self.name = self.assets.variables.get(
            self.id+"_name", self.id.replace("$", ""))
        self.desc = self.assets.variables.get(
            self.id+"_desc", self.id.replace("$", ""))

class saved(fadesprite):
    def __init__(self, assets, ticks=150, text="Saving...", block=True):
        super(saved, self).__init__(assets)
        self.id_name = "_saved_"
        self.text = text
        self.ticks = abs(ticks)
        self.start = self.ticks
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.pos[0] = 0
        self.pos[1] = 0
        self.block = block
        self.width = 0
        self.height = 0

    def draw(self, dest):
        txt1 = self.assets.get_font("itemset_big").render(
            self.text, [230, 230, 230])[0]  # 1,
        txt2 = self.assets.get_font("itemset_big").render(
            self.text, [30, 30, 30])[0]  # 1,
        txt2 = pygame.transform.scale(
            txt2, [txt2.get_width()-4, txt2.get_height()-4])
        dest.blit(txt1, self.pos)
        dest.blit(txt2, [self.pos[0]+2, self.pos[1]+2])

    def update(self):
        if self.ticks <= 0:
            self.delete()
            return False
        self.ticks -= self.assets.dt
        return self.block

class choose_game(gui.widget):
    id_name = "_choose_game_"
    def __init__(self, assets, *args,**kwargs):
        self.assets = assets
        gui.widget.__init__(self,*args,**kwargs)
        self.rpos[1] = other_screen(0, assets.screen_data)
        self.width,self.height = [1000,1000]
        self.z = 1000
        self.pri = -1000
        
        self.list = gui.scrollpane([0,10])
        self.list.width,self.list.height = [assets.sw,assets.sh-10]
        self.add_child(self.list)
        self.jump_when_close = None
        self.sort = "played"
    def update(self,*args):
        from .gui_elements import other_screen
        self.rpos[1] = other_screen(0, self.assets.screen_data)
        [x.update() for x in self.children]
        self.list.updatescroll()
        return False
    def delete(self):
        self.kill = 1
    def close(self):
        self.delete()
        if self.jump_when_close:
            self.assets.get_stack_top().goto_result("close")
    def k_space(self):
        self.close()
    def close_button(self,jump=False):
        self.has_close = True
        self.cb = ws_button(self.assets.screen_data.screen_mode, self,"close")
        self.cb.rpos[0]=224
        self.cb.z = 1005
        self.cb.pri = -1005
        self.children.append(self.cb)
        self.jump_when_close = jump
        self.list.scbar_y=self.cb.height-7
        self.list.scbar_height=-self.cb.height

        self.sort_played_btn = ws_button(self.assets.screen_data.screen_mode, self,"played")
        self.sort_played_btn.rpos[0]=2
        self.sort_played_btn.z = 1005
        self.sort_played_btn.pri = -1005
        self.children.append(self.sort_played_btn)

        self.sort_az_btn = ws_button(self.assets.screen_data.screen_mode, self,"A to Z")
        self.sort_az_btn.rpos[0]=40
        self.sort_az_btn.z = 1005
        self.sort_az_btn.pri = -1005
        self.children.append(self.sort_az_btn)
    def A_to_Z(self,*args):
        self.sort = "az"
        self.list.children[1].children[:] = []
        self.list_games(self.path)
    def played(self,*args):
        self.sort = "played"
        self.list.children[1].children[:] = []
        self.list_games(self.path)
    def list_games(self, path):
        from .libengine import __version__
        self.path = path
        games = []
        if not os.path.exists("games"):
            os.makedirs("games")
        for f in os.listdir(path):
            directory = "games/"+f+"/data"
            if f.startswith("."): continue
            if f in ["art","movies","music","sfx","fonts"]:#"data/art","data/music","data/sfx",
                continue
            if not os.path.isdir(path+"/"+f) and not ".zip" in path+"/"+f:
                continue
            if f in ["data","save_backup"]: continue
            games.append(f)
        try:
            f = open("lastgame")
            self.assets.played = eval(f.read())
            f.close()
        except:
            self.assets.played = []
        games.sort(key=lambda x: x.lower())
        if self.sort == "played":
            for i in reversed(self.assets.played):
                if i in games:
                    games.remove(i)
                    games.insert(0,i)
        for f in games:
            item = ws_button(self.assets.screen_data.screen_mode, self,f)
            d = get_data_from_folder(self.path+"/"+f)
            graphic = pygame.Surface([1,1])
            if d.get("icon",""):
                try:
                    graphic = pygame.image.load(self.path+"/"+f+"/"+d["icon"])#"/data/"+
                except:
                    pass
            title = d.get("title",f)
            if d.get("author",""):
                title += " by "+d["author"]
            lines = [l.text() for l in textutil.wrap_text([title],self.assets.get_text_renderer(NT_FLAG),190)]
            req = d.get("min_pywright_version","0")
            reqs = cver_s(req)
            if int(str(__version__[1])) < int(req):
                lines.append("Requires PyWright "+reqs)
            height = graphic.get_height()
            width = 200
            times_played = self.assets.played.count(f)
            if times_played:
                lines.append("played: %s"%times_played)
            for i in range(len(lines)):
                txt = self.assets.get_font(NT_FLAG).render(lines[i],[0,0,0])[0]#1,
                lines[i] = txt
                w,h = txt.get_size()
                height += h
            image = pygame.Surface([width+2,height+2])
            image.fill([0,0,0])
            pygame.draw.rect(image,gui.defcol["gamebg"],pygame.Rect((1,1),(width,height)))
            image.blit(graphic,[0,0])
            y = graphic.get_height()
            for txt in lines:
                image.blit(txt,[2,y])
                y+=txt.get_height()
            item.graphic = image
            self.list.add_child(item)
            def _play_game(func=f):
                self.assets.played.insert(0,func)
                sf = open("lastgame","w")
                sf.write(repr(self.assets.played))
                sf.close()
                gamedir = self.path+"/"+func
                #TODO: this is where .zipped files could be opened
                dirs = list(os.scandir(gamedir))
                for _f in dirs:
                    #if "
                    if _f.is_dir():
                        if _f.name == "data":
                            continue
                
                directory = _f.path+"/data"
                    
                print("starting 3")
                self.assets.clear_stack()
                self.assets.start_game(gamedir)
            if int(str(__version__[1])) >= int(req):
                setattr(self,f.replace(" ","_"),_play_game)
            else:
                setattr(self,f.replace(" ","_"),lambda: 1)

def trans_y(num_screens,y):
    """Alter y value to place us in the proper screen"""
    if num_screens == 1:
        y -= 192
    return y

class ws_button(gui.button):
    """A button created from WrightScript"""
    screen_setting = ""
    id_name = "_ws_button_"

    def __init__(self, screen_mode, object_containing_func,function_name, *args,**kwargs):
        self.screen_mode = screen_mode
        super(ws_button,self).__init__(object_containing_func,function_name, *args,**kwargs)

    def delete(self):
        self.kill = 1

    def _getrpos(self, num_screens):
        from .assets import trans_y
        rpos = self.rpos[:]
        if self.screen_setting == "try_bottom":
            rpos[1] = trans_y(num_screens,rpos[1])
        return rpos

    def event(self, name, pos, *args):
        orpos = self.rpos[:]
        self.rpos = self._getrpos(self.screen_mode.num_screens)
        ret = super(ws_button, self).event(name, pos, *args)
        self.rpos = orpos
        return ret

    def draw(self, dest):
        orpos = self.rpos[:]
        self.rpos = self._getrpos(self.screen_mode.num_screens)
        super(ws_button, self).draw(dest)
        self.rpos = orpos


class ws_editbox(gui.editbox):
    """An editbox created from WrightScript"""
    screen_setting = ""
    id_name = "_ws_button_"

    def __init__(self, screen_mode):
        self.screen_mode = screen_mode

    def delete(self):
        self.kill = 1

    def _getrpos(self, num_screens):
        rpos = self.rpos[:]
        if self.screen_setting == "try_bottom":
            rpos[1] = trans_y(num_screens,rpos[1])
        return rpos

    def event(self, name, pos, *args):
        orpos = self.rpos[:]
        self.rpos = self._getrpos(self.screen_mode.num_screens)
        ret = super(ws_editbox, self).event(name, pos, *args)
        self.rpos = orpos
        return ret

    def draw(self, dest):
        orpos = self.rpos[:]
        self.rpos = self._getrpos(self.screen_mode.num_screens)
        super(ws_editbox, self).draw(dest)
        self.rpos = orpos

def other_screen(y, screen_data):
    return y+(screen_data.screen_mode.num_screens-1)*screen_data.sh

from .gui_elems.textbox import textbox

class uglyarrow(fadesprite):
    def __init__(self, assets):
        self.assets = assets
        fadesprite.__init__(self, assets, x=0, y=self.assets.sh)
        self.load(self.assets.variables.get("_bigbutton_bg", "bg/main"))
        self.arrow = sprite(assets,0, 0).load("general/arrow_big")
        self.scanlines = fadesprite(assets, 0, 0).load("fg/scanlines")
        self.border_top = fadesprite(assets, 0, 0).load(self.assets.variables.get(
            "_screen2_letterbox_img", "general/bigbutton/border"))
        self.border_bottom = fadesprite(assets, 0, 0).load(self.assets.variables.get(
            "_screen2_letterbox_img", "general/bigbutton/border"))
        self.scanlines.fade = 50
        self.button = None
        self.double = None
        self.textbox = None
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.width = self.iwidth = self.assets.sw
        self.height = self.iheight = self.assets.sh
        self.high = False
        self.showleft = True
        self.last = None
        self.id_name = "_uglyarrow_"

    def show_unclicked(self):
        p = self.assets.variables.get("_bigbutton_img", "general/buttonpress")
        if self.last != p:
            self.last = p
            self.button = sprite(self.assets, 0, 0).load(p)

    def show_clicked(self):
        p = self.assets.variables.get("_bigbutton_img", "general/buttonpress")
        high = extensions.noext(p)+"_high" + extensions.onlyext(p)
        if self.last != high:
            self.last = high
            self.button = sprite(self.assets, 0, 0).load(high)

    def show_cross(self):
        if not self.double:
            self.double = sprite(self.assets, 0, 0).load(self.assets.variables.get(
                "_bigbutton_cross", "general/cross_exam_buttons"))
        self.button = None

    def update(self):
        self.pos[1] = self.assets.sh
        if self.high:
            self.show_clicked()
        else:
            self.show_unclicked()
        if self.textbox and self.textbox.statement:
            self.show_cross()
        self.arrow.update()
        return False

    def draw(self, dest):
        fadesprite.draw(self, dest)
        if self.button:
            self.button.pos[0] = (self.assets.sw-self.button.img.get_width())//2
            self.button.pos[1] = (
                self.assets.sh-self.button.img.get_height())//2+self.assets.sh
            self.button.draw(dest)
            self.iwidth = self.button.img.get_width()
            self.iheight = self.button.img.get_height()
            if self.can_click():
                self.arrow.pos[0] = (self.assets.sw-self.arrow.img.get_width())//2
                self.arrow.pos[1] = (
                    self.assets.sh-self.arrow.img.get_height())//2+self.assets.sh
                self.arrow.draw(dest)
        elif self.double:
            self.double.pos[0] = (self.assets.sw-self.double.img.get_width())//2
            self.double.pos[1] = (
                self.assets.sh-self.double.img.get_height())//2+self.assets.sh
            self.double.draw(dest)
            self.iwidth = self.double.img.get_width()
            self.iheight = self.double.img.get_height()
            if self.can_click():
                self.arrow.pos[0] = (
                    self.assets.sw-self.arrow.img.get_width())//2-75
                self.arrow.pos[1] = (
                    self.assets.sh-self.arrow.img.get_height())//2+self.assets.sh
                self.arrow.img = pygame.transform.flip(self.arrow.img, 1, 0)
                if self.showleft:
                    self.arrow.draw(dest)
                self.arrow.pos[0] = (
                    self.assets.sw-self.arrow.img.get_width())//2+70
                self.arrow.pos[1] = (
                    self.assets.sh-self.arrow.img.get_height())//2+self.assets.sh
                self.arrow.img = pygame.transform.flip(self.arrow.img, 1, 0)
                self.arrow.draw(dest)
        if vtrue(self.assets.variables.get("_screen2_scanlines", "off")):
            self.scanlines.pos = self.pos
            self.scanlines.draw(dest)
        if vtrue(self.assets.variables.get("_screen2_letterbox", "on")):
            self.border_top.pos = self.pos
            self.border_top.draw(dest)
            self.border_bottom.pos[0] = self.pos[0]
            self.border_bottom.pos[1] = self.pos[1] + \
                192-self.border_bottom.height
            self.border_bottom.draw(dest)

    def over(self, mp):
        if self.button:
            if mp[0] >= self.button.pos[0] and mp[1] >= self.button.pos[1]\
                    and mp[0] <= self.button.pos[0]+self.iwidth\
                    and mp[1] <= self.button.pos[1]+self.iheight:
                return True
        if self.double:
            if mp[0] >= self.double.pos[0] and mp[1] >= self.double.pos[1]\
                    and mp[0] <= self.double.pos[0]+self.iwidth/2\
                    and mp[1] <= self.double.pos[1]+self.iheight:
                return "left"
            if mp[0] >= self.double.pos[0]+self.iwidth/2 and mp[1] >= self.double.pos[1]\
                    and mp[0] <= self.double.pos[0]+self.iwidth\
                    and mp[1] <= self.double.pos[1]+self.iheight:
                return "right"

    def move_over(self, mp, rel, bt):
        if not self.over(mp):
            if self.high:
                self.high = None
        else:
            if self.high == None:
                self.high = True

    def click_down_over(self, mp):
        gui.window.focused = self
        over = self.over(mp)
        if over == True and not self.high and self.can_click():
            self.high = True
        if over == "left" and self.can_click() and self.showleft:
            self.textbox.k_left()
        if over == "right" and self.can_click():
            self.textbox.k_right()

    def click_up_over(self, mp):
        if self.high:
            self.high = False
            if self.can_click():
                self.textbox.enter_down()

    def can_click(self):
        return self.textbox and not getattr(self.textbox, "kill", 0) and self.textbox.can_continue()

class menu(fadesprite, gui.widget):
    z = 5
    fail = "none"
    id_name = "invest_menu"

    def over(self, mp):
        oy = self.getpos()[1]
        for o in self.options:
            p2 = self.opos[o]
            w, h = self.opt.get_width()//2, self.opt.get_height()//2
            if mp[0] >= p2[0] and mp[0] <= p2[0]+w and mp[1] >= p2[1]+oy and mp[1] <= p2[1]+h+oy:
                return o

    def move_over(self, pos, rel, buttons):
        if buttons[0]:
            self.click_down_over(pos)

    def click_down_over(self, mp):
        gui.window.focused = self
        o = self.over(mp)
        if o is not None:
            self.selected = o

    def click_up(self, mp):
        o = self.over(mp)
        if self.selected == o and o is not None:
            self.enter_down()

    def __init__(self, assets):
        self.assets = assets
        self.bg = None
        oy = 192
        fadesprite.__init__(self, self.assets, x=0, y=oy)
        gui.widget.__init__(self, [0, oy], [self.assets.sw, self.assets.sh])
        self.load("general/black")
        self.max_fade = int(
            float(self.assets.variables.get("_menu_fade_level", 50)))
        self.fade = 0
        if self.max_fade == 0:
            self.setfade(0)
        else:
            self.assets.get_stack_top().objects.append(
                fadeanim(self.assets, start=0, end=self.max_fade, speed=3, wait=1, name=None, obs=[self]))
        self.options = []
        self.selected = ""
        self.opt = self.assets.open_art("general/talkbuttons", key=[255, 0, 255])[0]
        stx, sty = (self.assets.sw-self.opt.get_width()) / \
            2, (self.assets.sh-self.opt.get_height())/2
        self.opos_c = {"examine": [0, 0], "move": [1, 0],
                       "talk": [0, 1], "present": [1, 1]}
        self.opos_l = [["examine", "move"], ["talk", "present"]]
        self.opos = {"examine": [stx, sty], "move": [stx+self.opt.get_width()/2, sty],
                     "talk": [stx, sty+self.opt.get_height()/2], "present": [stx+self.opt.get_width()/2, sty+self.opt.get_height()/2]}
        imgs = []
        x = y = 0
        while y < self.opt.get_height():
            while x < self.opt.get_width():
                imgs.append(self.opt.subsurface(
                    [[x, y], [self.opt.get_width()//2, self.opt.get_height()//2]]))
                x += self.opt.get_width()//2
            y += self.opt.get_height()//2+1
            x = 0
        self.oimgs = {
            "examine": imgs[0], "move": imgs[1], "talk": imgs[2], "present": imgs[3]}
        self.opthigh = self.assets.open_art(
            "general/talkbuttons_high", key=[255, 0, 255])[0]
        imgs = []
        x = y = 0
        while y < self.opthigh.get_height():
            while x < self.opthigh.get_width():
                imgs.append(self.opthigh.subsurface(
                    [[x, y], [self.opthigh.get_width()//2, self.opthigh.get_height()//2]]))
                x += self.opthigh.get_width()//2
            y += self.opthigh.get_height()//2+1
            x = 0
        self.oimgshigh = {
            "examine": imgs[0], "move": imgs[1], "talk": imgs[2], "present": imgs[3]}
        self.open_script = True
        self.primary = True

    def init_normal(self):
        # 
        subscript("show_court_record_button")

    def delete(self):
        super(menu, self).delete()
        # 
        subscript("hide_court_record_button")

    def update(self):
        if not self.options:
            self.delete()
        fadesprite.update(self)
        self.screen_setting = "try_bottom"
        return True

    def get_coord(self):
        try:
            return self.opos_c[self.selected][:]
        except:
            return [0, 0]

    def k_right(self):
        coord = self.get_coord()
        coord[0] += 1
        if coord[0] > 1:
            coord[0] = 0
        sel = self.opos_l[coord[1]][coord[0]]
        if sel in self.options:
            self.selected = sel
        # 
        subscript("sound_investigate_menu_select")

    def k_left(self):
        coord = self.get_coord()
        coord[0] -= 1
        if coord[0] < 0:
            coord[0] = 1
        sel = self.opos_l[coord[1]][coord[0]]
        if sel in self.options:
            self.selected = sel
        # 
        subscript("sound_investigate_menu_select")

    def k_up(self):
        coord = self.get_coord()
        coord[1] -= 1
        if coord[1] < 0:
            coord[1] = 1
        sel = self.opos_l[coord[1]][coord[0]]
        if sel in self.options:
            self.selected = sel
        # 
        subscript("sound_investigate_menu_select")

    def k_down(self):
        coord = self.get_coord()
        coord[1] += 1
        if coord[1] > 1:
            coord[1] = 0
        sel = self.opos_l[coord[1]][coord[0]]
        if sel in self.options:
            self.selected = sel
        # 
        subscript("sound_investigate_menu_select")

    def enter_down(self):
        if self.open_script:
            self.assets.get_stack_top().init_from_scene(self.scene+"."+self.selected)
        else:
            self.assets.get_stack_top().goto_result(self.selected, backup=self.fail)
        self.delete()
        # 
        subscript("sound_investigate_menu_confirm")

    def addm(self, opt):
        if opt:
            self.options.append(opt)
        if not self.selected:
            self.selected = opt

    def delm(self, opt):
        if opt in self.options:
            self.options.remove(opt)
            if self.selected == opt:
                self.selected = None

    def draw(self, dest):
        if not self.bg:
            for o in reversed(self.assets.get_stack_top().objects):
                if isinstance(o, bg):
                    self.bg = o.img.copy()
                    break
        self.screen_setting = "try_bottom"
        if self.bg:
            dest.blit(self.bg, self.getpos())
        if not hasattr(self, "fade") or self.fade >= self.max_fade:
            for o in self.options:
                if self.selected == o:
                    dest.blit(self.oimgshigh[o], [
                              self.opos[o][0], self.opos[o][1]+self.getpos()[1]])
                else:
                    dest.blit(self.oimgs[o], [
                              self.opos[o][0], self.opos[o][1]+self.getpos()[1]])

class listmenu(fadesprite, gui.widget):
    fail = "none"
    id_name = "list_menu_id"

    def over(self, mp):
        if getattr(self, "kill", 0):
            return False
        x = (self.assets.sw-self.choice.img.get_width())/2
        y = self.getpos()[1]+30
        si = None
        i = 0
        for c in self.options:
            if mp[0] >= x and mp[1] >= y and mp[0] <= x+self.choice.width and mp[1] <= y+self.choice.height:
                si = i
            i += 1
            y += self.choice.img.get_height()+5
        return si

    def move_over(self, pos, rel, buttons):
        if getattr(self, "kill", 0):
            return False
        if buttons[0]:
            self.click_down_over(pos)

    def click_down_over(self, mp):
        if getattr(self, "kill", 0):
            return False
        gui.window.focused = self
        si = self.over(mp)
        if si is not None:
            self.si = si
            self.selected = self.options[self.si]

    def click_up(self, mp):
        if getattr(self, "kill", 0):
            return False
        si = self.over(mp)
        if self.si == si and si is not None:
            self.enter_down()

    def __init__(self, assets, tag=None):
        self.assets = assets
        subscript("hide_court_record_button")
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        x, y = 0, 192
        gui.widget.__init__(self, [x, y], [self.assets.sw, self.assets.sh])
        fadesprite.__init__(self, self.assets, x=x, y=y)
        self.load(self.assets.variables.get("_list_bg_image", "general/black"))
        self.max_fade = int(
            float(self.assets.variables.get("_menu_fade_level", 50)))
        self.fade = 0
        if self.assets.screen_mode.num_screens == 2 and not vtrue(self.assets.variables.get("_double_screen_list_fade", "false")):
            self.setfade(255)
        elif self.max_fade == 0:
            self.setfade(0)
        else:
            self.assets.get_stack_top().objects.append(
                fadeanim(self.assets, start=0, end=self.max_fade, speed=3, wait=1, name=None, obs=[self]))
        self.options = []
        self.si = 0
        self.selected = ""
        self.choice = fadesprite(self.assets).load("general/talkchoice")
        self.choice_high = fadesprite(self.assets).load("general/talkchoice_high")
        self.hidden = True
        self.tag = tag
        self.primary = True

    def init_normal(self):
        # 
        subscript("hide_court_record_button")

    def delete(self):
        # 
        subscript("show_court_record_button")
        self.kill = 1
        if hasattr(self, "bck"):
            self.bck.kill = 1

    def update(self):
        fadesprite.update(self)
        self.screen_setting = "try_bottom"
        if self.hidden:
            return False
        if not hasattr(self, "bck") and vtrue(self.assets.variables.get("_list_back_button", "true")) and not getattr(self, "noback", False):
            self.bck = guiBack(self.assets)
            self.bck.pri = 1000

            def k_space(b=self.bck):
                self.delete()
                # 
                subscript("sound_list_menu_cancel")
                print("kill back button and self")
                self.assets.variables["_selected"] = "Back"
            self.bck.k_space = k_space
            self.assets.get_stack_top().objects.append(self.bck)
        if not self.options:
            self.k_space()
        return True

    def k_up(self):
        if getattr(self, "kill", 0):
            return False
        self.si -= 1
        if self.si < 0:
            self.si = len(self.options)-1
        self.selected = self.options[self.si]
        self.change_selected()
        # 
        subscript("sound_list_menu_select")

    def k_down(self):
        if getattr(self, "kill", 0):
            return False
        self.si += 1
        if self.si >= len(self.options):
            self.si = 0
        self.selected = self.options[self.si]
        self.change_selected()
        # 
        subscript("sound_list_menu_select")

    def enter_down(self):
        if getattr(self, "kill", 0):
            return False
        if not self.selected:
            return
        # 
        subscript("sound_list_menu_confirm")
        if self.tag:
            self.assets.lists[self.tag][self.selected["label"]] = 1
        if self.selected["result"] != "Back":
            self.assets.variables["_selected"] = self.selected["result"]
            self.assets.get_stack_top().goto_result(
                self.selected["result"], backup=self.fail)
        else:
            self.assets.variables["_selected"] = "Back"
        self.delete()

    def change_selected(self):
        print("working")
        scr = self.options[self.si].get("on_select", None)
        if scr:
            self.assets.get_stack_top().execute_macro(scr)
            # subscript(scr)
        if not hasattr(self, "bck") and vtrue(self.assets.variables.get("_list_back_button", "true")) and not getattr(self, "noback", False):
            self.bck = guiBack(self.assets)
            self.bck.pri = 1000

            def k_space(b=self.bck):
                self.delete()
                # 
                subscript("sound_list_menu_cancel")
                print("kill back button and self")
                self.assets.variables["_selected"] = "Back"
            self.bck.k_space = k_space
            self.assets.get_stack_top().objects.append(self.bck)

    def draw(self, dest):
        if getattr(self, "kill", 0):
            return False
        if not self.selected and self.options:
            self.selected = self.options[self.si]
            self.change_selected()
        fadesprite.draw(self, dest)
        x = (self.assets.sw-self.choice.img.get_width())/2
        y = self.getpos()[1]+30
        # self.choice.setfade(200)
        # self.choice_high.setfade(200)
        for c in self.options:
            if 0:  # self.selected == c:
                img = self.choice_high.img.copy()
            else:
                img = self.choice.img.copy()
            rt = c["label"]
            checkmark = self.assets.variables.get(
                "_list_checked_img", "general/checkmark")
            if "checkmark" in c:
                checkmark = c["checkmark"]
            try:
                checkmark = sprite(self.assets).load(checkmark)
            except:
                checkmark = None
            if (not (checkmark and checkmark.width)) and self.tag and self.assets.lists[self.tag].get(rt, None):
                rt = "("+rt+")"
            txt = self.assets.get_text_renderer("list").render(rt, [110, 20, 20])
            img.blit(txt, [(img.get_width()-txt.get_width())/2,
                           (img.get_height()-txt.get_height())/2])
            dest.blit(img, [x, y])
            if self.selected == c:
                lwi = 2
                color = color_str(self.assets.variables, self.assets.variables.get(
                    "_list_outline_color", "ffaa45"))
                pygame.draw.line(dest, color, [x-1, y+8], [x-1, y+1], lwi)
                pygame.draw.line(dest, color, [x+1, y-2], [x+8, y-2], lwi)

                pygame.draw.line(
                    dest, color, [x+img.get_width(), y+8], [x+img.get_width(), y+1], lwi)
                pygame.draw.line(
                    dest, color, [x+img.get_width()-2, y-2], [x+img.get_width()-9, y-2], lwi)

                pygame.draw.line(dest, color, [x+img.get_width(), y+img.get_height()-2], [
                                 x+img.get_width(), y+img.get_height()-9], lwi)
                pygame.draw.line(dest, color, [x+img.get_width()-2, y+img.get_height()], [
                                 x+img.get_width()-9, y+img.get_height()], lwi)

                pygame.draw.line(
                    dest, color, [x-1, y+img.get_height()-2], [x-1, y+img.get_height()-9], lwi)
                pygame.draw.line(
                    dest, color, [x+1, y+img.get_height()], [x+8, y+img.get_height()], lwi)
            if checkmark and checkmark.width and self.tag and self.assets.lists[self.tag].get(rt, None):
                if "check_x" in c:
                    cx = int(c["check_x"])
                else:
                    cx = int(self.assets.variables.get("_list_checked_x", "-10"))
                if "check_y" in c:
                    cy = int(c["check_y"])
                else:
                    cy = int(self.assets.variables.get("_list_checked_y", "-10"))
                dest.blit(checkmark.base[0], [x+cx, y+cy])
            y += self.choice.img.get_height()+5

    def k_space(self):
        if getattr(self, "kill", 0):
            return False
        if hasattr(self, "bck") or "Back" in self.options:
            self.delete()
            # 
            subscript("sound_list_menu_cancel")

class examine_menu(sprite, gui.widget):
    fail = "none"

    def move_over(self, pos, rel, buttons):
        if self.xscrolling:
            return
        if gui.window.focused == self:
            self.mx, self.my = [pos[0], pos[1]-self.getpos()[1]]
            self.highlight()

    def click_down_over(self, mp):
        if self.xscrolling:
            return
        gui.window.focused = self
        if self.hide or self.selected == ["none"] or mp[0] < 175 or mp[1] < 159+self.getpos()[1]:
            self.move_over(mp, None, None)

    def click_up(self, mp):
        if self.xscrolling:
            return
        if gui.window.focused == self:
            self.enter_down()
            gui.window.over = None
            gui.window.focused = None

    def initialize_scroll(self, amtx, amty, speed):
        return scroll(self.assets, amtx, amty, speed)

    def __init__(self, assets, hide=False, name="blah"):
        self.assets = assets
        self.name = name
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        sprite.__init__(self, assets)
        self.pos = [0, 192]
        self.z = sorting.get_z_layer(self.__class__.__name__)
        self.width = self.assets.sw
        self.height = self.assets.sh
        gui.widget.__init__(self, self.pos, [self.assets.sw, self.assets.sh])
        self.img = self.assets.Surface([64, 64])
        self.regions = []
        self.mouse = pygame.Surface([10, 10])
        self.mouse.fill([100, 100, 100])
        self.selected = [None]
        self.mx, self.my = self.assets.sw/2, self.assets.sh/2
        self.check = self.assets.open_art(
            "general/check"+self.assets.appendgba, key=[255, 0, 255])[0]
        self.hide = hide
        self.bg = []
        if not self.assets.variables.get("_examine_use", None):
            self.bg = [x for x in self.assets.get_stack_top().objects if isinstance(x, bg)]
        else:
            self.bg = [x for x in self.assets.get_stack_top().objects if getattr(
                x, "id_name", None) == self.assets.variables["_examine_use"]]
        self.fg = self.assets.open_art("general/examinefg")[0]
        self.xscroll = 0
        self.xscrolling = 0
        scroll_amt = self.assets.variables.get("_xscroll_"+self.name, 0)
        if scroll_amt == -1:
            self.xscroll = -1
            if self.getoffset() != -constants.STANDARD_SW:
                self.assets.get_stack_top().objects.append(
                    self.initialize_scroll(amtx=-constants.STANDARD_SW, amty=0, speed=constants.STANDARD_SW))
        self.blocking = not vtrue(
            self.assets.variables.get("_examine_skipupdate", "0"))
        self.klefth = self.krighth = self.kuph = self.kdownh = 0
        self.screen_setting = "try_bottom"
        self.primary = True

    def init_normal(self):
        # 
        subscript("hide_court_record_button")

    def delete(self):
        # 
        subscript("show_court_record_button")
        self.kill = 1
        if hasattr(self, "bck"):
            self.bck.delete()
        if hasattr(self, "scrollbut"):
            self.scrollbut.delete()
        # 
        subscript("hide_court_record_button")

    def getoffset(self):
        x = [o.pos[0] for o in self.bg]
        if not x:
            return 0
        x.sort()
        return x[0]

    def screens(self):
        screens = 1
        xes = [o.pos[0] for o in self.bg]
        xes.sort()
        smallest = 0
        if xes:
            smallest = xes[0]
        for mx in xes:
            x = mx-smallest
            if x >= self.assets.sw and screens < 2:
                screens = 2
            if x >= self.assets.sw*2 and screens < 3:
                screens = 3
        xes = [o[0] for o in self.regions]
        xes.sort()
        smallest = 0
        if xes:
            smallest = xes[0]
        for mx in xes:
            x = mx-smallest
            if x >= self.assets.sw and screens < 2:
                screens = 2
            if x >= self.assets.sw*2 and screens < 3:
                screens = 3
        self.width = screens*self.assets.sw
        return screens

    def addregion(self, x, y, width, height, label):
        reg = [int(x), int(y), int(width), int(height), label]
        self.regions.append(reg)
        self.highlight()

    def highlight(self):
        self.selected = [None]
        for reg in self.regions:
            if self.mx > reg[0]+self.getoffset() and self.my > reg[1] and \
                    self.mx < reg[0]+self.getoffset()+reg[2] and self.my < reg[1]+reg[3]:
                self.selected = reg
                return

    def draw(self, dest):
        self.highlight()
        if not self.assets.variables.get("_examine_use", None):
            [dest.blit(o.img, [o.pos[0], o.pos[1]+self.getpos()[1]])
             for o in self.bg]
        my = self.my+self.getpos()[1]
        if vtrue(self.assets.variables.get("_examine_showcursor", "true")):
            if self.assets.variables.get("_examine_cursor_img", "").strip():
                spr = (self.assets,0, 0)
                spr.load(self.assets.variables.get("_examine_cursor_img", ""))
                spr.pos[0] = self.mx-spr.width//2
                spr.pos[1] = my-spr.height//2
                spr.draw(dest)
            else:
                col = color_str(self.assets.variables, self.assets.variables.get(
                    "_examine_cursor_col", WHITE))
                pygame.draw.line(dest, col, [0, my], [self.mx-5, my])
                pygame.draw.line(dest, col, [self.mx+5, my], [self.assets.sw, my])
                pygame.draw.line(
                    dest, col, [self.mx, self.getpos()[1]], [self.mx, my-5])
                pygame.draw.line(
                    dest, col, [self.mx, my+5], [self.mx, self.getpos()[1]+self.assets.sh])
                pygame.draw.rect(dest, col, ((self.mx-5, my-5), (10, 10)), 1)
        if vtrue(self.assets.variables.get("_examine_showbars", "true")):
            dest.blit(self.fg, [0, self.getpos()[1]])
        if self.selected != [None] and not self.hide:
            dest.blit(self.check, [self.assets.sw-self.check.get_width(),
                                   self.getpos()[1]+self.assets.sh-self.check.get_height()])

    def k_left(self, *args):
        self.klefth = 1

    def k_right(self, *args):
        self.krighth = 1

    def k_up(self, *args):
        self.kuph = 1

    def k_down(self, *args):
        self.kdownh = 1

    def update(self, *args):
        if self.xscrolling:
            self.assets.get_stack_top().objects.append(self.initialize_scroll(-self.xscrolling, 0, speed=16))
            self.xscrolling = 0
            if hasattr(self, "scrollbut"):
                self.scrollbut.delete()
                del self.scrollbut
            return self.blocking
        keys = pygame.key.get_pressed()
        spd = 3*self.assets.dt
        #print(str(spd)+" spd")
        d = [0, 0]
        if keys[pygame.K_LEFT] or self.assets.screen_data.jsleft():
            d[0] -= spd
        if keys[pygame.K_RIGHT] or self.assets.screen_data.jsright():
            d[0] += spd
        if keys[pygame.K_UP] or self.assets.screen_data.jsup():
            d[1] -= spd
        if keys[pygame.K_DOWN] or self.assets.screen_data.jsdown():
            d[1] += spd
        self.klefth = self.krighth = self.kuph = self.kdownh = 0
        self.mx += d[0]
        self.my += d[1]
        if self.mx-5 < 0:
            self.mx = 5
        if self.mx+5 > self.assets.sw:
            self.mx = self.assets.sw-5
        if self.my-5 < 0:
            self.my = 5
        if self.my+5 > self.assets.sh:
            self.my = self.assets.sh-5
        if self.assets.variables.get("_examine_scrolling", None) == "perceive":
            def add(x):
                x.pos[0] -= d[0]
                x.pos[1] -= d[1]
            [add(x) for x in self.bg]
            x = float(self.assets.variables.get("_examine_offsetx", 0))
            y = float(self.assets.variables.get("_examine_offsety", 0))
            x -= d[0]
            y -= d[1]
            self.assets.variables["_examine_offsetx"] = str(x)
            self.assets.variables["_examine_offsety"] = str(y)
            self.highlight()
            return False
        self.highlight()
        if not hasattr(self, "bck") and not self.hide:
            self.bck = guiBack(self.assets)
            self.bck.pri = 1000

            def k_space(b=self.bck):
                self.k_space()
            self.bck.k_space = k_space
            self.bck.update = lambda *x: False
            self.assets.get_stack_top().objects.append(self.bck)
        scrn = (-self.getoffset()//self.assets.sw)+1
        self.xscroll = None
        if scrn < self.screens():
            self.xscroll = 1
        elif scrn > 1:
            self.xscroll = -1
        if not self.xscroll and hasattr(self, "scrollbut"):
            self.scrollbut.delete()
            del self.scrollbut
        if self.xscroll and not hasattr(self, "scrollbut"):
            self.scrollbut = guiScroll(self.assets, self.xscroll)
            self.scrollbut.parent = self
            self.assets.get_stack_top().objects.append(self.scrollbut)
        self.assets.variables["_xscroll_"+self.name] = self.xscroll
        return self.blocking

    def enter_down(self):
        self.assets.variables["_examine_clickx"] = str(self.mx)
        self.assets.variables["_examine_clicky"] = str(self.my)
        print("FAIL ", self.fail)
        go = self.selected[-1]
        if go == None:
            go = self.fail
        self.assets.get_stack_top().goto_result(go, backup=self.fail)
        self.delete()

    def k_space(self):
        if not self.hide:
            self.delete()
            # 
            subscript("sound_examine_menu_cancel")

class evidence_menu(fadesprite, gui.widget):
    fail = "none"

    def click_down_over(self, mp):
        mp[1] -= self.getpos()[1]
        gui.window.focused = self
        if self.mode == "overview" and mp[1] >= 0 and mp[1] <= 30 and mp[0] <= 260 and mp[0] >= 160:
            pressedbg = self.assets.variables["ev_mode_bg_" +
                                         self.item_set+"_pressed"]
            if("evidence_pressed" in pressedbg):
                return
            # FIXME: mysterious evidence pressed bug. 
            # Put a workaround on top of the workaround
            string = pressedbg+self.assets.appendgba
            if (string != ""):  # workaround
                self.load(pressedbg+self.assets.appendgba)
            self.background_pressed = True
        if mp[0] >= 0 and mp[0] <= 78 and mp[1] >= 162 and mp[1] <= 192:
            if self.canback():
                self.back_button.highlight()

    def hold_down_over(self, mp):
        pass

    def move_over(self, mp, rel, buttons):
        pass

    def click_up(self, mp):
        mp[1] -= self.getpos()[1]
        self.back_button.unhighlight()
        if self.mode == "overview" and mp[1] >= 0 and mp[1] <= 30 and mp[0] <= 260 and mp[0] >= 160:
            # self.load(self.assets.variables["ev_mode_bg_evidence"]+self.assets.appendgba)
            # self.set_bg(self.assets.variables["ev_mode_bg_evidence"])
            #defbg = self.assets.variables["ev_mode_bg_evidence"]
            bg = self.assets.variables.get("ev_mode_bg_"+self.item_set)
            #self.assets.variables.set("ev_mode_bg_"+self.item_set, "evidence")
            self.load(bg+self.assets.appendgba)
            self.background_pressed = False

        if self.mode == "overview" and mp[0] >= 36 and mp[1] >= 62 and mp[0] <= 218 and mp[1] <= 145:
            rx = (218-36)//4
            ry = (145-62)//2
            sx, sy = self.sx, self.sy
            self.sx = (mp[0]-36)/rx
            self.sy = (mp[1]-62)/ry
            try:
                self.choose()
            except:
                import traceback
                traceback.print_exc()
                self.sx, self.sy = sx, sy
                return
            self.back = False
            self.back_button.unhighlight()
            print("6")
            self.enter_down()
        if mp[0] >= 0 and mp[0] <= 78 and mp[1] >= 162 and mp[1] <= 192:
            if self.canback():
                self.back = True
                self.enter_down()
                self.back = False
                self.back_button.unhighlight()
            #self.sx,self.sy = [0,0]
        if mp[0] >= 0 and mp[1] >= 56 and mp[0] <= 16 and mp[1] <= 149:
            if self.mode == "overview" and len(self.pages) > 1:
                self.page_prev()
                # 
                subscript("sound_court_record_scroll")
            if self.mode == "zoomed":
                self.k_left()
        if mp[0] >= 238 and mp[1] >= 56 and mp[1] <= 149:
            if self.mode == "overview" and len(self.pages) > 1:
                self.page_next()
                # 
                subscript("sound_court_record_scroll")
            if self.mode == "zoomed":
                self.k_right()
        if mp[0] >= 177 and mp[1] <= 29:
            self.switch = True
            self.enter_down()
            self.switch = False
        check = self.assets.open_art("general/check"+self.assets.appendgba)[0]
        chk = [self.assets.sw-check.get_width(), self.assets.sh-check.get_height()]
        if mp[0] >= chk[0] and mp[1] >= chk[1]:
            self.do_check()
        #~ self.enter_down()
        # ~ gui.window.over = None
        # ~ gui.window.focused = None

    def load(self, *args, **kwargs):
        fadesprite.load(self, *args, **kwargs)

    def __init__(self, assets, items=[], gba=True):
        self.assets = assets
        subscript("sound_court_record_display")
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        x, y = 0, 192
        self.z = sorting.get_z_layer(self.__class__.__name__)
        print("evidence z", self.z)
        fadesprite.__init__(self, self.assets, x=x, y=y)
        gui.widget.__init__(self, [x, y], [self.assets.sw, self.assets.sh])
        self.items = items
        self.load(self.assets.variables["ev_mode_bg_evidence"]+self.assets.appendgba)
        self.cursor = self.assets.open_art(self.assets.variables["ev_cursor_img"])[0]
        self.page = 0
        self.sx, self.sy = [0, 0]
        self.examine = None
        self.present = None

        self.background_pressed = False

        self.back_button = guiBack(self.assets)
        self.back_button.pri = 1000

        def k_space(b=self.back_button):
            b.delete()
            self.delete()
            self.assets.variables["_selected"] = "Back"
        self.back_button.k_space = k_space

        self.back = False
        self.switch = False
        self.chosen = None
        self.mode = "overview"  # overview, zoomed, check
        if self.assets.gbamode:
            self.mode = "zoomed"
        self.checking = None
        self.ev_zoom = self.assets.open_art(
            self.assets.variables["ev_z_bg"]+self.assets.appendgba)[0]
        self.scroll = 0
        self.scroll_dir = 0

        self.pages_set = self.assets.variables.get(
            "_ev_pages", "evidence profiles").split(" ")
        for p in self.pages_set[:]:
            if not vtrue(self.assets.variables.get("_%s_enabled" % p, "true")):
                self.pages_set.remove(p)
        if not self.pages_set:
            self.delete()

        # Loading saved position
        self.item_set = self.assets.variables.get(
            "_cr_current_item_set", self.pages_set[0])
        if self.item_set not in self.pages_set:
            self.item_set = self.pages_set[0]
        self.layout()
        if not self.pages:
            self.item_set = self.pages_set[0]
            self.layout()
        self.page = int(self.assets.variables.get("_cr_current_page", 0))
        self.sx = int(self.assets.variables.get("_cr_current_selected_x", 0))
        self.sy = int(self.assets.variables.get("_cr_current_selected_y", 0))

        self.screen_setting = "try_bottom"
        self.script = self.assets.get_stack_top()
        self.primary = True

    def delete(self):
        super(evidence_menu, self).delete()
        # 
        subscript("hide_present_button2")

    def update(self):
        self.choose()
        # present button
        if not hasattr(self, "present_button"):
            self.present_button = False
        if self.can_present():
            if not self.present_button:
                # 
                subscript("show_present_button2")
                self.present_button = True
        else:
            if self.present_button:
                # 
                subscript("hide_present_button2")
                self.present_button = False
        if not getattr(self, "hidden", None):
            return True  # Don't update anything else

    def layout(self):
        self.pages = []
        lines = []
        line = []
        for icon in self.items:
            icon.reload()
            if self.item_set != icon.page:
                continue
            line.append(icon)
            if len(line) == 4:
                lines.append(line)
                line = []
                if len(lines) == 2:
                    self.pages.append(lines)
                    lines = []
        if line:
            lines.append(line)
        if lines:
            self.pages.append(lines)
        self.page = 0
        self.sx = 0
        self.sy = 0

    def remember_vars(self):
        self.assets.variables.set("_cr_current_item_set", self.item_set)
        self.assets.variables.set("_cr_current_page", self.page)
        self.assets.variables.set("_cr_current_selected_x", self.sx)
        self.assets.variables.set("_cr_current_selected_y", self.sy)

    def page_prev(self):
        self.page -= 1
        if self.page < 0:
            self.page = len(self.pages)-1
        page = self.pages[self.page]
        self.sy = len(page)-1
        self.sx = len(page[self.sy])-1
        self.remember_vars()

    def page_next(self):
        self.page += 1
        if self.page > len(self.pages)-1:
            self.page = 0
        self.sx = 0
        page = self.pages[self.page]
        if self.sy > len(page)-1:
            self.sy = 0
        self.remember_vars()

    def set_bg(self):
        # ev_mode_bg_evidence_pressed
        defbg = self.assets.variables["ev_mode_bg_evidence"]
        bg = self.assets.variables.get("ev_mode_bg_"+self.item_set, defbg)
        self.load(bg+self.assets.appendgba)

    def k_left(self):
        if self.page >= len(self.pages):
            return
        self.set_bg()
        self.back = False
        self.back_button.unhighlight()
        self.sx -= 1
        if self.mode == "overview":
            if self.sx < 0:
                self.page_prev()
            page = self.pages[self.page]
            if self.sy > len(page)-1:
                self.sy = 0
            while self.sx > len(page[self.sy])-1 and self.sx > 0:
                self.sx -= 1
            subscript("sound_court_record_scroll")
        elif self.mode == "zoomed":
            if self.sx < 0 and self.sy > 0:
                self.sx = 3
                self.sy -= 1
            if self.sx < 0 and self.sy == 0:
                self.page_prev()
            self.lastchoose = self.chosen_icon
            scroll = 0
            for p in self.pages:
                for line in p:
                    for icon in line:
                        scroll += 1
            if scroll > 1:
                self.scroll = constants.STANDARD_SW
                self.scroll_dir = 1
            # 
            subscript("sound_court_record_scroll_zoomed")
        self.choose()

    def k_right(self):
        if self.page >= len(self.pages):
            return
        self.set_bg()
        self.back = False
        self.back_button.unhighlight()
        self.sx += 1
        if self.mode == "overview":
            page = self.pages[self.page]
            if self.sy > len(page)-1 or self.sx > len(page[self.sy])-1:
                self.page += 1
                if self.page > len(self.pages)-1:
                    self.page = 0
                self.sx = 0
            page = self.pages[self.page]
            if self.sy > len(page)-1:
                self.sy = 0
            subscript("sound_court_record_scroll")
        elif self.mode == "zoomed":
            page = self.pages[self.page]
            if self.sx > len(page[int(self.sy)])-1:
                self.sy += 1
                self.sx = 0
            if self.sy > len(page)-1:
                self.sx = 0
                self.sy = 0
                self.page += 1
                if self.page >= len(self.pages):
                    self.page = 0
            self.lastchoose = self.chosen_icon
            scroll = 0
            for p in self.pages:
                for line in p:
                    for icon in line:
                        scroll += 1
            if scroll > 1:
                self.scroll = constants.STANDARD_SW
                self.scroll_dir = -1
            # 
            subscript("sound_court_record_scroll_zoomed")
        self.choose()

    def k_up(self):
        self.set_bg()
        if self.mode == "overview":
            if self.sy == 0:
                self.switch = True
            elif self.sy == 1:
                self.sy = 0
            if self.back:
                self.sy = 1
            if self.page >= len(self.pages) or self.sy > len(self.pages[self.page])-1:
                self.sy = 0
            # 
            subscript("sound_court_record_scroll")
        elif not self.back:
            self.switch = True
        self.back = False
        self.back_button.unhighlight()
        self.choose()

    def k_down(self):
        self.set_bg()
        if self.mode == "overview":
            self.back = False
            self.back_button.unhighlight()
            if not self.switch:
                self.sy += 1
            self.switch = False
            if self.page >= len(self.pages) or self.sy >= len(self.pages[self.page]) and self.canback():
                self.back = True
                self.back_button.highlight()
            
            # 
            subscript("sound_court_record_scroll")
        elif self.mode == "zoomed":
            if self.switch == False and self.canback():
                self.back = True
                self.back_button.highlight()
            self.switch = False
        self.choose()

    def choose(self):
        if self.background_pressed == False:
            # print(False)
            itback = self.assets.variables.get("ev_mode_bg_"+self.item_set, None)
        else:
            # print(True)
            itback = self.assets.variables.get(
                "ev_mode_bg_"+self.item_set+"_pressed", None)
        # print(itback)
        if not itback:
            itback = self.assets.variables.get("ev_mode_bg_evidence", None)
        if self.back and self.canback():
            self.load(itback+"_back"+self.assets.appendgba)
        elif self.switch:
            self.load(itback+"_profile"+self.assets.appendgba)
        else:
            self.load(itback+self.assets.appendgba)
        # print(itback)

        if self.back or self.switch or self.page >= len(self.pages):
            pass
        else:
            # print(self.sy)
            # print(self.sx)
            if self.sy >= len(self.pages[self.page]):
                self.sy = 0
            if self.sx > len(self.pages[self.page][int(self.sy)]):
                self.sx = len(self.pages[self.page][int(self.sy)])-1
            page = self.pages[self.page]
            row = page[int(self.sy)]
            if self.sx >= len(row):
                self.sx = len(row)-1
            # TODO: court record bug. will loop the same 2 items in a later page
            # over and over instead of switching pagesz
            col = row[int(self.sx)]
            self.chosen = col.id
            self.chosen_icon = col
        self.remember_vars()

    def enter_down(self):
        print("enter_down")
        if self.switch:
            self.k_z()
        elif self.back and self.canback():
            print("switch 2")
            # 
            subscript("sound_court_record_cancel")
            if self.mode == "overview":
                #self.assets.variables["_selected"] = self.chosen
                #self.assets.get_stack_top().cross = "presenting"
                # self.delete()
                for o in self.assets.get_stack_top().objects:
                    if isinstance(o, textbox):
                        self.assets.get_stack_top().refresh_arrows(o)
                        # o.delete()
                        # o.repeat()
                        # o.init_gui()
                        # print(dir(o))
                    # if isinstance(o,uglyarrow):
                        #u = uglyarrow()
                        # self.assets.get_stack_top().objects.add_object(u,True)
                        # pass
                        # o.delete()
                        # print(dir(uglyarrow))
                #self.assets.get_stack_top().goto_result((self.chosen+" "+self.assets.get_stack_top().statement).strip(),backup=self.fail)
                # pass
                self.delete()  # *!*
            elif self.mode == "zoomed":
                if self.assets.gbamode:
                    self.delete()
                else:
                    self.mode = "overview"
            elif self.mode == "check":
                self.mode = "overview"
        else:
            if self.mode == "overview":
                print("switch 5")
                self.mode = "zoomed"
                subscript("sound_court_record_zoom")
            elif self.mode == "zoomed":
                self.do_check()

    def do_check(self):
        if not self.chosen:
            return
        self.assets.variables["_selected"] = self.chosen
        chk = self.assets.variables.get(self.chosen+"_check", None)
        if chk:
            self.assets.addscene(chk)
            # 
            subscript("sound_court_record_check")

    def can_present(self):
        if not vtrue(self.assets.variables.get("_"+self.item_set+"_present", "true")):
            # print "_"+self.item_set+"_present","is false"
            return
        if not self.chosen:
            # print "nothing chosen"
            return
        if self.back or self.switch:
            # print "have back or switch",self.back,self.switch
            return
        if not self.assets.get_stack_top().cross == "proceed":
            # print "no cur_script.cross"
            return
        if not vtrue(self.assets.variables.get("_allow_present_"+self.item_set, "true")):
            # print "_allow_present_"+self.item_set,"is false"
            return
        if not vtrue(self.assets.variables.get(self.chosen+"_presentable", "true")):
            # print self.chosen+"_presentable","is false"
            return
        return True

    def k_x(self):
        if not self.can_present():
            return
        self.assets.variables["_selected"] = self.chosen
        self.assets.get_stack_top().cross = "presenting"
        self.delete()
        for o in self.assets.get_stack_top().objects:
            if isinstance(o, textbox):
                o.delete()
            if isinstance(o, uglyarrow):
                o.delete()
        self.assets.get_stack_top().goto_result(
            (self.chosen+" "+self.assets.get_stack_top().statement).strip(), backup=self.fail)

    def next_screen(self):
        if len(self.pages_set) == 1:
            return ""
        cur = self.pages_set.index(self.item_set)
        cur += 1
        if cur >= len(self.pages_set):
            cur = 0
        return self.pages_set[cur]

    def k_z(self):
        if len(self.pages_set) == 1:
            return
        self.chosen = None
        self.item_set = self.next_screen()
        self.layout()
        self.remember_vars()
        #print("page switch")
        #if not self.pages: self.item_set = modes[self.item_set]
        # self.layout()
        self.switch = False
        # 
        subscript("sound_court_record_switch")

    def k_space(self):
        # 
        subscript("sound_court_record_cancel")
        if self.mode == "zoomed":
            if self.assets.gbamode:
                self.delete()
            else:
                self.mode = "overview"
        elif self.canback():
            self.delete()
        #self.assets.get_stack_top().cross = ""
        #self.assets.get_stack_top().instatement = False

    def canback(self):
        show_back = vtrue(self.assets.variables.get(
            "_cr_back_button", "true")) and not getattr(self, "noback", False)
        if self.mode != "overview" or show_back:
            return True
        return False

    def draw(self, dest):
        if self.assets.gbamode:
            self.mode = "zoomed"
        pos = self.getpos()
        dest.blit(self.img, pos)
        x, y = pos
        if not self.assets.gbamode:
            if vtrue(self.assets.variables["ev_show_mode_text"]):
                dest.blit(self.assets.get_text_renderer("itemset").render(self.item_set.capitalize(), [255, 255, 255]),
                          [x+int(self.assets.variables["ev_mode_x"]), y+int(self.assets.variables["ev_mode_y"])])
        name = ""
        if self.chosen:
            name = self.assets.variables.get(
                self.chosen+"_name", self.chosen).replace("$", "")
        if not self.assets.gbamode or self.mode != "zoomed":
            dest.blit(self.assets.get_text_renderer("itemname").render(name, [255, 255, 255]),
                      [x+int(self.assets.variables["ev_currentname_x"]), y+int(self.assets.variables["ev_currentname_y"])])
        if vtrue(self.assets.variables.get("_evidence_enabled", "true")) and vtrue(self.assets.variables.get("_profiles_enabled", "true")):
            #FIXME: here itemset is a tuple. Why? Isn't it just supposed to return the Surface and be done with it?
            itemset = self.assets.get_font("itemset_big").render(self.next_screen().capitalize(), [255, 255, 255])
            dest.blit(itemset[0],  
                [x+int(self.assets.variables["ev_modebutton_x"]), 
                y+int(self.assets.variables["ev_modebutton_y"])])
        page = []
        if self.pages:
            page = self.pages[self.page]
        if self.mode != "zoomed":
            cx, cy = 0, 0
            sx = pos[0]+int(self.assets.variables["ev_items_x"])
            sy = pos[1]+int(self.assets.variables["ev_items_y"])
            x, y = sx, sy
            w = int(self.assets.variables["ev_spacing_x"])
            h = int(self.assets.variables["ev_spacing_y"])
            for line in page:
                for icon in line:
                    icon.reload()
                    if not icon.small:
                        continue
                    dest.blit(icon.small, [x, y])
                    if not self.back and not self.switch and self.sx == cx and self.sy == cy:
                        dest.blit(self.cursor, [x-1, y-1])  # -3 -3
                    x += w
                    cx += 1
                x = sx
                cx = 0
                y += h
                cy += 1
            if len(self.pages) > 1:
                arr = self.assets.open_art(self.assets.variables["ev_arrow_img"])[0]
                dest.blit(arr, [pos[0]+int(self.assets.variables["ev_rarrow_x"]),
                                pos[1]+int(self.assets.variables["ev_rarrow_y"])])
                dest.blit(pygame.transform.flip(arr, 1, 0),
                          [pos[0]+int(self.assets.variables["ev_larrow_x"]),
                           pos[1]+int(self.assets.variables["ev_larrow_y"])])
        if self.mode == "zoomed":
            showarrow = 0
            for p in self.pages:
                for line in p:
                    for icon in line:
                        showarrow += 1
            if showarrow > 1:
                if not getattr(self, "arr", None):
                    self.arr = self.assets.open_art(
                        self.assets.variables["ev_zarrow_img"])[0]
                dest.blit(self.arr, [pos[0]+int(self.assets.variables["ev_zrarrow_x"]),
                                     pos[1]+int(self.assets.variables["ev_zrarrow_y"])])
                dest.blit(pygame.transform.flip(self.arr, 1, 0),
                          [pos[0]+int(self.assets.variables["ev_zlarrow_x"]),
                           pos[1]+int(self.assets.variables["ev_zlarrow_y"])])
            if getattr(self, "chosen_icon", None) and getattr(self, "chosen", None):
                if self.scroll:
                    self.scroll -= 16*self.assets.dt
                    if self.scroll < 0:
                        self.scroll = 0
                    self.draw_ev_zoom(self.lastchoose, [
                                      (constants.STANDARD_SW-self.scroll+pos[0])*self.scroll_dir, pos[1]], dest)
                    self.draw_ev_zoom(self.chosen_icon, [
                                      constants.STANDARD_SW*(-self.scroll_dir)+(constants.STANDARD_SW-self.scroll+pos[0])*self.scroll_dir, pos[1]], dest)
                else:
                    self.draw_ev_zoom(self.chosen_icon, pos[:], dest)
                chk = self.assets.variables.get(self.chosen+"_check", None)
                if chk:
                    check = self.assets.open_art(
                        self.assets.variables["ev_check_img"]+self.assets.appendgba)[0]
                    dest.blit(
                        check, [pos[0]+self.assets.sw-check.get_width(), pos[1]+self.assets.sh-check.get_height()])
            else:
                self.mode = "overview"
        if self.canback():
            self.back_button.draw(dest)

    def draw_ev_zoom(self, icon, pos, surf):
        if not hasattr(self, icon.id+"_zoom_"+str(self.assets.gbamode)):
            if self.assets.gbamode:
                back_pos = [28, 62]
                tbpos = [106, 78]
                tbsize = [120, 50]
            else:
                back_pos = [int(self.assets.variables["ev_z_icon_x"]),
                            int(self.assets.variables["ev_z_icon_y"])]
                tbpos = [int(self.assets.variables["ev_z_textbox_x"]),
                         int(self.assets.variables["ev_z_textbox_y"])]
                tbsize = [int(self.assets.variables["ev_z_textbox_w"]),
                          int(self.assets.variables["ev_z_textbox_h"])]
            newsurf = self.assets.Surface([self.assets.sw, self.assets.sh]).convert_alpha()
            newsurf.fill([0, 0, 0, 0])
            if self.assets.gbamode:
                pos[1] -= 10
            newsurf.blit(self.ev_zoom, [int(self.assets.variables["ev_z_bg_x"]),
                                        int(self.assets.variables["ev_z_bg_y"])])
            name = icon.name
            if self.assets.gbamode:
                newsurf.blit(self.assets.get_text_renderer("itemname").render(
                    name, [255, 255, 0])[0], [103, 65])
            tb = textblock(self.assets, icon.desc, [tbpos[0], tbpos[1]], tbsize, color_str(
                self.assets.variables,
                self.assets.variables["ev_z_text_col"]))
            newsurf.blit(icon.scaled, [back_pos[0], back_pos[1]])
            tb.pos = [tbpos[0], tbpos[1]]
            tb.draw(newsurf)
            setattr(self, icon.id+"_zoom_"+str(self.assets.gbamode), newsurf)
        else:
            newsurf = getattr(self, icon.id+"_zoom_"+str(self.assets.gbamode))
        surf.blit(newsurf, pos)

class textblock(sprite):
    autoclear = True

    def __init__(self, assets, text="", pos=[0, 0], size=[100, 100], color=[255, 255, 255], surf=None):
        super(textblock, self).__init__(assets)
        self.text = text
        self.lines = [x.split(" ") for x in text.split("{n}")]
        self.pos = pos
        self.size = size
        self.surf = surf
        self.color = color
        self.width, self.height = self.size

    def update(self):
        pass

    def draw(self, dest):
        x = self.pos[0]
        y = self.pos[1]
        i = 0
        for line in self.lines:
            for word in line:
                nl = False
                if word.strip():
                    """
                    TODO: another potential candidate for the game text
                    """
                    wordi = self.assets.get_font("block").render(
                        word, self.color)[0]  # 1,
                else:
                    wordi = pygame.Surface([4, 10]).convert_alpha()
                    wordi.fill([0, 0, 0, 0])
                if wordi.get_width()+x > self.pos[0]+self.size[0] or nl:
                    x = self.pos[0]
                    y += 10
                if y > self.pos[1]+self.size[1]:
                    break
                dest.blit(wordi, [x, y])
                x += wordi.get_width()+4
                i += 1
            x = self.pos[0]
            y += 10

class waitenter(sprite):
    def __init__(self, assets):
        super(waitenter, self).__init__(assets)
        self.pri = sorting.get_u_layer(self.__class__.__name__)

    def draw(self, dest): pass

    def update(self):
        return True

    def enter_down(self):
        self.delete()

class delay(sprite):
    def __init__(self, assets, ticks=1):
        super(delay, self).__init__(assets)
        self.visible = False
        self.ticks = abs(ticks)
        self.pri = sorting.get_u_layer(self.__class__.__name__)

    def draw(self, dest): pass

    def update(self):
        if self.ticks <= 0:
            self.delete()
            return False
        self.ticks -= self.assets.dt
        return True

class timer(sprite):
    def __init__(self, assets, ticks=1, run=None):
        sprite.__init__(self, assets)
        self.ticks = abs(ticks)
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.run = run
        #self.script = self.assets.get_stack_top()

    def update(self):
        self.ticks -= self.assets.dt
        self.assets.variables["_timer_value_"+self.run] = str(self.ticks)
        if self.ticks <= 0:
            self.delete()
            if self.run:
                # 
                subscript(self.run)

from core.effects import *

class guiBack(sprite, gui.widget):
    def click_down_over(self, mp):
        self.k_space()

    def __init__(self, assets, image=None, x=None, y=None, z=None, name=None):
        sprite.__init__(self, assets)
        gui.widget.__init__(self)
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        if not image:
            image = "general/back"
        self.image = image
        self.unhighlight()
        self.pos = [0, self.assets.sh+self.assets.sh-self.img.get_height()]
        if x is not None:
            self.pos[0] = x
        if y is not None:
            self.pos[1] = y
        if z is not None:
            self.z = z
        if name is not None:
            self.id_name = name
        gui.widget.__init__(self, self.pos, self.img.get_size())
        self.screen_setting = "try_bottom"

    def highlight(self):
        self.load(self.image+"_high"+self.assets.appendgba)

    def unhighlight(self):
        self.load(self.image+self.assets.appendgba)

    def k_space(self):
        self.delete()
        # 
        subscript("sound_back_button_cancel")

    def update(self):
        return True

class guiScroll(sprite, gui.widget):
    def click_down_over(self, mp):
        self.k_z()

    def __init__(self, assets, direction):
        sprite.__init__(self, assets, flipx=direction+1)
        gui.widget.__init__(self)
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.load("general/examine_scroll")
        self.pos = [self.assets.sw//2-self.img.get_width()//2, self.assets.sh *
                    2-self.img.get_height()]
        gui.widget.__init__(self, self.pos, self.img.get_size())
        self.direction = direction
        self.screen_setting = "try_bottom"

    def k_z(self):
        self.delete()
        self.parent.xscrolling = self.direction*self.assets.sw
        # 
        subscript("sound_examine_scroll")
        #del self.parent.scrollbut

    def update(self):
        return False

class guiWait(sprite):
    id_name = "_guiWait_"

    def __init__(self, assets, run=None, mute=False):
        sprite.__init__(self, assets)
        gui.widget.__init__(self)
        self.width = 0
        self.height = 0
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.pos = [0, 0]
        self.run = run
        self.script = self.assets.get_stack_top()
        self.mute = mute  # Mute sound while waiting
        if self.mute:
            self.assets.pause_sound()

    def delete(self):
        self.kill = 1
        if self.mute:
            self.assets.resume_sound()

    def update(self):
        if self.run:
            ns = self.script.execute_macro(self.run)
        return True

class error_msg(gui.pane):
    def __repr__(self):
        return self.msg

    def delete(self):
        self.kill = 1

    def click_down_over(self, mp):
        self.delete()

    def __init__(self, assets, msg, line, lineno, script):
        self.assets = assets
        self.id_name = "_error_msg_"
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        self.z = sorting.get_z_layer(self.__class__.__name__)
        gui.pane.__init__(self)
        msg += "\nscene:'"+str(script.scene)+"', line '"+str(lineno)+"'"
        msg += "\ncurrent game:"+ assets.game_folder
        self.msg = msg
        msg_lines = [""]
        for c in msg:
            msg_lines[-1] += c
            if (len(msg_lines[-1]) > 35 or c == "\n") and msg_lines[-1].strip():
                if len(msg_lines[-1]) > 35:
                    msg_lines[-1] += " - "
                msg_lines.append("")
        msg_lines.append("       Click to continue  ")
        for msg_line in msg_lines:
            msg = gui.editbox(None, msg_line)
            msg.draw(assets.Surface([64, 64]))
            msg.draw_back = False

            def click_down_over(self, *args):
                pass
            msg.click_down_over = click_down_over
            msg.event = click_down_over
            msg.width = 0
            self.children.append(msg)
        self.lineno = lineno
        self.script = script
        self.width = constants.STANDARD_SW
        self.height = len(msg_lines)*20
        if vtrue(assets.variables.get("_production", "false")):
            self.delete()

    def update(self):
        return True

class SoundEvent(object):
    kill = 0
    pri = -1000000

    def __init__(self, assets, name, after=0, volume=1.0):
        self.assets = assets
        self.name = name
        self.wait = after
        self.volume = volume
        self.z = sorting.get_z_layer(self.__class__.__name__)

    def delete(self):
        self.kill = 1

    def update(self):
        self.wait -= self.assets.dt
        if self.wait <= 0:
            try:
                self.assets.play_sound(self.name, volume=self.volume)
            except art_error as e:
                self.delete()
                self.assets.handle_error(e)
        self.delete()
        return False

    def draw(self, *args):
        pass

class movie:
    pass