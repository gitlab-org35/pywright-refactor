class Sprite_Metadata:
    def __init__(self, text=None):
        self.horizontal = 1
        self.vertical = 1
        self.length = 1
        self.loops = False
        self.sounds = {}
        self.split = None
        self.framecompress = [0, 0]
        self.blinkmode = "blinknoset"
        self.offsetx = 0
        self.offsety = 0
        self.blipsound = None
        self.frameoffset = {}
        self.delays = {}
        self.speed = 6
        self.blinkspeed = [100, 200]
        if(text):
            self.load_from(text)

    def __set_frame_compress(self, fc):
            if(len(fc)) == 1:
                self.framecompress = [0, int(fc[0])]
            else:
                self.framecompress = [int(x) for x in fc]

    def __get_metadata_from_line(self, l):
        split_lines = l.split(" ")
        command = split_lines[0]
        args = split_lines[1:]

        #TODO: avoid setting own attributes by name
        attribute_names = ["horizontal", "vertical", "length", "loops",
                           "sfx", "offsetx", "offsety"]
        for attribute_name in attribute_names:
            if(command == attribute_name):
                self.__setattr__(attribute_name, int(split_lines[1]))

        if (command == "framecompress"):
            fc = l.replace("framecompress ", "").split(",")
            self.__set_frame_compress(fc)

        if (command == "blinksplit"):
            self.split = int(l.replace("blinksplit ", ""))
        if (command == "blinkmode"):
            self.blinkmode = l.replace("blinkmode ", "")
        if (command == "blipsound"):
            self.blipsound = l.replace("blipsound ", "").strip()
        if (command == "framedelay"):
            frame, delay = args
            self.delays[int(frame)] = int(delay)

        if (command == "globaldelay"):
            speed = (l.split(" ", 1)[1])
            self.speed = float(speed)
        if (command == "blinkspeed"):
            self.blinkspeed = [int(x) for x in args]

    def load_from(self, text):
        """
        Parses a text file to obtain metadata for sprites.
        Large list of ifs may be turned into a collection.
        """
        text = text.decode("utf8", "ignore")
        text = text.replace('\ufeff', '')
        lines = text.replace("\r\n", "\n").split("\n")
        setlength = False
        for l in lines:
            self.__get_metadata_from_line(l)
            if (l.split(" ")[0] == "length"):
                setlength = True

        if not setlength:
            self.length = self.horizontal*self.vertical
        return self