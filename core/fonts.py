font_list = """set _font_tb pwinternational.ttf
set _font_update Vera.ttf
set _font_update_size 8
set _font_tb_size 10
set _font_block arial.ttf
set _font_block_size 10
set _font_nt arial.ttf
set _font_nt_size 10
set _font_list pwinternational.ttf
set _font_list_size 10
set _font_itemset pwinternational.ttf
set _font_itemset_size 10
set _font_itemset_big arial.ttf
set _font_itemset_big_size 14
set _font_itemname pwinternational.ttf
set _font_itemname_size 10
set _font_loading arial.ttf
set _font_loading_size 16
set _font_gametitle arial.ttf
set _font_gametitle_size 16
set _font_new_resume arial.ttf
set _font_new_resume_size 14"""

def get_fonts():
    fonts = {}
    for line in font_list.split("\n"):
        args = line.split(" ")
        fonts[args[1]] = args[2]
    return fonts

