
from core.pwvlib import *
from core.new_sorting import sorting
from core import textutil
from core.errors import *
from core import gui
from core.vtrue import vtrue
from core.engine.scriptinterpreter import subscript
from core.color_str import color_str
from core import constants
import random
TextRenderer = textutil.TextRenderer
TEXTBOX_FLAG = "tb"

class textbox(gui.widget):
    pri = 30

    def click_down_over(self, pos):
        if self.for_list == False:
            if not hasattr(self, "rpos1"):
                return
            if getattr(self, "hidden", 0):
                return
            if self.statement:
                if pos[1] >= self.rpos[1] and pos[1] <= self.rpos1[1]+self.height1:
                    if pos[0] >= self.rpos1[0] and pos[0] <= self.rpos1[0]+self.width/2:
                        self.k_left()
                    if pos[0] >= self.rpos1[0]+self.width/2 and pos[0] <= self.rpos1[0]+self.width:
                        self.k_right()
            if pos[0] >= self.rpos1[0] and pos[0] <= self.rpos1[0]+self.width1 and pos[1] >= self.rpos1[1] and pos[1] <= self.rpos1[1]+self.height1:
                self.enter_down()

    def _get_lines_from_markup_text(self, text):
        text = textutil.markup_text(text)
        text.m_replace(lambda c: hasattr(c, "variable"),
                       lambda c: self.assets.variables[c.variable])
        return text.fulltext().split("\n")

    def _create_pages(self, text, local_sw, renderer, wrap, wrap_avoid_controlled):
        lines = self._get_lines_from_markup_text(text)
        if wrap_avoid_controlled:
            if len(lines) > 1:
                wrap = False
        lines = textutil.wrap_text(lines, renderer, local_sw-6, wrap)
        return [lines[i:i+3] for i in range(0, len(lines), 3)]

    def set_text(self, text):
        wrap = vtrue(self.assets.variables.get("_textbox_wrap", "true"))
        wrap_avoid_controlled = vtrue(
            self.assets.variables.get("_textbox_wrap_avoid_controlled", "true"))
        local_sw = self.assets.sw
        renderer = self.renderer

        local_pages = self._create_pages(text, local_sw, renderer, wrap, wrap_avoid_controlled)
        local_text = "\n"
        local_markup = textutil.markup_text("")
        for page in local_pages:
            for line in page:
                local_text += line.fulltext()
                local_text += "\n"
                local_markup._text.extend(line.chars())
                local_markup._text.append("\n")

        self.pages = local_pages
        self._text = local_text
        self._markup = local_markup


    text = property(lambda self: self._text, set_text)

    def __init__(self, assets, text="", color=constants.WHITE_LIST, delay=2, speed=1, rightp=True, leftp=False, nametag="\n"):
        self.nametag = nametag
        self.assets = assets
        self.renderer = self.assets.get_text_renderer(TEXTBOX_FLAG)
        gui.widget.__init__(self, [0, 0], [self.assets.sw, self.assets.sh])
        self.z = sorting.get_z_layer(self.__class__.__name__)
        nametag = self.nametag
        if self.assets.portrait:
            nametag = getattr(self.assets.portrait, "nametag", nametag) or nametag
        self.nt_full = None
        self.nt_left = None
        self.nt_text_image = None
        self.base = self.assets.open_art(self.assets.variables.get(
            "_textbox_bg", "general/textbox_2"))[0].convert_alpha()
        nt_full_image = self.assets.variables.get("_nt_image", "")
        if nt_full_image:
            self.nt_full = self.assets.open_art(nt_full_image)[0].convert_alpha()
        elif nametag.strip():
            self.nt_left = self.assets.open_art(
                "general/nt_left")[0].convert_alpha()
            self.nt_middle = self.assets.open_art(
                "general/nt_middle")[0].convert_alpha()
            self.nt_right = self.assets.open_art(
                "general/nt_right")[0].convert_alpha()
        self.nametag = nametag
        self.img = self.base.copy()
        self.go = 0
        self.for_list = False
        self.text = text
        self.mwritten = []
        self.num_lines = 4
        self.next = self.num_lines
        self.color = color
        self.delay = delay
        self.speed = speed
        self.next_char = 0
        self.in_paren = 0  # Record whether we are inside parenthesis or not
        # Show pointer left and right
        self.rightp = rightp
        self.leftp = leftp
        from core.gui_elements import fg
        self.rpi = fg(self.assets, "pointer")
        self.kill = False
        self.statement = None
        self.wait = "auto"

        self.can_skip = True
        self.blocking = not vtrue(
            self.assets.variables.get("_textbox_skipupdate", "0"))

        self.made_gui = False

        self.id_name = "_textbox_"
        self.is_cross = False

    def init_cross(self):
        self.is_cross = True

    """
    Nota: es el primer metodo donde se llama a la funcion subscript
    """
    def init_normal(self):

        # 
        subscript("show_court_record_button")

    def init_gui(self):
        if not self.made_gui:
            self.made_gui = True
            if self.statement:
                self.init_cross()
            else:
                self.init_normal()

    def delete(self):
        # 
        self.kill = 1
        subscript("hide_court_record_button")
        subscript("hide_press_button")
        subscript("hide_present_button")
        if vtrue(self.assets.variables.get("_tb_on", "off")):
            subscript("tboff")
        self.assets.get_stack_top().refresh_arrows(self)
        self.assets.get_stack_top().build_mode = True

    def gsound(self):
        """
        Some characters can have diferent blip sounds.
        If there is a portrait, use the character's 'blip' sound
        when typing their dialogue.
        """
        if hasattr(self, "_clicksound"):
            return self._clicksound
        if self.assets.portrait:
            return self.assets.portrait.clicksound
        return "blipmale.ogg"

    def ssound(self, v):
        self._clicksound = v
    clicksound = property(gsound, ssound)

    def can_continue(self):
        """If not blocking, player cannot make text continue
        If skip mode is on (_debug mode) we can just skip the text
        Otherwise, check to see if all the text has been written out"""
        if not self.blocking:
            return
        if self.can_skip or self.nextline():
            return True

    def enter_down(self):
        if not self.can_continue():
            return
        if not self.nextline():
            while not self.nextline():
                try:
                    self.add_character()
                except markup_error as e:
                    self.assets.handle_error(e)
        else:
            self.forward()

    def k_left(self):
        if self.statement and self.nextline():
            self.assets.get_stack_top().prev_statement()
            self.forward()

    def k_right(self):
        if self.statement and self.nextline():
            self.forward()

    def k_z(self):
        if self.statement and self.nextline():
            self.assets.get_stack_top().cross = "pressed"
            self.assets.get_stack_top().clear_arrows()
            self.assets.get_stack_top().goto_result(
                "press "+self.statement, backup=self.assets.variables.get("_court_fail_label", None))
            self.delete()

    def k_x(self):
        if self.statement and self.nextline():
            em = self.assets.addevmenu()
            em.fail = self.assets.variables.get("_court_fail_label", None)
            self.assets.get_stack_top().clear_arrows()

    def forward(self, sound=True):
        """Set last written text to the contents of the textbox
        turn off testimony blinking
        scroll to next 3 lines of text if they exist
        if there is no more text, delete textbox
        play the bloop sound"""
        t = textutil.markup_text()
        t._text = self.mwritten
        self.assets.variables["_last_written_text"] = t.fulltext()
        lines = self.text.split("\n")
        lines = lines[4:]
        self.set_text("\n".join(lines))
        self.mwritten = []
        self.next = self.num_lines
        self.img = self.base.copy()
        if not self.text.strip():
            self.delete()
        if sound:
            self.assets.play_sound("bloop.ogg", volume=0.7)

    def draw(self, dest):
        self.children = []
        if not self.go or self.kill:
            return
        # For the widget
        x = self.assets.variables.get("_textbox_x")
        y = self.assets.variables.get("_textbox_y")
        if(x and y):
            self.rpos1 = [
                int(self.assets.variables.get("_textbox_x")),
                int(self.assets.variables.get("_textbox_y"))]
        else:
            self.rpos1 = [
                (self.assets.sw - self.img.get_width()) / 2,
                (self.assets.sh - self.img.get_height())]
        self.width1 = self.img.get_width()
        self.height1 = self.img.get_height()

        #NOTE: This prints the textbox itself!
        dest.blit(self.img, self.rpos1)
        if self.rightp and self.nextline():
            dest.blit(self.rpi.img, [self.rpos1[0]+self.width1-16,
                                     self.rpos1[1]+self.height1-16])
            pass
        if getattr(self, "showleft", False) and self.nextline():
            import pygame
            dest.blit(pygame.transform.flip(self.rpi.img, 1, 0), [self.rpos1[0],
                                                                  self.rpos1[1]+self.height1-16])
            pass
        # This code prints the nametag (nt) text.
        x = self.assets.variables.get("_nt_x", "")
        y = self.assets.variables.get("_nt_y", "")
        if self.nt_full:
            nx, ny = self.rpos1[0], (self.rpos1[1]-self.nt_full.get_height())
            if x != "":
                nx = int(x)
            if y != "":
                ny = int(y)
            dest.blit(self.nt_full, [nx, ny])
            if self.nt_text_image:
                if self.assets.variables.get("_nt_text_x", "") != "":
                    nx += int(self.assets.variables.get("_nt_text_x", 0))
                if self.assets.variables.get("_nt_text_y", "") != "":
                    ny += int(self.assets.variables.get("_nt_text_y", 0))
                dest.blit(self.nt_text_image, [nx+5, ny]) #nametag printing
        elif self.nt_left and self.nt_text_image:
            nx, ny = self.rpos1[0], (self.rpos1[1]-self.nt_left.get_height())
            if x != "":
                nx = int(x)
            if y != "":
                ny = int(y)
            dest.blit(self.nt_left, [nx, ny])
            for ii in range(self.nt_text_image.get_width()+8):
                dest.blit(self.nt_middle, [nx+3+ii, ny])
            dest.blit(self.nt_right, [nx+3+ii+1, ny])
            if self.assets.variables.get("_nt_text_x", "") != "":
                nx += int(self.assets.variables.get("_nt_text_x", 0))
            if self.assets.variables.get("_nt_text_y", "") != "":
                ny += int(self.assets.variables.get("_nt_text_y", 0))
            dest.blit(self.nt_text_image, [nx+5, ny]) #nametag printing

    def _set_clicksound(self, a_clicksound):
        '''
        FIXME: clicksound used to break silently when it couldnt find the sound file.
        This setter should check ONCE if the clicksound file exists, and
        set the clicksound as none if it couldnt be found. In the mean time,
        here are some popular 'fake clicksounds' i have seen case developers use.
        '''
        self.clicksound = a_clicksound

    def _interpret_command(self, command, args, char, next_char):
        commands = ["sfx", "sound", "delay", "spd", "_fullspeed", "_endfullspeed",
                                "wait", "center", "type", "next",
                                "tbon", "tboff",
                                "e", "f", "s", "p", "c"]
        commands.sort(key=lambda o: len(o))
        commands.reverse()
        for cm in commands:
            if command.startswith(cm):
                nargs = command.split(cm, 1)[1]
                if nargs and not nargs.startswith(" "):
                    command, args = cm, nargs
                break
        if command == "sfx":
            self.assets.play_sound(args)
        elif command == "sound":
            self._set_clicksound(args)
        elif command == "delay":
            self.delay = int(args)
            self.wait = "manual"
        elif command == "spd":
            self.speed = float(args)
        elif command == "_fullspeed":
            self.last_speed = self.speed
            self.speed = 0
        elif command == "_endfullspeed":
            self.speed = self.last_speed
        elif command == "wait":
            self.wait = args
        elif command == "center":
            pass
        elif command == "type":
            self._set_clicksound("typewriter.ogg")
            self.delay = 2
            self.wait = "manual"
        elif command == "next":
            if self.assets.portrait:
                self.assets.portrait.set_blinking()
            del self.mwritten[-1]
            self.forward(False)
            return 0
        elif command == "e":
            try:
                self.assets.set_emotion(args.strip())
            except:
                import traceback
                traceback.print_exc()
                raise markup_error("No character to apply emotion to")
        elif command == "f":
            self.assets.flash = 3
            self.assets.flashcolor = constants.WHITE_LIST
            command = args.split(" ")
            if len(command) > 0 and command[0]:
                self.assets.flash = int(command[0])
            if len(command) > 1:
                self.assets.flashcolor = color_str(self.assets.variables, command[1])
        elif command == "s":
            self.assets.shakeargs = [
                x for x in args.split(" ") if x.strip()]
        elif command == "p":
            next_char = int(args.strip())
        elif command == "c":
            pass
        elif command == "tbon":
            self.assets.get_stack_top().tbon()
        elif command == "tboff":
            self.assets.get_stack_top().tboff()
        else:
            raise markup_error(
                "No macro or markup command valid for:"+command)
        return next_char
    def add_character(self):
        command = None
        next_char = 1
        char = self._markup._text[len(self.mwritten)]
        self.mwritten.append(char)
        if isinstance(char, textutil.markup_command):
            try:
                command, args = char.command, char.args
                if self.assets.get_stack_top().macros.get(command, None):
                    print("RUNNING A MACRO")
                    self.assets.variables["_return"] = ""
                    this = self.assets.get_stack_top()
                    ns = self.assets.get_stack_top().execute_macro(command, args)
                    old = ns._endscript
                    s = len(self.mwritten)-1
                    mt = self._markup._text
                    self._markup._text = self.mwritten

                    def back(*args):
                        old()
                        t0 = []
                        t1 = []
                        for i, c in enumerate(mt):
                            if i < s and not isinstance(c, textutil.markup_command):
                                t0.append(c)
                            if i > s:
                                t1.append(c)
                        t2 = [textutil.markup_command("_fullspeed", "")]+t0+[textutil.markup_command(
                            "_endfullspeed", "")]+list(self.assets.variables["_return"])+t1[:-1]
                        t = textutil.markup_text()
                        t._text = t2
                        self.set_text(t.fulltext())
                        self.mwritten = []
                        self.next_char = 0
                    ns._endscript = back
                else:
                    next_char=self._interpret_command(command, args, char, next_char)
            except (art_error, markup_error) as e:
                next_char = 1
                self.assets.handle_error(e) 
        elif isinstance(char, textutil.markup):
            pass
        else:
            if not hasattr(self, "_lc"):
                self._lc = ""
            self.go = 1
            if self._lc in [".?"] and char == " ":
                next_char = 6
            if self._lc in ["!"] and char == " ":
                next_char = 8
            if self._lc in [","] and char == " ":
                next_char = 4
            if self._lc in ["-"] and (char.isalpha() or char.isdigit()):
                next_char = 4
            if char in ["("]:
                self.in_paren = 1
            if char in [")"]:
                self.in_paren = 0
            if self.assets.portrait:
                punctuation = [x for x in self.assets.variables.get(
                    "_punctuation", ".,?!")]
                if not self.in_paren and not char in punctuation:
                    self.assets.portrait.set_talking()
                if self.in_paren:
                    self.assets.portrait.set_blinking()
            if str(char).strip() and self.clicksound:
                try:
                    volume=random.uniform(0.65, 1.0)
                    self.assets.play_sound(self.clicksound, volume=volume)
                except art_error as e:
                    self.assets.handle_error(e)

            next_char = int(next_char*self.delay)
            if self.wait == "manual":
                if char.strip():
                    next_char = 5*self.delay
                else:
                    next_char = 2
            self._lc = char
        return next_char

    def nextline(self):
        """Returns true if all the text waiting to be written into the textbox has been written"""
        t = textutil.markup_text()
        t._text = self.mwritten
        return not len(self.mwritten) < len(self._markup._text) or len(t.fulltext().split("\n")) >= self.num_lines

    @staticmethod
    def _number_of_lines(num_lines, lines):
        number_of_lines = 3
        #TODO: do without exception
        if num_lines == "auto":
            number_of_lines = 3
        else: 
            try:
                if (int(num_lines) > 0):
                    number_of_lines = int(num_lines)
            except:
                print("Failed number of lines:" +num_lines)
        return number_of_lines

    def _process_characters(self):
        while (not self.nextline()) and self.next_char <= 0:
            #self.next_char += 1
            num_chars = max(int(self.speed), 1)
            next_char = 0
            cnum = num_chars
            while (not self.nextline()) and ((not self.speed) or cnum > 0):
                cnum -= 1
                ac_next = self.add_character()
                if self.speed:
                    next_char += ac_next
                
            if self.speed:
                self.next_char += (next_char/float(self.speed))

    def _handle_title(self, line, color):
        # if there is something besides whitespace to print
        if line.strip():
            text_color = self.assets.variables.get("_nt_text_color", "")
            if text_color:
                text_color = color_str(self.assets.variables, text_color)
            else:
                text_color = color
            font = self.assets.get_font("nt")
            clean_line = line.replace("_", " ")
            text_image = font.render(clean_line, text_color)[0]  # 1,
            self.nt_text_image = text_image
        return False

    '''
        Actual line rendering

        color = TextRenderer.lastcolor
        if "{center}" in line:
            center = not center
        if center:
            x = (self.assets.sw-img.get_width())//2
        if x+img.get_width() > self.assets.sw:
            if not getattr(self, "OVERAGE", None) and self.assets.debug_mode:
                self.OVERAGE = x+img.get_width()-self.assets.sw
                raise offscreen_text(
                    'Text Overflow:"%s" over by %s' % (line, self.OVERAGE))
        
    '''
    def _handle_line(self, line, base_color, y, x, inc, center):
        if not center: center = ("{center}" in line)
        used_color = self.renderer.render_on_surf(
            self.img, [x,y], line, base_color, center)
        y += inc
        return y, x, inc, center, used_color

    def _process_lines(self, lines, number_of_lines):
        title = True
        y, x, inc = 6, 6, 18*(self.assets.sh/constants.STANDARD_SH)
        base_color = self.color
        center = False
        for i, line in enumerate(lines[:number_of_lines+1]):
            # if this is a title screen
            if title:
                title = self._handle_title(line, base_color)
            else:
                try:
                    y, x, inc, center, used_color = self._handle_line(
                        line, base_color, y, x, inc, center)
                    base_color = used_color
                except offscreen_text as e:
                    self.assets.handle_error(e)

    def _create_markup_text(self):
        t = textutil.markup_text()
        t._text = self.mwritten
        fulltext = t.fulltext().split("\n")
        nametag = self.nametag.replace("\n", "")
        return [nametag]+fulltext

    def update(self, dt=None):
        self.rpi.update()
        if self.kill:
            return
        if dt is None:
            dt = self.assets.dt
        self.next_char -= dt
        
        self._process_characters()
        
        if self.assets.portrait:
            if self.next_char > 10 or self.nextline():
                self.assets.portrait.set_blinking()
        title = True
        self.next = 0
        self.img = self.base.copy()
        
        lines = self._create_markup_text()
        var_lines = self.assets.variables.get("_textbox_lines", "auto")
        number_of_lines = self._number_of_lines(var_lines, lines)

        self._process_lines(lines, number_of_lines)
        
        self.next = self.num_lines

        # check whether we're printing cross-examination text.
        if self.is_cross and self.nextline():
            # 
            self.is_cross = False
            subscript("show_press_button")
            subscript("show_present_button")
        if self.blocking:
            return True
        return
