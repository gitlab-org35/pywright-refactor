from core.gui_elements import *
from core.constants import *

def _has_img(sprite):
    return hasattr(sprite, "img")

class portrait(sprite):
    autoclear = True

    def get_self_image(self):
        if _has_img(self): return self.get_current_sprite().img
    img = property(get_self_image)
    f = open(BLIPSOUNDS_FILE)
    bliplines = f.readlines()
    f.close()
    male = bliplines[1].strip().split(" ")
    female = bliplines[4].strip().split(" ")

    def __init__(self, assets, name=None, hide=False):
        self.talk_sprite = fadesprite(assets)
        self.blink_sprite = fadesprite(assets)
        self.combined = fadesprite(assets)
        self._width = 60 # TODO
        self._height = 60
        super(portrait, self).__init__(assets)
        self.init(assets, name, hide)
        pass

    def init_sounds(self):
        a_clicksound = None
        default = "blipmale.ogg"
        a_clicksound = self.assets.variables.get("char_defsound", default)
        if self.charname in self.female:
            a_clicksound = "blipfemale.ogg"
        if self.charname in self.male:
            a_clicksound = "blipmale.ogg"
        if hasattr(self, "talk_sprite"):
            if getattr(self.talk_sprite, "blipsound", None):
                a_clicksound = self.talk_sprite.blipsound
        if "char_"+self.charname+"_defsound" in self.assets.variables:
            a_clicksound = self.assets.variables["char_" +
                                               self.charname+"_defsound"]
        
        "FIXME: a hack for no sound"
        if a_clicksound == "/null":
            a_clicksound = default

        self.clicksound = a_clicksound

    @staticmethod
    def _paren(text):
        return "(" + text + ")"

    def _emoblink(self, filename : str):
        mode = ""
        for flag in [COMBINED, BLINK, TALK]:
            if filename.endswith(self._paren(flag)):
                filename, mode = filename.rsplit(self._paren(flag), 1)[0], flag
        return filename, mode

    def _init_basic(self, name, charname):
        #layers
        self.z = sorting.get_z_layer(self.__class__.__name__)
        self.pri = sorting.get_u_layer(self.__class__.__name__)
        #transform
        self.pos = [0, 0]
        self.rot = [0, 0, 0]
        #names
        self.name = name
        self.id_name = charname
        self.nametag = self.assets.variables.get(
            "char_"+charname+"_name", charname.capitalize())+"\n"

    def _set_sprite_from_combined(self, _surfs, _sprite, name):
        _sprite.load_base(_surfs)
        _sprite.name = name
        _sprite.loops = 1
        _sprite.delays = self.combined.delays
        _sprite.offsetx = self.combined.offsetx
        _sprite.offsety = self.combined.offsety

    def _try_store_asset(self, asset, store_into):
        '''
        True if we set loaded the asset on 'store_onto'
        '''
        if asset and not _has_img(store_into):
            store_into.load(asset.rsplit(ART_FOLDER, 1)[1][:-4])  # data/
            return True 
        return False

    def _handle_blink(self, resource):
        self._try_store_asset(resource, self.blink_sprite)

    def _handle_talk(self, resource):
        self._try_store_asset(resource, self.talk_sprite)

    def _handle_combined(self, resource):
        if self._try_store_asset(resource, self.combined):
            _split = self.combined.split if self.combined.split else len(self.combined.base)//2
            front = self.combined.base[:_split]
            back = self.combined.base[_split:]
            self._set_sprite_from_combined(front, self.talk_sprite, self.combined.name+"_"+TALK)
            self._set_sprite_from_combined(back, self.blink_sprite, self.combined.name+"_"+BLINK)
            self.blink_sprite.blinkmode = self.combined.blinkmode
            self.blink_sprite.blinkspeed = self.combined.blinkspeed
        
    def _handle_no_suffix(self, resource):
        if self._try_store_asset(resource, self.blink_sprite):
            if self.blink_sprite.blinkmode == "blinknoset":
                self.blink_sprite.blinkmode = "stop"

    def _loadfrom(self, path, emo, blinkemo):
        if not path.endswith("/"):
            path += "/"
        
        blink = self.assets.registry.lookup(path+blinkemo+self._paren(BLINK))
        talk = self.assets.registry.lookup(path+emo+self._paren(TALK))
        combined = self.assets.registry.lookup(path+emo+self._paren(COMBINED))
        available = self.assets.registry.lookup(path+emo)

        if not any([blink, talk, combined, available]):
            raise art_error("Character folder %s not found" % emo)

        self._handle_blink(blink)
        self._handle_talk(talk)
        self._handle_combined(combined)
        self._handle_no_suffix(available)

    def _blinkhandle(self, mode):
        if _has_img(self.talk_sprite) and not _has_img(self.blink_sprite):
            self.blink_sprite.img = _img = self.talk_sprite.img
            self.blink_sprite.base = [_img]
            self.blink_sprite.blinkmode = "stop"
        if _has_img(self.blink_sprite) and not _has_img(self.talk_sprite):
            self.talk_sprite.img = _img = self.blink_sprite.img
            self.talk_sprite.base = [_img]
        if _has_img(self.talk_sprite) and _has_img(self.blink_sprite):
            if mode == BLINK:
                self.set_blinking()
        else:
            raise art_error("Can't load character ("+mode+")")

        _port_spd = int(self.assets.get_frame_delay("port"))
        self.blink_sprite.loopmode = self.blink_sprite.blinkmode
        self.blink_sprite.spd = self.talk_sprite.spd = _port_spd
    
    def init(self, assets, name=None, hide=False, blinkname=None, init_basic=True):
        if not name:
            return self.init_sounds()
        charname, filename = name.split("/", 1)
        if init_basic:
            self._init_basic(name, charname)
        emo, mode = self._emoblink(filename)
        self.charname = charname
        self.emoname = emo
        # set by lip syncing logic
        self.modename = mode
        # overpowers the modename when not set to LIPSYNC
        self.supermode = LIPSYNC

        if not self.emoname:
            hide = "wait"
        self.hide = hide
        if not self.hide:
            self.talk_sprite = fadesprite(assets)
            self.blink_sprite = fadesprite(assets)
            self.combined = fadesprite(assets)

            path = PATH_PORT+charname
            self._loadfrom(path, emo, blinkname if blinkname else filename)
            self._blinkhandle(mode)

            self.blinkspeed = self.blink_sprite.blinkspeed
        self.init_sounds()

    def setprop(self, p, v):
        if p in "xy":
            self.pos["xy".index(p)] = float(v)
        if p in "z":
            self.z = int(v)
        if p in ["supermode", "mode"]:
            setattr(self, p, v)

    def set_dim(self, amt):
        self.blink_sprite.dim = amt
        self.talk_sprite.dim = amt

    def get_dim(self):
        return self.blink_sprite.dim
    dim = property(get_dim, set_dim)

    def draw(self, dest):
        _sprite = self.get_current_sprite()
        if not self.hide and _sprite:
            _sprite.tint = self.tint
            _sprite.greyscale = self.greyscale
            _sprite.invert = self.invert
            pos = self.pos[:]
            pos[0] += (self.assets.sw-(_sprite.offsetx + _sprite.img.get_width()))//2
            pos[1] += (self.assets.sh-(_sprite.img.get_height() - _sprite.offsety))
            _sprite.pos = pos
            _sprite.rot = self.rot[:]
            _sprite.draw(dest)

    def delete(self):
        self.kill = 1

    def update(self):
        _sprite = self.get_current_sprite()
        if not self.hide and _sprite:
            return _sprite.update()

    def set_emotion(self, emo):
        if self.hide and self.hide != "wait":
            return
        if not emo:
            return
        self.hide = False
        self.init(self.charname+"/"+emo +
                  "("+self.modename+")", init_basic=False)

    def set_blink_emotion(self, emo):
        if self.hide and self.hide != "wait":
            return
        if not emo:
            return
        self.hide = False
        self.init(self.charname+"/"+self.emoname +
                  "("+self.modename+")", blinkname=emo, init_basic=False)

    def set_talking(self):
        self.modename = TALK

    def set_blinking(self):
        self.modename = BLINK

    def set_single(self):
        self.modename = BLINK
        self.supermode = BLINK

    def set_lipsync(self):
        self.supermode = LIPSYNC
        self.modename = BLINK

    def get_current_sprite(self):
        mode = self.modename if self.supermode == LIPSYNC else self.supermode
        if mode == BLINK: return self.blink_sprite
        if mode == TALK: return self.talk_sprite
        if mode == "loop": self.blink_sprite.loopmode = "loop"
        return self.blink_sprite
    cur_sprite = property(get_current_sprite)

    def setfade(self, *args):
        self.blink_sprite.setfade(*args)
        self.talk_sprite.setfade(*args)
    invert = 0
    tint = None
    greyscale = 0
