import pygame
from . import constants
import pygame.freetype
#pygame.import_as_pygame()
import sys,os

pygame.freetype.init()#font
#print(str(os.path.join("fonts","NotoSans-SemiCondensedMedium.ttf"))+"font")

class Window(object):
    _focused = None
    _keyboard = False
    def sf(self,v):
        self._focused = v
        
    focused = property(lambda s: s._focused,sf)
    move = []
    over = None
window = Window()

defcol = {
"bgcol" : [203,178,143],
"bgcol2" : [183,137,72],
"bgfocus" : [174,150,115],
"textcol" : [0,0,0],
"fgcol" : [208,144,40],
"olcol" : [0,0,0],
"butbg" : [142,65,0],
"butborder" : [30,30,30],
"buttext" : [253,251,234],
"buthigh" : [214,165,49],
"panebg" : [145,113,66],
"paneborder" : [0,0,0],
"scrollbg" : [0,112,0],
"scrollfg" : [0,184,0],
"sbarbg" : [108,44,20],
"sbarfg" : [208,144,40],
"gamebg":[225,223,175],
}

class widget():
    """parent class of many gui objects in core"""
    visible = 1
    mouse_pos = property(lambda x: pygame.mouse.get_pos())
    def __init__(self,pos=[0,0],size=[0,0],parent=None):
        self.rpos = pos[:]
        self.font = pygame.freetype.Font(os.path.join("fonts","NotoMono-Regular.ttf"),10)#Vera
        self.parent = parent
        self._width = size[0]
        self._height = size[1]
        self.visible = True
        self.children = []
        self.padding = {"top":3,"bottom":3,"left":2,"right":2}
    def getprop(self,p):
        """for wrightscript"""
        if p in "xy":
            return self.rpos["xy".index(p)]
        return getattr(self,p,"")
    def setprop(self,p,v):
        """for wrightscript"""
        if p in "xy":
            self.rpos["xy".index(p)] = float(v)
        if p in "z":
            self.z = int(v)
    def setlayout(self,v):
        self.nolayout = not v
        return self
    def focus(self):
        window.focused = self
    # TODO: not all widgets have width and height. fix
    def gw(self):
        if not self.visible: return 0
        return self._width
    def sw(self,w):
        self._width = w
    def gh(self):
        if not self.visible: return 0
        return self._height
    def sh(self,h):
        self._height = h
    width = property(gw,sw)
    height = property(gh,sh)
    def add_child(self,win):
        if win not in self.children: self.children.append(win)
        win.parent = self
        return win
    def remove_child(self,win):
        if win in self.children: self.children.remove(win)
        win.parent = None
        return win
    #~ def mouseover(mp):
        #~ return mp[0]>=pos[0] and mp[0]<=pos[0]+self.width and mp[1]>=pos[1] and mp[1]<=pos[1]+self.height
    def draw(self,dest):
        if self.visible: 
            for c in self.children:
                rp = c.rpos[:]
                c.rpos[0]+=self.rpos[0]
                c.rpos[1]+=self.rpos[1]
                c.draw(dest)
                c.rpos = rp
    
    def event(self,name,pos,*args):
        if getattr(self,"kill",0) or getattr(self,"hidden",0):
            return False
        if(not hasattr(self, "width") or not self.width): return
        if pos[0]>=self.rpos[0] and pos[0]<=self.rpos[0]+self.width and pos[1]>=self.rpos[1] and pos[1]<=self.rpos[1]+self.height:
            p2 = [pos[0]-self.rpos[0],pos[1]-self.rpos[1]]
            for w in reversed(self.children):
                if not hasattr(w,"event"): continue
                done = w.event(name,p2,*args)
                if done:
                    return True
            if not window.over:
                window.over = self
            func = getattr(self,name,None)
            if func: 
                args = [pos]+list(args)
                func(*args)
                return True
    def click_down_over(self,pos):
        window.focused = self
    def click_up(self,pos):
        if window.focused == self:
            window.focused = None
    def handle_events(self,evts):
        if pygame.mouse.get_pressed()[0]:
            self.event("hold_down_over",widget.mouse_pos)
        quit = False
        for evt in evts:
            if evt.type == pygame.KEYUP and evt.key == pygame.K_ESCAPE:
                quit = True
            elif evt.type == pygame.QUIT:
                quit = True
            elif evt.type == pygame.MOUSEMOTION:
                window.over = None
                self.event("move_over",evt.pos,evt.rel,evt.buttons)
                for f in window.move:
                    f(evt.pos,evt.rel)
            elif evt.type == pygame.MOUSEBUTTONDOWN and evt.button == 1:
                window.focused = None
                self.event("click_down_over",evt.pos)
            elif evt.type == pygame.MOUSEBUTTONDOWN and evt.button == 3:
                self.event("rclick_down_over",evt.pos)
            elif evt.type == pygame.MOUSEBUTTONDOWN and evt.button == 4:
                self.event("scroll_up_over",evt.pos)
            elif evt.type == pygame.MOUSEBUTTONDOWN and evt.button == 5:
                self.event("scroll_down_over",evt.pos)
            elif evt.type == pygame.MOUSEBUTTONUP and evt.button == 1:
                self.event("click_up_over",evt.pos)
                if window.focused:
                    window.focused.click_up(evt.pos)
            #~ elif evt.type == pygame.MOUSEBUTTONUP and evt.button == 3:
                #~ if window.over:
                    #~ for i,l in enumerate(self):
                        #~ if over in l.objects:
                            #~ f = open("turnabout_zzztrial/tmp.txt","w")
                            #~ f.writelines([str(l).replace("\n","")+"\n" for l in lines[i:]])
                            #~ f.close()
                            #~ os.system("c:/python25/python PyWright.py turnabout_zzztrial tmp")
            #~ elif evt.type == pygame.KEYDOWN and evt.key == pygame.K_LEFT:
                #~ if window.focused and hasattr(window.focused,"carat_left"):
                    #~ window.focused.carat_left()
            #~ elif evt.type == pygame.KEYDOWN and evt.key == pygame.K_RIGHT:
                #~ if window.focused and hasattr(window.focused,"carat_right"):
                    #~ window.focused.carat_right()
            elif evt.type == pygame.KEYDOWN and evt.key == pygame.K_UP:
                if window.focused and hasattr(window.focused,"carat_up"):
                    window.focused.carat_up()
            elif evt.type == pygame.KEYDOWN and evt.key == pygame.K_DOWN:
                if window.focused and hasattr(window.focused,"carat_down"):
                    window.focused.carat_down()
            elif evt.type == pygame.KEYDOWN and evt.key == pygame.K_RETURN and window.focused:
                if window.focused and hasattr(window.focused,"enter_pressed"):
                    win,window.focused = window.focused,None
                    win.enter_pressed()
            elif evt.type == pygame.KEYDOWN:
                if evt.key in self.repeat:
                    self.start_repeat(evt.key)
                elif evt.key not in [pygame.K_LSHIFT,pygame.K_RSHIFT,pygame.K_ESCAPE] and window.focused:
                    if hasattr(window.focused,"insert") and hasattr(evt,"unicode"):
                        def t(m):
                            if hasattr(window.focused,"insert"):
                                window.focused.insert(m["unicode"])
                        self.repeat[evt.key] = {"func":t,"val":0,"del":True,"unicode":evt.unicode}
                        self.start_repeat(evt.key)
                elif evt.key in [pygame.K_LSHIFT,pygame.K_RSHIFT]:
                    for k in list(self.repeat.keys()):
                        if "unicode" in self.repeat[k]:
                            del self.repeat[k]
            elif evt.type == pygame.KEYUP:
                if evt.key in [pygame.K_LSHIFT,pygame.K_RSHIFT]:
                    for k in list(self.repeat.keys()):
                        if "unicode" in self.repeat[k]:
                            del self.repeat[k]
                if evt.key in self.repeat:
                    self.stop_repeat(evt.key)
        return quit
    def start_repeat(self,key):
        self.repeat[key]['val'] = 120
    def stop_repeat(self,key):
        self.repeat[key]['val'] = 0
        if self.repeat[key].get("del",False):
            del self.repeat[key]
    def handle_repeat(self,key):
        if not self.repeat[key]['val']:
            return
        self.repeat[key]['val'] -= 1
        if self.repeat[key]['val'] in [0,119]:
            self.repeat[key]['func'](self.repeat[key])
        if self.repeat[key]['val'] in [0]:
            self.repeat[key]['val'] = 15
    def update(self):
        for k in self.repeat:
            self.handle_repeat(k)
        return False
    def carat_left_caller(m):
        if window.focused and hasattr(window.focused,"carat_left"):
            window.focused.carat_left()
    def carat_right_caller(m):
        if window.focused and hasattr(window.focused,"carat_right"):
            window.focused.carat_right()
    def delete(m):
        if hasattr(window.focused,"delete"):
            #pass
            window.focused.delete_2()
    def backspace(m):
        if hasattr(window.focused,"backspace"):
            window.focused.backspace()
    repeat = {pygame.K_LEFT:{"func":carat_left_caller,"val":0},
                pygame.K_RIGHT:{"func":carat_right_caller,"val":0},
                pygame.K_DELETE:{"func":delete,"val":0},
                pygame.K_BACKSPACE:{"func":backspace,"val":0}}

class editbox(widget):
    def __init__(self,target_ob,target_attr,is_dict=False):
        super(editbox,self).__init__()
        self.is_dict = is_dict
        if target_ob is None:
            self.target_ob = self
            self.target_attr = "text"
            self.text = target_attr
        else:
            self.target_ob = target_ob
            self.target_attr = target_attr
        self.height = self.font.render("TEST",[0,0,0])[0].get_height()#1,
        self.draw_back = True
        self.carat = 0
        self.force_width = None
        self.password=False
    def click_up(self,pos):
        pass
    def click_down_over(self,mp):
        mp[0]-=self.rpos[0]
        txt = self.val()
        metrics = self.font.get_metrics(txt)
        w = 0
        i = 0
        while metrics and w<mp[0] and i<len(metrics):
            w+=metrics[i][4]
            i += 1
        if metrics and mp[0]>2 and mp[0]<sum([x[4] for x in metrics])-2:
            i -= 1
        self.carat = i
        window.focused = self
        return True
    def click_up_over(self,mp):
        pass
    def carat_left(self):
        self.carat -= 1
        if self.carat<0: self.carat = 0
    def carat_right(self):
        self.carat += 1
        l = len(self.val())
        if self.carat>l: self.carat = l
    def val(self):
        if self.is_dict:
            return self.target_ob.get(self.target_attr,"").replace("\n","")
        else:
            return getattr(self.target_ob,self.target_attr).replace("\n","")
    def insert(self,str1):
        try:
            u = str(str1)
        except:
            return
        if not u:
            return
        if ord(u)<32:# or ord(u)>165:
            return
        v = self.val()
        #print(v)
        #print(v)print(self.carat)
        if self.carat == 0:
            v = str(str1)+v
        elif self.carat == len(v):
            v = v+str(str1)
        else:
            v = v[:self.carat]+str(str1)+v[self.carat:]
        self.set(v)
        self.carat += 1
    def backspace(self):
        v = self.val()
        if self.carat == 0:
            return
        elif self.carat == len(v):
            v = v[:-1]
        else:
            v = v[:self.carat-1]+v[self.carat:]
        self.set(v)
        self.carat -= 1
    def delete_2(self):
        v = self.val()
        if self.carat == 0:
            v = v[1:]
        elif self.carat == len(v):
            return
        else:
            v = v[:self.carat]+v[self.carat+1:]
        self.set(v)
    def set(self,v):
        if not self.is_dict:
            setattr(self.target_ob,self.target_attr,v)
        else:
            self.target_ob[self.target_attr] = v
    bgcol = defcol["bgcol"]
    bgcol2 = defcol["bgcol2"]
    bgfocus = defcol["bgfocus"]
    textcol = defcol["textcol"]
    def draw(self,dest):
        if not self.visible: return
        pos = self.rpos
        val = self.val()
        if self.password:
            val = "*"*len(val)
        textcol = self.textcol
        bgcol = self.bgcol
        bgcol2 = self.bgcol2
        if not bgcol: bgcol = [0,0,0,0]
        if not bgcol2: bgcol2 = [0,0,0,0]
        if self == window.focused:
            bgcol = self.bgfocus
        if not getattr(self,"txtrender",None)==val:
            self.txtrender = self.font.render(val,textcol)[0]#1,
            if hasattr(self,"bg"): del self.bg
        txt = self.txtrender
        ts = list(txt.get_size())
        if self.force_width is not None:
            ts[0] = self.force_width
        if not hasattr(self,"bg"): 
            self.bg = pygame.Surface([ts[0]+4,ts[1]+4]).convert_alpha()
            if hasattr(self,"lastbgcol"): del self.lastbgcol
        bg = self.bg
        if not getattr(self,"lastbgcol",None)==bgcol: 
            bg.fill(bgcol)
            self.lastbgcol = bgcol
            if hasattr(self,"lastbgcol2"): del self.lastbgcol2
        if not getattr(self,"lastbgcol2",None)==bgcol2: 
            pygame.draw.rect(bg,bgcol2,bg.get_rect(),1)
            self.lastbgcol2 = bgcol2
        bg.blit(txt,[2,2])
        if self == window.focused:
            metrics = self.font.get_metrics(val)
            if metrics:
                x = sum([w[4] for w in metrics][:self.carat])+1
            else:
                x = 0
            pygame.draw.line(bg,textcol,[x,0],[x,ts[1]+4])
        if self.draw_back:
            dest.blit(bg,pos)
        else:
            dest.blit(txt,[pos[0]+2,pos[1]+2])
        self.width = bg.get_width()+4
        super(editbox,self).draw(dest)
        
class label(editbox):
    def __init__(self,text="",rpos=None):
        editbox.__init__(self,None,text)
        if rpos: self.rpos = rpos
        self.draw_back = False
        self.width = 0

class checkbox(widget):
    lastclicked = None
    lastoperation = True
    def __init__(self,text,**kwargs):
        widget.__init__(self,**kwargs)
        self.text = text
        self.editbox = editbox(self,"text")
        self.editbox.draw_back = False
        self.height = self.editbox.height
        self.checked = False
        self.listeners = []
    def add_listener(self, func):
        self.listeners.append(func)
    def remove_listener(self, func):
        self.listeners.remove(func)
    def is_checked(self):
        return self.checked
    def set_checked(self,val):
        self.checked = val
        for listener in self.listeners:
            listener(val)
    def draw(self,dest):
        if not self.visible: return
        bgcol = defcol["bgcol"]
        bgcol2 = defcol["fgcol"]
        pygame.draw.rect(dest,bgcol,pygame.Rect(self.rpos[0], self.rpos[1],14,self.editbox.height))
        pygame.draw.rect(dest,defcol["olcol"],pygame.Rect(self.rpos[0], self.rpos[1],14,self.editbox.height),1)
        if self.checked:
            pygame.draw.rect(dest,bgcol2,pygame.Rect((self.rpos[0]+1,self.rpos[1]+1),(12,self.editbox.height-2)))
            pygame.draw.rect(dest,defcol["olcol"],pygame.Rect((self.rpos[0]+4,self.rpos[1]+4),(14-8,self.editbox.height-8)))
        self.editbox.rpos[0]=self.rpos[0]+16
        self.editbox.rpos[1]=self.rpos[1]
        self.editbox.draw(dest)
        self.width = self.editbox.width+16
        if window.over == self:
            pygame.draw.line(dest,defcol["olcol"],[self.rpos[0],self.rpos[1]+self.height-1],[self.rpos[0]+self.width,self.rpos[1]+self.height-1])
    def click_up(self,pos):
        pass
    def click_down_over(self,mp):
        keys = pygame.key.get_pressed()
        if (keys[pygame.K_LSHIFT] or keys[pygame.K_RSHIFT]):
            if checkbox.lastclicked:
                go = 0
                for x in self.parent.children:
                    if x==checkbox.lastclicked:
                        x.set_checked(checkbox.lastoperation)
                        if not go:
                            go = 1
                        else:
                            checkbox.lastoperation = not checkbox.lastoperation
                            return
                    elif x==self:
                        x.set_checked(checkbox.lastoperation)
                        if not go:
                            go = 1
                        else:
                            checkbox.lastoperation = not checkbox.lastoperation
                            return
                    elif go:
                        x.set_checked(checkbox.lastoperation)
        self.set_checked(not self.checked)
        checkbox.lastclicked = self
        checkbox.lastoperation = self.checked
    def click_up_over(self,mp):
        pass
        
class radiobutton(checkbox):
    groups = {}
    def __init__(self,text,group):
        bin = radiobutton.groups.get(group,[])
        bin.append(self)
        radiobutton.groups[group] = bin
        self.group = radiobutton.groups[group]
        checkbox.__init__(self,text)
    def draw(self,dest):
        if not self.visible: return
        bgcol = defcol["bgcol"]
        bgcol2 = defcol["fgcol"]
        pygame.draw.circle(dest,bgcol,[self.rpos[0]+7,self.rpos[1]+7],7)
        pygame.draw.circle(dest,defcol["olcol"],[self.rpos[0]+7,self.rpos[1]+7],7,1)
        if self.checked:
            pygame.draw.circle(dest,bgcol2,[self.rpos[0]+7,self.rpos[1]+7],3)
            pygame.draw.circle(dest,defcol["olcol"],[self.rpos[0]+7,self.rpos[1]+7],3,1)
        self.editbox.rpos[0]=self.rpos[0]+16
        self.editbox.rpos[1]=self.rpos[1]
        self.editbox.draw(dest)
        self.width = self.editbox.width+16
    def click_down_over(self,mp):
        for cb in self.group:
            cb.set_checked(False)
        self.set_checked(True)
        
class progress(widget):
    def __init__(self):
        widget.__init__(self)
        self.progress = 0
        self.text = ""
        self.editbox = editbox(self,"text")
        self.editbox.draw_back = False
    def draw(self,dest):
        if not self.visible: return
        bgcol = defcol["bgcol"]
        bgcol2 = defcol["fgcol"]
        pygame.draw.rect(dest,bgcol,pygame.Rect(self.rpos[0], self.rpos[1],self.width, self.height))#****
        pygame.draw.rect(dest,defcol["olcol"],pygame.Rect(self.rpos[0], self.rpos[1],self.width, self.height),1)
        pygame.draw.rect(dest,bgcol2,pygame.Rect(self.rpos[0]+1,self.rpos[1]+1,(self.width-2)*self.progress,self.height-2))
        self.editbox.rpos = self.rpos
        self.editbox.draw(dest)
    def click_up(self,pos):
        pass
    def click_down_over(self,mp):
        pass
    def click_up_over(self,mp):
        pass
        
class button(widget):
    def __init__(self,object_containing_func,function_name,*args,**kwargs):
        super(button,self).__init__(*args,**kwargs)
        self.target_ob = object_containing_func
        self.target_func = function_name
        self.text = self.target_func.replace("\n","")
        self.height = self.font.render("TEST",[0,0,0])[0].get_height()+2#1,
        self.bgcolor = defcol["butbg"]
        self.bordercolor = defcol["butborder"]
        self.textcolor = defcol["buttext"]
        self.highlightcolor = defcol["buthigh"]
        self.graphic = None
        self.graphichigh = None
    def click_down_over(self,mp):
        f = self.target_func.replace(" ","_")
        object_containing_func = self.target_ob if self.target_ob else self
        # This thing creates functions with dates on them, somehow.
        func = getattr(object_containing_func,f)
        func()
    def draw(self,dest):
        if not self.visible: return
        pos = self.rpos
        bg = self.graphic
        if self.graphichigh and window.over == self:
            bg = self.graphichigh
        if not bg:
            txt = self.font.render(self.text,self.textcolor)[0]#1,
            ts = txt.get_size()
            bg = pygame.Surface([ts[0]+4,ts[1]+4])
            bgcolor = self.bgcolor
            if window.over == self: bgcolor = self.highlightcolor
            bg.fill(bgcolor)
            pygame.draw.rect(bg,self.bordercolor,[0,0,bg.get_width(),bg.get_height()],1)
            bg.blit(txt,[2,2])
        self.width = bg.get_width()+1
        self.height = bg.get_height()
        dest.blit(bg,pos)
        super(button,self).draw(dest)

#meant to replace ws_button
class main_menu_button(widget):
    screen_setting = ""
    id_name = "_ws_button_"
    def __init__(self,func,name,*args,**kwargs):
        super(main_menu_button,self).__init__(*args,**kwargs)
        self.func = func
        self.text = name
        self.height = self.font.render("TEST",[0,0,0])[0].get_height()+2#1,
        self.bgcolor = defcol["butbg"]
        self.bordercolor = defcol["butborder"]
        self.textcolor = defcol["buttext"]
        self.highlightcolor = defcol["buthigh"]
        self.graphic = None
        self.graphichigh = None
    def click_down_over(self,mp):
        self.func()

    def _draw(self,dest):
        if not self.visible: return
        pos = self.rpos
        bg = self.graphic
        if self.graphichigh and window.over == self:
            bg = self.graphichigh
        if not bg:
            txt = self.font.render(self.text,self.textcolor)[0]#1,
            ts = txt.get_size()
            bg = pygame.Surface([ts[0]+4,ts[1]+4])
            bgcolor = self.bgcolor
            if window.over == self: bgcolor = self.highlightcolor
            bg.fill(bgcolor)
            pygame.draw.rect(bg,self.bordercolor,[0,0,bg.get_width(),bg.get_height()],1)
            bg.blit(txt,[2,2])
        self.width = bg.get_width()+1
        self.height = bg.get_height()
        dest.blit(bg,pos)
        super(main_menu_button,self).draw(dest)

    def delete(self):
        self.kill = 1

    def _getrpos(self):
        rpos = self.rpos[:]
        if self.screen_setting == "try_bottom":
            pass 
        return rpos

    def event(self, name, pos, *args):
        orpos = self.rpos[:]
        self.rpos = self._getrpos()
        _super = super(main_menu_button, self)
        ret = _super.event(name, pos, *args)
        self.rpos = orpos
        return ret

    def draw(self, dest):
        orpos = self.rpos[:]
        self.rpos = self._getrpos()
        self._draw(dest)
        self.rpos = orpos

class pane(widget):
    align = "vert"
    bgcolor = defcol["panebg"]
    bordercolor = defcol["paneborder"]
    border = True
    background = True
    def __init__(self,*args,**kwargs):
        super(pane,self).__init__(*args,**kwargs)
        self.in_height = 0
    def render(self):
        if not self.visible: return
        if not hasattr(self,"offset"):
            self.offset = [0,0]
        surf = pygame.Surface([self.width,self.height])
        if self.background:
            surf.fill(self.bgcolor)
        else:
            surf = surf.convert_alpha()
            surf.fill([0,0,0,0])
        x = self.offset[0]
        yoff = self.offset[1]
        y = yoff
        for w in self.children:
            if self.align and not getattr(w,"nolayout",False):
                w.rpos = [x+w.padding["left"],y+w.padding["top"]]
            if y+w.height>0 and y<self.height or not self.align or getattr(w,"nolayout",False):
                w.draw(surf)
            if self.align == "vert" and not getattr(w,"nolayout",False):
                #if not w.height: continue
                y += w.height+w.padding["bottom"]+w.padding["top"]
            elif self.align == "horiz" and not getattr(w,"nolayout",False):
                #if not w.width: continue
                x += w.width+w.padding["right"]+w.padding["left"]
        self.in_height = y-yoff
        if self.border:
            pygame.draw.rect(surf,self.bordercolor,surf.get_rect(),1)
        return surf
    def draw(self,dest):
        dest.blit(self.render(),self.rpos)
        
class scrollbutton(widget):
    def __init__(self,rpos=[0,0]):
        super(scrollbutton,self).__init__(rpos)
        window.move.append(self.move)
    def draw(self,dest):
        if not self.visible: return
        #print(self.rpos)
        #print(self.parent.height-self.height-2)
        if self.rpos[1] < 2:#this should probably be with the actual function
            self.scroll([self.rpos[0],2-self.rpos[1]])
        if self.rpos[1] > self.parent.height-self.height-2:
            self.scroll([self.rpos[0],-(self.rpos[1]-(self.parent.height-self.height-2))])
        x,y = self.rpos
        pygame.draw.rect(dest,defcol["scrollbg"],pygame.Rect((x,y),(self.width,self.height)))
        pygame.draw.rect(dest,defcol["scrollfg"],pygame.Rect((x+1,y+1),(self.width-2,self.height-2)))
    def move(self,pos,rel):
        if window.focused==self:
            self.scroll(rel)
    def scroll(self,rel):
        self.rpos[1]+=rel[1]
        if self.rpos[1]+self.height>self.parent.height-2:
            self.rpos[1] = self.parent.height-self.height-2
        if self.rpos[1]<2:
            self.rpos[1] = 2
class scrollbar(widget):
    def __init__(self,rpos=[0,0]):
        super(scrollbar,self).__init__(rpos)
        self.scbut = scrollbutton([2,2])
        self.add_child(self.scbut)
    def draw(self,dest):
        if not self.visible: return
        self.draw_scrollbar(dest)
    def draw_scrollbar(self, dest):
        x,y = self.rpos
        surf = pygame.Surface([self.width,self.height])
        pygame.draw.rect(surf,defcol["sbarbg"],pygame.Rect((0,0),(self.width,self.height)))
        pygame.draw.rect(surf,defcol["sbarfg"],pygame.Rect((1,1),(self.width-2,self.height-2)))
        
        self.scbut.width = self.width-4
        self.scbut.draw(surf)
        dest.blit(surf,self.rpos)
                    
class scrollpane(pane):
    def __init__(self,*args,**kwargs):
        super(scrollpane,self).__init__(*args,**kwargs)
        self.scbar = scrollbar([0,0])
        self.scbar.nolayout = True
        self.scbar_y = 0
        self.scbar_height = 0
        self.add_child(self.scbar,"root")
        self.pane = pane([0,0])
        self.pane.border = False
        self.pane.background = False
        self.add_child(self.pane,"root")
        self.pix = 0
        self.last_scbar_pos = None
    def add_child(self,ob,which="pane"):
        if which == "pane":
            return self.pane.add_child(ob)
        return super(scrollpane,self).add_child(ob)
    def scroll_up_over(self,mp):
        if not self.scbar.visible: return
        self.set_offset(self.pane.offset[1]+20)
    def scroll_down_over(self,mp):
        if not self.scbar.visible: return
        self.set_offset(self.pane.offset[1]-20)
    def set_offset(self,offset):
        self.updatescroll()
        self.pane.offset[1] = offset
        try:
            self.scbar.scbut.rpos[1] = -(self.pane.offset[1]-2)/float(self.pix)+2
        except ZeroDivisionError:
            self.scbar.scbut.rpos[1] = 0
            #pass
            #self.scbar.scbut.rpos[1] = -(self.pane.offset[1]-2)/1+2
        self.last_scbar_pos = self.scbar.scbut.rpos[1]
        self.updatescroll()
    def scroll_to_object(self,object):
        self.set_offset(-object.rpos[1])
    def updatescroll(self):
        self.pane.width = self.width-15
        self.pane.height = self.height
        self.pane.rpos = [0,0]
        surf = super(self.__class__,self).render()
        self.scbar.rpos = [self.width-15,self.scbar_y]
        self.scbar.width = 15
        self.scbar.height = self.height+self.scbar_height
        pages = self.pane.in_height/float(self.pane.height)
        if pages <= 1:
            self.scbar.visible = False
        if pages > 1:
            self.scbar.visible = True
            
        if(pages != 0):
            self.scbar.scbut.height = int(self.scbar.height-4)/(pages)
        else:
            self.scbar.scbut.height = int(self.scbar.height-4)
            
            
        if self.scbar.scbut.height > int(self.scbar.height-4):
            self.scbar.scbut.height = int(self.scbar.height-4)
        try:
            pix = float(self.pane.in_height)/float(self.scbar.height-4)
        except ZeroDivisionError:
            pix = 0
        self.pix = pix
        if self.scbar.scbut.rpos[1] != self.last_scbar_pos:
            self.pane.offset[1]=-int(pix*(self.scbar.scbut.rpos[1]-2)+2)
            self.last_scbar_pos = self.scbar.scbut.rpos[1]
        if self.scbar not in self.children:
            self.add_child(self.scbar)
        return surf
    def draw(self,dest):
        if not self.visible: return
        surf = self.updatescroll()
        self.scbar.draw(surf)
        dest.blit(surf,self.rpos)

class directory(pane):
    def populate(self,assets,dir,variabledest,variablename,filter,close_on_choose=True):
        self.children[:] = []
        self.width=constants.SW
        self.height=constants.SH
        self.files = scrollpane([10,20])
        self.files.width = 240
        self.files.height = 140
        self.children.append(self.files)
        
        class myb(button):
            def click_down_over(s,*args):
                self.populate(assets, dir.rsplit("/",1)[0],variabledest,variablename,filter,close_on_choose)
        if dir.replace(".","").replace("/","").strip():
            b = myb(None,"<----")
            self.files.add_child(b)
        if not dir:
            ld = "./"
        else:
            ld = dir+"/"
        ld = ld.replace(",/", "/")
        print("listing",ld)
        for file in os.listdir(ld):
            if os.path.isdir(ld+file) or filter(file):# or filter(file):# or list(filter(file)):
                class myb(button):
                    def click_down_over(s,*args):
                        if os.path.isdir(s.path):
                            from . import settings
                            assets.tool_path = s.path
                            settings.write_init_file(assets)
                            self.populate(assets, s.path,variabledest,variablename,filter,close_on_choose)
                        else:
                            self.chosen_file = s.path
                            self.buttonpane.children.append(self.choose_b)
                b = myb(None,file)
                b.path = ld+file
                self.files.add_child(b)
        
        self.chosen_file = dir
        self.file = editbox(self,"chosen_file")
        self.file.force_width = 250
        self.children.append(self.file)
        
        self.buttonpane = pane(size=[constants.SW,20])
        self.buttonpane.align = ""
        self.buttonpane.border = False
        self.children.append(self.buttonpane)
        
        class myb(button):
            def click_down_over(s,*args):
                setattr(variabledest,variablename,self.chosen_file)
                if close_on_choose:
                    self.delete()
        self.choose_b = myb(None,"Choose")
        
        class myb(button):
            def click_down_over(s,*args):
                self.delete()
        b = myb(None,"close")
        b.rpos = [200,0]
        self.buttonpane.children.append(b)
    def delete(self):
        self.kill = 1
        super(directory,self).delete()

class script_code(pane):
    def __repr__(self):
        return self.msg

    def delete(self):
        self.kill = 1

    def __init__(self, script):
        pane.__init__(self)
        self.rpos = [0, 0]
        self.width = 256
        self.height = 192
        self.pri = -10000
        self.z = 10000
        self.children.append(label(script.scene))
        self.lines = scrollpane([10, 20])
        self.lines.rpos = [0, 40]
        self.lines.width = 240
        self.lines.height = 140
        self.children.append(self.lines)
        scroll_i = None
        for i, line in enumerate(script.scriptlines+["END OF SCRIPT"]):
            color = [0, 0, 0]
            text = line
            if i == script.si:
                color = [255, 0, 0]
                text = "> "+line
            line = label(text)
            line.textcol = color
            if i == script.si:
                scroll_i = line
            self.lines.pane.children.append(line)
        if scroll_i:
            self.lines.updatescroll()
            self.lines.scroll_to_object(scroll_i)
        self.children.append(button(self, "delete"))

    def update(self):
        return True

