import functools

def argsort(list,arg="pri",get=getattr):
    def cmp(a, b):
        if a is None:
            a = 0
        if b is None:
            b = 0
        return (a > b) - (a < b) 
    def _cmp(a,b):
        return cmp(get(a,arg),get(b,arg))
    list.sort(key=functools.cmp_to_key(_cmp))

class mylist(list): pass
class World:
    """A collection of objects"""
    def __init__(self,assets, initial_objects=None):
        self.assets = assets
        if not initial_objects: initial_objects = []
        self.all = initial_objects[:]
        for o in self.all:
            o.cur_script = assets.get_stack_top()
    def render_order(self):
        """Return a list of objects in the order they should
        be rendered"""
        modded_list = mylist(self.all[:])
        if self.assets.variables.get("_layering_method","zorder") == "zorder":
            argsort(modded_list,"z")
        else:
            pass

        # redefine mylist's append(object) method, to also use World.append()
        oldapp = modded_list.append
        def _modded_append(ob):
            self.append(ob)
            oldapp(ob)
        modded_list.append = _modded_append
        return modded_list
    def update_order(self):
        """Return a list of objects in the order they
        should be updated"""
        n = self.all[:]
        argsort(n,"pri")
        return n
    def select(self):
        """Return a list of objects that match the query"""
    def append(self,ob):
        self.all.append(ob)
        ob.cur_script = self.assets.get_stack_top()
    def extend(self,obs,unique=True):
        if unique:
            for o in obs:
                if o not in self.all:
                    self.all.append(o)
        else:
            self.all.extend(o)
    def remove(self,ob):
        self.all.remove(ob)
