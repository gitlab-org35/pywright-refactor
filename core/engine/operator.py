from core.assets_init import assets
from core.vtrue import vtrue

def INT(n):
    try:
        return int(n)
    except:
        return float(n)


def EVAL(stuff):
    stuff = stuff.split(" ", 2)
    if len(stuff) == 1:
        return vtrue(assets.variables.get(stuff[0], ""))
    if len(stuff) == 2:
        stuff = stuff[0], "=", stuff[1]
    current, op, check = stuff
    if op not in ["<", ">", "=", "!=", "<=", ">="]:
        check = op+" "+check
        op = "="
    current = assets.variables.get(current)
    if op == "=":
        op = "=="
    if op not in ["==", "!="]:
        current = INT(current)
        check = INT(check)
    if op == ">":
        return current > check
    elif op == "<":
        return current < check
    elif op == "==":
        return current == check
    elif op == "!=":
        return current != check
    elif op == "<=":
        return current <= check
    elif op == ">=":
        return current >= check


def GV(v):
    if v[0].isdigit():
        if "." in v:
            return float(v)
        return int(v)
    if v.startswith("'") and v.endswith("'"):
        return v[1:-1]
    v = assets.variables.get(v, "")
    if v[0].isdigit():
        if "." in v:
            return float(v)
        return int(v)
    return v


def ADD(statements):
    return GV(statements[0])+GV(statements[1])


def MUL(statements):
    return GV(statements[0])*GV(statements[1])


def MINUS(statements):
    return GV(statements[0])-GV(statements[1])


def DIV(statements):
    return GV(statements[0])/GV(statements[1])


def EQ(statements):
    return str(GV(statements[0]) == GV(statements[1])).lower()


def GTEQ(statements):
    return str(GV(statements[0]) >= GV(statements[1])).lower()


def GT(statements):
    return str(GV(statements[0]) > GV(statements[1])).lower()


def LT(statements):
    return str(GV(statements[0]) < GV(statements[1])).lower()


def LTEQ(statements):
    return str(GV(statements[0]) <= GV(statements[1])).lower()


def AND(statements):
    if vtrue(statements[0]) and vtrue(statements[1]):
        return "true"
    return "false"


def OR(stuff):
    for line in stuff:
        if EVAL(line):
            return True
    return False


def OR2(statements):
    if vtrue(statements[0]) or vtrue(statements[1]):
        return "true"
    return "false"


def EXPR(line):
    statements = []
    cur = ""
    paren = []
    quote = []
    for word in line.split(" "):
        if not paren and not quote and word == "+":
            statements.append(ADD)
        elif not paren and not quote and word == "*":
            statements.append(MUL)
        elif not paren and not quote and word == "-":
            statements.append(MINUS)
        elif not paren and not quote and word == "/":
            statements.append(DIV)
        elif not paren and not quote and word == "==":
            statements.append(EQ)
        elif not paren and not quote and word == "<=":
            statements.append(LTEQ)
        elif not paren and not quote and word == ">=":
            statements.append(GTEQ)
        elif not paren and not quote and word == "<":
            statements.append(LT)
        elif not paren and not quote and word == ">":
            statements.append(GT)
        elif not paren and not quote and word == "AND":
            statements.append(AND)
        elif not paren and not quote and word == "OR":
            statements.append(OR2)
        elif word.strip():
            if paren:
                if word.endswith(")"):
                    paren.append(word)
                    statements.append(EXPR(" ".join(paren)[1:-1]))
                    paren = []
                else:
                    paren.append(word)
            elif quote:
                quote.append(word)
                if word.endswith("'"):
                    statements.append(" ".join(quote))
                    quote = []
            elif word.startswith("(") and word.endswith(")"):
                statements.append(word[1:-1])
            elif word.startswith("("):
                paren.append(word)
            elif word.startswith("'") and word.endswith("'"):
                statements.append(word)
            elif word.startswith("'"):
                quote.append(word)
            else:
                statements.append(word)
    return statements


def EVAL_EXPR(expr):
    if not isinstance(expr, list):
        return str(expr)
    if len(expr) == 1:
        return EVAL_EXPR(expr[0])
    oop = [MUL, DIV, ADD, MINUS, EQ, LT, GT, LTEQ, GTEQ, OR2, AND]
    ops = []
    for i, v in enumerate(expr):
        if v in oop:
            ops.append((i, v))
    if not ops:
        return str(expr[0])
    ops.sort(key=lambda x: oop.index(x[1]))
    op = ops[0]
    left = expr[op[0]-1:op[0]]
    right = expr[op[0]+1:op[0]+2]
    left = EVAL_EXPR(left)
    right = EVAL_EXPR(right)
    v = op[1]([left, right])
    expr[op[0]-1] = v
    del expr[op[0]]
    del expr[op[0]]
    return EVAL_EXPR(expr)
