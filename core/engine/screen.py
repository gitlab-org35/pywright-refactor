import pygame
from core import screen_format
from core import constants
import os, sys

def get_screen_dim(total_width, total_height, sw, sh, cur_screen, mode, cur_script=None, aspect=True):
    if mode == screen_format.TWO_SCREENS:
        top_pos = [0,0]
        top_size = [1,0.5]
        bottom_pos = [0,0.5]
        bottom_size = [1,0.5]
        aspect = float(sw)/(float(sh)*2)
        raspect = total_width/float(total_height)
        baspect = aspect/raspect
        #Too skinny
        if baspect>1: 
            top_size[1]*=raspect/aspect
            bottom_size[1]=top_size[1]
            top_pos[1]=(1-(top_size[1]+bottom_size[1]))/2.0
            bottom_pos[1]=top_pos[1]+top_size[1]
        #Too fat
        elif baspect<1:
            top_size[0]*=aspect/raspect
            bottom_size[0]=top_size[0]
            top_pos[0]=(1-top_size[0])/2.0
            bottom_pos[0]=(1-bottom_size[0])/2.0
    if mode == screen_format.HORIZONTAL:
        top_pos = [0,0]
        top_size = [0.5,0.75]
        bottom_pos = [0.5,0.25]
        bottom_size = [0.5,0.75]
    if mode == screen_format.SMALL_BOTTOM:
        top_pos = [0,0]
        top_size = [0.75,1]
        bottom_pos = [0.75,0.25]
        bottom_size = [0.25,0.4]
    if mode == screen_format.SCALED_SINGLE:
        top_pos = [0,0]
        top_size = [1,1]
        bottom_pos = None
        bottom_size = None
    if mode == screen_format.SHOW_ONE:
        top_pos = [0,0]
        top_size = [1,1]
        bottom_pos = None
        bottom_size = None

    if cur_screen != 0 or (
            mode in [screen_format.SHOW_ONE,screen_format.SMALL_BOTTOM] 
            and cur_script
            and cur_script.get_primary_switch()):
        top_size,bottom_size = bottom_size,top_size
        top_pos,bottom_pos = bottom_pos,top_pos

    d = {"top":None,"bottom":None}
    if top_pos:
        top_pos_t = [top_pos[0]*total_width,top_pos[1]*total_height]
        top_size_t = [top_size[0]*total_width,top_size[1]*total_height]
        d["top"] = [top_pos,top_size,top_pos_t,top_size_t]
    if bottom_pos:
        bottom_pos_t = [bottom_pos[0]*total_width,bottom_pos[1]*total_height]
        bottom_size_t = [bottom_size[0]*total_width,bottom_size[1]*total_height]
        d["bottom"] = [bottom_pos,bottom_size,bottom_pos_t,bottom_size_t]
    return d


#this method ends up being assigned to the Assets singleton and only used through 'assets.make_screen()'.
# TODO: remove use of reflection and make delegation explicit
class PyGameScreen:
    def __init__(self) -> None:
        self.total_width = None
        self.total_height = None
        self.sw = constants.STANDARD_SW
        self.sh = constants.STANDARD_SH
        self.fullscreen = 0
        self.show_fps = 0
        self.framerate = 60
        self.smoothscale = 0
        self.cur_screen = 0

        self.real_screen = None
        self.screen = None
        self.blank = None
        self.js1 = None
        self.jsleft = None
        self.jsright = None
        self.jsup = None
        self.jsdown = None
        self.gbamode = False
        self.screen_refresh = 1
        self.screen_mode = screen_format.TWO_SCREENS

    def make_screen(self, assets):
        if self.total_width<constants.STANDARD_SW:
            self.total_width=constants.STANDARD_SW
        if self.total_height/self.screen_mode.num_screens<constants.STANDARD_SH:
            self.total_height = constants.STANDARD_SH*self.screen_mode.num_screens

        flags = pygame.RESIZABLE|pygame.FULLSCREEN*self.fullscreen|pygame.DOUBLEBUF 
        SCREEN= self.real_screen = pygame.display.set_mode([self.total_width,self.total_height],flags)
        self.screen = pygame.Surface([self.sw,self.sh*2]).convert()
        self.blank = self.screen.convert()
        self.blank.fill([0,0,0])
        js1 = None
        def gl():
            return js1 and js1.get_numhats() and js1.get_hat(0)[0]<0
        def gr():
            return js1 and js1.get_numhats() and js1.get_hat(0)[0]>0
        def gu():
            return js1 and js1.get_numhats() and js1.get_hat(0)[1]>0
        def gd():
            return js1 and js1.get_numhats() and js1.get_hat(0)[1]<0
        self.jsleft = gl
        self.jsright = gr
        self.jsup = gu
        self.jsdown = gd
        
        ##pygame.display.set_icon(pygame.image.load("art/general/bb.png"))
        if pygame.joystick.get_init():
            pygame.joystick.quit()
        pygame.joystick.init()
        if pygame.joystick.get_count():
            self.js1 = pygame.joystick.Joystick(0)
            self.js1.init()
        if os.environ.get("SDL_VIDEODRIVER",0)=="dummy":
            self.screen = pygame.Surface([self.sw,self.sh*2],0,32)
            self.blank = pygame.Surface([self.sw,assets.self.sh*2],0,32)
            self.blank.fill([0,0,0])

def scale_relative_click(assets, pos, rel):
    dim = assets.screen_format()
    def col(pp,ss):
        if pos[0]>=pp[0] and pos[0]<=pp[0]+ss[0]\
            and pos[1]>=pp[1] and pos[1]<=pp[1]+ss[1]:
            x = rel[0]/float(ss[0])*assets.screen_data.sw
            y = rel[1]/float(ss[1])*assets.screen_data.sh
            return [x,y]
    if dim["top"]:
        r = col(*dim["top"][2:])
        if r:
            return r
    if dim["bottom"]:
        r = col(*dim["bottom"][2:])
        if r:
            return r
    return rel
    
def translate_click(assets, pos):
    dim = assets.screen_format()
    def col(pp,ss):
        if pos[0]>=pp[0] and pos[0]<=pp[0]+ss[0]\
            and pos[1]>=pp[1] and pos[1]<=pp[1]+ss[1]:
            x = pos[0]-pp[0]
            x = x/float(ss[0])*assets.screen_data.sw
            y = pos[1]-pp[1]
            y = y/float(ss[1])*assets.screen_data.sh
            return [int(x),int(y)]
    if dim["top"]:
        r = col(*dim["top"][2:])
        if r:
            return r
    if dim["bottom"]:
        r = col(*dim["bottom"][2:])
        if r:
            r[1]+=assets.screen_data.sh
            return r
    return [-100000,-100000]
def _fit(surf,size, smoothscale):
    if smoothscale and surf.get_bitsize() in [24,32]:
        surf = pygame.transform.scale2x(surf)
        surf = pygame.transform.smoothscale(surf,[int(x) for x in size])
    else:
        surf = pygame.transform.scale(surf,[int(x) for x in size])
    return surf
    
"""
Requires a PygameScreen with an initialized screen
"""
def __blit_screen__(screen_data,stack_top=None):
    if screen_data.total_height!=screen_data.sh or screen_data.total_width!=screen_data.sw: scale = 1
    top = screen_data.screen.subsurface(pygame.Rect((0,0),(screen_data.sw,screen_data.sh)))
    bottom = top
    mode = screen_data.screen_mode

    dim = get_screen_dim(
                screen_data.total_width, screen_data.total_height, 
                screen_data.sw, screen_data.sh, 
                screen_data.cur_screen, 
                screen_data.screen_mode,
                stack_top)

    if mode == screen_format.TWO_SCREENS \
            or mode == screen_format.HORIZONTAL \
            or mode == screen_format.SHOW_ONE \
            or mode == screen_format.SMALL_BOTTOM:
        bottom = screen_data.screen.subsurface(pygame.Rect((0,screen_data.sh),(screen_data.sw,screen_data.sh)))
    screen_data.real_screen.fill([10,10,10])

    def draw_segment(dest,surf,pos,size):
        rp = [pos[0]*screen_data.total_width,pos[1]*screen_data.total_height]
        rs = [size[0]*screen_data.total_width,size[1]*screen_data.total_height]
        surf = _fit(surf, rs, screen_data.smoothscale)
        dest.blit(surf,rp)
    if dim["top"]:
        draw_segment(screen_data.real_screen,top,dim["top"][0],dim["top"][1])
    if dim["bottom"]:
        draw_segment(screen_data.real_screen,bottom,dim["bottom"][0],dim["bottom"][1])

"""
Requires an Assets instance with an initialized screen (with make_screen) or it will fail.
"""
def draw_screen(assets, showfps):
    if assets.stack_empty():
        __blit_screen__(assets.screen_data)
    else:
        __blit_screen__(assets.screen_data, assets.get_stack_top())
    if showfps:
        var = assets.get_font("nt").render(str(assets.clock.get_fps()),[0,0,0],[100,180,200])
        rect = [0,assets.screen_data.real_screen.get_height()-12]
        assets.screen_data.real_screen.blit(var[0],rect)
    pygame.display.flip()