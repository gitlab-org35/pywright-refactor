from . import script
def create_menu_bottomscript():
    return create_bottomscript(scriptlines=[
        'print test','textblock 0 0 256 192 Loading menu...','gui Wait'])

def create_start_bottomscript():
    return create_bottomscript(scriptlines=["fg ../general/logosmall y=-15 x=-35 name=logo",
        "zoom mag=-0.25 frames=30 nowait"] + ["gui Wait"])

def create_bottomscript(scriptlines):
    bottomscript = script.Script()
    bottomscript.init_from_scriptlines(scriptlines=scriptlines)
    bottomscript.scene = None
    return bottomscript

def create_macro_script(parent, scriptlines, args, macros, scene, world):
    nscript = script.Script(parent)
    nscript.world = world
    nscript.init_from_scriptlines(scriptlines=scriptlines)
    nscript.scene = scene
    nscript.macros = macros
    nscript.buildmode = True
    return nscript