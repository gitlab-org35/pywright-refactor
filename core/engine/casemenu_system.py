class CaseMenuSystem:
    def add_case_menu(self, priority):
        self.priority = priority

    def end_script(self, interpreter, cache, world):
        new_casemenu = False
        if hasattr(self, 'priority'):
            new_casemenu = True
            interpreter.set(self.priority, cache, world)
        return new_casemenu