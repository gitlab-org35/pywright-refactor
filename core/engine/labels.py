from core import errors  
 
def __label__(line):
    return line.split(" ", 1)[1].strip().replace(" ", "_")

def __argslist(line):
    return line.split(" ")[1:]

def __cross_label__(line):
    return "_".join(
            [word 
            for word in __argslist(line)
            if not word.startswith("fail=")]
        ).strip()

def __append_label__(line, index, list):
    label = __label__(line)
    if label: list.append([label, index])
    return list

def __append_cross_label__(line, index, list):
    label = __cross_label__(line)
    if label: list.append([label, index])
    return list

def create_labels(scriptlines):
    labels = []
    cross = False
    for i, line in enumerate(scriptlines):
        line = line.lstrip()
        if line.startswith("label ") or line.startswith("result "):
            labels = __append_label__(line, i, labels)
        elif line.startswith("list ") or line.startswith("statement "):
            labels = __append_label__(line, i-1, labels)
        elif line.startswith("cross ") or line == "cross":
            cross = True
            labels = __append_cross_label__(line, i-1, labels)
        elif line.startswith("endcross"):
            cross = False
        elif line.startswith("statement") and not cross:
            raise errors.script_error(
                "'statement' command may only be used between 'cross' and 'endcross'.")
    return labels