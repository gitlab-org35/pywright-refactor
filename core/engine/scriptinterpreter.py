class ScriptInterpreter:
    def __init__(self):
        self.temp_casemenu = None
        self.temp_world = None
        self.temp_img = None
    def set(self, temp_casemenu, temp_world, temp_img):
        self.temp_casemenu = temp_casemenu
        self.temp_world = temp_world
        self.temp_img = temp_img
    
    def update_casemenu(self, script, casemenu):
        '''
        if temp_casemenu is truthy, push a casemenu, cache and world
        '''
        if self.temp_casemenu != 0 and self.temp_casemenu != None:  
            script.objects = [casemenu]
            script.imgcache = self.temp_img
            script.set_world(self.temp_world)
            self.temp_casemenu = None
        
    ## Interpret scripts -> interpret line > safe exec > execute macro
    def interpret_scripts(self, assets):
        """Process wrightscript until we should block and show action on the screen"""
        while 1:
            if assets.stack_empty():
                assets.make_start_script(False)
            # TODO: review casemenu logic
            # casemenu = assets.casemenu(self.temp_casemenu)
            # self.update_casemenu(assets.get_stack_top(), casemenu)
            block = assets.get_stack_top().interpret_line()
            if block:
                return

"""
used more than 50 times, this method allows other parts of the code
to run macros. Example:

        subscript("hide_court_record_button")
        subscript("hide_press_button")
        subscript("hide_present_button")
"""
def subscript(macro):
    from core.assets_init import assets
    script = assets.get_stack_top().execute_macro(macro)
    assets.interpreter.interpret_scripts(assets)
    return