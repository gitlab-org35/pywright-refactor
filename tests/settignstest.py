import unittest
class TestAssetSettings(unittest.TestCase):

    #TODO: fix unset settings
    def test_set_assets_from_settings(self):
        from core.assets import Assets
        from core import settings
        from core.assets_init import mixer

        lines = [';standard width is 256\n', 
        ';standard height is 192\n','width=969\n',
         'height=470\n', 'scale2x=0\n', 
        'smoothscale=1\n', 'fullscreen=0\n',
         'show_fps=1\n', 'framerate=60.0\n', 
         'sound_format=44100\n',
         'sound_bits=16\n', 'sound_buffer=4096\n', 
         'sound_volume=100\n', 
         'music_volume=100\n', 'mute_sound=0\n',
          'screen_refresh=3\n', 'autosave=1\n',
           'autosave_interval=5\n', 'autosave_keep=2\n',
           'tool_path=,,,,,,,,,,,,,,,,,,,,,,,,\n', 
           'screen_mode=two_screens']
        a1 = Assets(singleton=False) # disables checks for multiple instances of Assets.
        #In a real program there would only be one instance of assets.

        mixer.init()

        #total_width and total_height attributes don't exist
        self.assertEquals(getattr(a1,"total_width", None), None)
        self.assertEquals(getattr(a1,"total_height", None), None)

        settings.set_assets_attributes(a1, lines)

        #total_width and total_height attributes exist
        self.assertEquals(969, a1.screen_data.total_width)
        self.assertEquals(470, a1.screen_data.total_height)

        # IMPORTANT: without this teardown, the next test will break
        mixer.quit()

    # if mixer is not initialized when loading settings, errors will occur
    def test_set_assets_from_settings_without_init(self):  
        from core import assets
        f = open("program_settings/settings.ini", "r")
        lines = f.readlines()
        a1 = assets.Assets(singleton=False) # disables checks for multiple instances of Assets.
        #In a real program there would only be one instance of assets.
        from core import settings
        settings.set_assets_attributes(a1, lines)

        #total_width and total_height attributes exist
        self.assertEquals(646, a1.screen_data.total_width)
        self.assertEquals(639, a1.screen_data.total_height)

