import unittest
from core import new_sorting as ns
from core.new_sorting import Sorting
# Characterization tests. There to identify accidental changes.
# Feel free to edit if (and preferably before) you change or refactor
# the sorting module.
class TestLayerOderingImport(unittest.TestCase):
    # tests for new_sorting.py, to replace sorting.py
    def test_newulayers(self):
        zlayers, ulayers = ns.getlayers(ns.getopts("program_settings/newsorting.ini"))
        ulayers[''] = 80     # parser ignores keys with no value
        self.assertDictEqual(
            ulayers,
            {'error_msg': -10000, 'saved': -5000, 'waitenter': -1000, 'scroll': -1000, 'rotateanim': -1000, 'fadeanim': -1000, 'guiScroll': -1000, 'timer': -1000, 'zoomanim': -1000, 'uglyarrow': -1000, 'flash': -1000, 'shake': -1000, 'evidence_menu': 0, 'fg': 1, 'movie': 1, 'sprite': 1, 'fadesprite': 1, 'portrait': 20, 'case_menu': 25, 'textbox': 30, 'penalty': 50, 'menu': 50, 'listmenu': 50, '': 80, 'examine_menu': 80, 'guiWait': 81, 'notguilty': 1000, 'guilty': 1000, 'delay': 10000})

    def test_newzlayers(self):
        zlayers, ulayers = ns.getlayers(ns.getopts("program_settings/newsorting.ini"))
        zlayers[''] = 8      # parser ignores keys with no value
        self.assertDictEqual(
            zlayers,
            {'': 8, 'uglyarrow': 0, 'waitenter': 0, 'delay': 0, 'scroll': 0, 'rotateanim': 0, 'fadeanim': 0, 'guiWait': 0, 'SoundEvent': 0, 'invisible': 0, 'bg': 1, 'portrait': 2, 'char': 2, 'sprite': 3, 'fadesprite': 3, 'graphic': 3, 'mesh': 3, 'evidence': 3, 'fg': 3, 'movie': 3, 'testimony_blink': 3, 'textblock': 4, 'textbox': 4, 'penalty': 4, 'notguilty': 4, 'guilty': 4, 'press_button': 7, 'present_button': 7, 'menu': 5, 'listmenu': 5, 'examine_menu': 5, 'gui': 5, 'record_button': 6, 'evidence_menu': 7, 'guiBack': 7, 'guiScroll': 7, 'case_menu': 8, 'flash': 9, 'shake': 9, 'saved': 10, 'error_msg': 11})

    def test_newzlayers_object(self):
        sorting = Sorting()
        self.assertEqual(sorting.get_z_layer("uglyarrow"), 0)
        self.assertEqual(sorting.get_z_layer("bg"), 1)
        self.assertEqual(sorting.get_z_layer("portrait"), 2)
        self.assertEqual(sorting.get_z_layer("char"), 2)
        

    def test_newulayers_object(self):
        sorting = Sorting()
        self.assertEqual(sorting.get_u_layer("saved"), -5000)
        self.assertEqual(sorting.get_u_layer("portrait"), 20)
        self.assertEqual(sorting.get_u_layer("undefined"), None)
        