import unittest

class TestCoreMacroFunctionality(unittest.TestCase):

    def test_subscript(self):
        from core.constants import STANDARD_SW, STANDARD_SH
        from core.engine.scriptinterpreter import subscript
        from core.assets_init import assets
        from core.engine import script
        assets.set_stack([script.Script()])
        assets.get_stack_top().init()
        self.assertNotEqual(assets, None)
        self.assertNotEqual(assets.get_stack_top(), None)
        assets.autosave = False
        assets.screen_data.total_width = STANDARD_SW
        assets.screen_data.total_height = STANDARD_SH
        assets.screen_data.cur_screen = 0

        import pygame
        pygame.init()
        #i need to make the screen due to interpret_scripts->endscript->make_start_script
        assets.make_screen()
        subscript("hide_press_button")
    
    def test_macro_oneline_args(self):
        from core import macro
        lines = macro.expand_macro_from_args("macro", [], macros = {"macro": ["hewwo", "uwu"]}, lineno=1)
        self.assertEquals(["hewwo", "uwu"], lines)