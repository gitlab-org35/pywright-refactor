import unittest
from core.assets import TextRenderer
from core.textutil import markup_text

class TestTextUtil(unittest.TestCase):
    def testmarkup(self):
        m = markup_text("This is a test")
        self.assertEqual(m._text, [c for c in "This is a test"])

    def testmarkup_noflag(self):
        m = markup_text("{$lb}$_speaking_name{$rb}")
        self.assertNotEqual(m._text, [c for c in "{$lb}$_speaking_name{$rb}"])

    def testmarkup_withflag(self):
        m = markup_text("{$lb}$_speaking_name{$rb}", True)
        self.assertNotEqual(m._text, [c for c in "{$lb}$_speaking_name{$rb}"])

    def test_render(self):
        import pygame
        from core.textutil import TextRenderer
        pygame.init()
        font = pygame.freetype.Font("fonts/pwinternational.ttf",80)
        renderer = TextRenderer(font)
        char = renderer._render_surf('a', [0,0,0])

    def test_render_more(self):
        import pygame

        pygame.init()
        screen = pygame.display.set_mode([600,600])
        
        import pygame
        from core.textutil import TextRenderer
        pygame.init()
        font = pygame.freetype.Font("fonts/pwinternational.ttf",80)
        renderer = TextRenderer(font)
        surf = renderer._render_surf('a', [0,0,0])
        rect = surf.get_rect()
        k = 1
        while k < 999:
            screen.fill((230,255,230))
            screen.blit(surf, rect)
            pygame.display.update()
            k += 1


