import unittest
from core.registry import File
from core.registry import Registry

class TestRegistry(unittest.TestCase):
    def testfile(self):
        a = File("../data/art/port/kristoph2/normal(talk).txt")
        b = File("../data/art/port/kristoph2/normal(talk).png")
        c = File("../data/art/port/kristoph12/normal(talk).jpg")
        d = File("../data/art/port/kristoph2/normal(talk)")
        assert a.path=="../data/art/port/kristoph2/normal(talk).txt"
        assert b.path=="../data/art/port/kristoph2/normal(talk).png"
        assert c.path=="../data/art/port/kristoph12/normal(talk).jpg"
        assert d.path=="../data/art/port/kristoph2/normal(talk)"
        assert a.filetag==b.filetag==c.filetag==d.filetag
        assert a.pathtag==b.pathtag!=c.pathtag
        assert b.priority<c.priority,"bpri:%s cpri:%s"%(b.priority,c.priority)
        assert c.priority<d.priority

    def test_registry(self):
        rec = Registry("./examples")
        self.assertNotEqual(rec.map, {})

        self.assertEqual("./examples/art/ev/maya.png",rec.lookup("art/ev/maya"))
        self.assertEqual("./examples/art/ev/maya.png",rec.lookup("art/ev/maya.txt"))

        #even an invalid extension will work
        self.assertEqual("./examples/art/ev/maya.png",rec.lookup("art/ev/maya.lolxd"))
        self.assertEqual("./examples/art/ev/maya.png",rec.lookup("art/ev/maya."))

    def test_invalid_root(self):
        rec = Registry("garbage garbage")
        self.assertEqual(rec.map, {})