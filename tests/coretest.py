import unittest
from core.assets_init import Assets
from core.assets_init import Variables
from core.constants import STANDARD_SH, STANDARD_SW
import core.screen_format

class TestCoreClasses(unittest.TestCase):
    def test_assets_initialization(self):
        assets = Assets(singleton=False) # disables checks for multiple instances of Assets.
        #In a real program there would only be one instance of assets.

        #gigalist of assets attributes
        self.assertEqual(assets.lists , {})
        self.assertEqual(assets.snds , {})

        # Variables() initializes the same here as during Assets() initialization
        self.assertEqual(assets.variables, Variables(assets))
        self.assertEqual(assets.gbamode, False)
        self.assertEqual(assets.screen_mode, core.screen_format.TWO_SCREENS)
        self.assertRaises(AttributeError, lambda: assets.num_screens)

        self.assertEqual(assets.sw , STANDARD_SW)
        self.assertEqual(assets.sh, STANDARD_SH)
        self.assertEqual(assets.fonts, {})

        self.assertEqual(assets.game_folder,None)
        # Deffonts is no longer defined in the middle of the object Assets.
        self.assertEqual(assets.deffonts, None)

        #total_width and total_height attributes don't exist
        self.assertEquals(getattr(assets,"total_width", None), None)
        self.assertEquals(getattr(assets,"total_height", None), None)

    def test_variables_init(self):
        var = Variables(Assets(singleton=False))
        assert(var.get("_version", None))
        self.assertEqual(var.get("_version", None), '1.15.10')
