import unittest
import time
class TestSoundClasses(unittest.TestCase):
    def test_sound_in_examples(self):
        from core.assets import Music_Assets
        music_assets = Music_Assets()

        with self.assertRaises(Exception) as context:
            music_assets.game_folder
            
        music_assets.init_sound()
        music_assets.play_sound("examples","crossexam.ogg")
        time.sleep(3)


    def test_sound_in_nonexistent_folder(self):
        from core.assets import Music_Assets
        music_assets = Music_Assets()

        with self.assertRaises(Exception) as context:
            music_assets.game_folder
            
        music_assets.init_sound()
        
        #sound is still found from erroneous folder.
        music_assets.play_sound("GARBAGEGARBAGETRASH","crossexam.ogg")
        time.sleep(3)

