import unittest
from core.engine import script
class TestScript(unittest.TestCase):
    def test_script(self):
        myclass = script.Script
        methods = [method for method in dir(myclass) if method.startswith('_') and (not method.startswith('__'))]
        def cleanup(string):
            return string.replace('_', '',1)
        methods = list(map(cleanup, methods))
        self.assertEquals(methods, ['absvar', 
        'addev', 'addvar', 'bemo', 'bg', 'callpresent', 
        'callpress', 'casemenu', 'char', 'clear', 'clearcross', 
        'controlanim', 'cross', 'cross_restart', 'debug', 'delete', 'delev', 
        'delflag', 'divvar', 'emo', 'endcross', 'endscript', 'ev', 'examine', 'examine3d', 'exit',
         'exportvars', 'fade', 'fg', 'filewrite', 'flag', 'forgetlist', 'forgetlistitem', 'framerate', 
         'game', 'gamemenu', 'gchildren', 'getprop', 'getvar', 'globaldelay', 'goto', 
         'grey', 'gui', 'importvars', 'invert', 'is', 'is_ex', 'isempty', 'isnot', 'isnotempty',
          'isnumber', 'joinvar', 'label', 'li', 'list', 'lo', 'loadgame', 'localmenu', 'menu', 'mesh',
           'movie', 'mulvar', 'mus', 'next_statement', 'noflag', 'nt', 'obj', 'pause', 'penalty', 'present',
            'prev_statement', 'print', 'random', 'region3d', 'resume', 'rotate', 'savegame', 'screenshot', 'script', 
            'scroll', 'set', 'set_ex', 'setflag', 'setprop', 'setvar', 'setvar_ex', 'sfx', 'shake', 
            'showlist', 'showpresent', 'showrecord', 'statement', 'step', 'subvar', 'surf3d', 'textblock', 'textbox',
             'timer', 'tint', 'top', 'update_objects', 'waitenter', 'zoom'])

    def test_eval(self):
        from core.assets_init import assets
        from core.vtrue import vtrue
        EVAL_EXPR = script.EVAL_EXPR
        EXPR = script.EXPR
        assert EVAL_EXPR(EXPR("5 + 1 + 3 * 10")) == "36"
        assert EVAL_EXPR(EXPR("2 * (5 + 1)")) == "12"
        assert EVAL_EXPR(EXPR("'funny ' + 'business'")) == "funny business"
        assert vtrue(EVAL_EXPR(EXPR("2 * (5 + 1) == (5 + 1) * 2")))
        assets.variables["something"] = "1"
        assert EVAL_EXPR(EXPR("5 + something + 3 * 10")) == "36"
        del assets.variables["something"]
        assert EVAL_EXPR(EXPR(
            "(5 == 4 OR 5 == 5) AND (1 + 3 == 4) AND ('funny' = 'not funny')")) == "false"
        assert EVAL_EXPR(
            EXPR("(5 == 4 OR 5 == 5) AND (1 + 3 == 4) OR ('funny' = 'not funny')")) == "true"
        assert EVAL_EXPR(EXPR("5 > 3")) == "true"
        assert EVAL_EXPR(EXPR("5 > 6")) == "false"
        assert EVAL_EXPR(EXPR("5 < 3")) == "false"
        assert EVAL_EXPR(EXPR("5 < 6")) == "true"
        assert EVAL_EXPR(EXPR("5 >= 3")) == "true"
        assert EVAL_EXPR(EXPR("5 >= 5")) == "true"
        assert EVAL_EXPR(EXPR("5 >= 6")) == "false"
        assert EVAL_EXPR(EXPR("5 <= 3")) == "false"
        assert EVAL_EXPR(EXPR("5 <= 5")) == "true"
        assert EVAL_EXPR(EXPR("5 <= 6")) == "true"
